# What is this Project?

My prefered way of thinking is through writing. The result of this is that for every project I work on I always end up with lots of text files. I spend hours writing and organizing my thoughts into different files. I couldn't find a wiki system that I liked for various reasons. The two biggest reasons are that I don't like writing using markup languages, I find that it is space sensistive and prone to errors, and that most wiki's don't support nesting text very well, which I do frequently. So, currently I use a standard text editor to edit the text files with and standard file system browser to create and organize the files. I believe I can do better in building a system to organizing my thoughts without annoying me and getting in the way of my thinking.

This project takes inspiration from Doug Engelbart's NLS and the idea of creating an integrating environment in which to do all of your knowledge work in. I plan to add support for other applications besides text editing, such as creating and publishing the documentation for this project and for creating and publishing my blog. This way I can organize all of my ideas into one consistent environment where I can fix all of the annoyances I have with other applications.

# Current Status
The project currently is a cross between a wiki system and a structured text editor. There are enough features implemented to support wikis with multiple files and basic structured text editing operations. The program should be considered experimental. It is usable, but there are bugs, sometimes data corrupting bugs. Do not use this program for anything serious.

Development is done on the master branch. To use the application checkout the stable branch

# license
The code is licensed under the MIT license
