/**
 * This module has functions that are shared between both electron_main.js and WindowManager.js this avoids circular dependencies.
 */
const {dialog}=require('electron');

function confirmUnsavedChanges(win){
    let choice=dialog.showMessageBoxSync(win,{            
        message:"There are unsaved changes to this wiki. Are you sure you want to close?",
        buttons:["Yes","No"]
    });
    if(choice==0){
        return true;
    }
    else{
        return false;
    }
}

module.exports={confirmUnsavedChanges:confirmUnsavedChanges};