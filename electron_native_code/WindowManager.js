const {BrowserWindow}=require('electron');
const {confirmUnsavedChanges}=require("./utlity.js");

/**
 * The electron BrowserWindow class cannot be extend due to design decisions made by the electron team. So, the BrowserWindow object is wrapped inside of the WindowMetadata class to add metadata to the window.
 */
class WindowMetadata{    
    constructor(windowObj){
        this.windowObj=windowObj;
        this.fileName="";
        this.hasUnsavedChanges=false;
    }
}

/**
 * The WindowManager class keeps track of all of the open windows. This is necessary to ensure that only one window per file is opened. This is needed to prevent accidentally overwriting the changes made in one window with change made in the same file in another window.
 */
class WindowManager{
    constructor(){
        this.openWindows=[];
        this.desktopState=null;
    }

    createWindow(fileName){
        let existingWindow=null;
        if (fileName!==null){
            existingWindow=this.openWindows.find((winObj)=>{
                return winObj.fileName===fileName;
            })
        }
        if(!existingWindow){
            let title;
            if(fileName!==null){
                title=fileName;
            } 
            else{
                title="new wiki";
            }
            let win= new BrowserWindow({
                width:800,height:800,title:title,
                webPreferences:{
                    nodeIntegration:true,
                    contextIsolation:false
                }
            });
            win.setMenu(null);
            win.loadURL(`file://${__dirname}/../src/index.html`)
            let winWithMeta=new WindowMetadata(win);
            winWithMeta.fileName=fileName;
            this.openWindows.push(winWithMeta);
            
            win.on('close',(e)=>{
                if(winWithMeta.hasUnsavedChanges){
                    let confirmed=confirmUnsavedChanges(win);
                    if(!confirmed){
                        e.preventDefault();
                        return;
                    }
                    else{
                        let index=this.openWindows.indexOf(winWithMeta);
                        this.openWindows.splice(index,1); 
                    }
                }
                else{
                    let index=this.openWindows.indexOf(winWithMeta);
                    this.openWindows.splice(index,1); 
                }
            })

            win.webContents.once('did-finish-load',()=>{
                win.webContents.send("setZoomLevel",this.desktopState.zoomLevel);
                win.webContents.send("setRecentlyOpenedWikiFiles",this.desktopState.recentlyOpenedWikiFiles)
                if(fileName!==null){
                    win.webContents.send("loadFile",fileName)
                }
            });
        }
        else{
            existingWindow.windowObj.focus();
        }
    }

    areWindowsOpen(){
        if(this.openWindows.length===0){
            return false;
        }
        else{
            return true;
        }
    }

    getWindowByFileName(fileName){
        let result=this.openWindows.find(window=>window.fileName===fileName);
        if(result){
            return result;
        }
        else{
            return false;
        }         
    }

    getWindowByWindowId(windowId){
        let result=this.openWindows.find(window=>window.windowObj.id===windowId);
        if(result){
            return result;
        }
        else{
            return false;
        } 
    }

    /**
     * Given an event from electron ipcMain, return the window that sent the event. This replaces getting the windowId in the client using the remote module, since using the remote module in electron is now deprecated.
     */
    getWindowFromEvent(event){
        let win=BrowserWindow.fromWebContents(event.sender);
        return this.getWindowByWindowId(win.id);
    }
}

module.exports={WindowManager:WindowManager}