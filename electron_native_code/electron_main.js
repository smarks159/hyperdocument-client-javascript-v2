const {app,ipcMain,dialog} = require('electron');
const fs=require("fs");
const path=require("path");
const {WindowManager}=require("./WindowManager.js");
const {confirmUnsavedChanges}=require("./utlity.js");
let Windows=new WindowManager();

/**
 * Check to see if the current window has unsaved changes. If so, prompt the user to see if they really what to close the window.
 */
ipcMain.on('confirmUnsavedChanges',(event)=>{
    let win=Windows.getWindowFromEvent(event);
    if(win.hasUnsavedChanges){
        let confirmed=confirmUnsavedChanges(win.windowObj);
        event.returnValue=confirmed;
    }
    else{
        event.returnValue=true;
    }
})

class DesktopState{
    constructor(){
        this.zoomLevel=0;
        this.recentlyOpenedWikiFiles=[]
    }

    toJSON(){
        return {"zoomLevel":this.zoomLevel,"recentlyOpenedWikiFiles":this.recentlyOpenedWikiFiles};
    }

    static fromJSON(jsonObj){
        let desktopState=new DesktopState()
        desktopState.zoomLevel=jsonObj.zoomLevel;
        desktopState.recentlyOpenedWikiFiles=jsonObj.recentlyOpenedWikiFiles;
        return desktopState;
    }
}

let desktopState=new DesktopState();
Windows.desktopState=desktopState;
ipcMain.on('setZoomLevel',(event,zoomLevel)=>{
    desktopState.zoomLevel=zoomLevel;
    for(const window of Windows.openWindows){
        window.windowObj.webContents.send("setZoomLevel",zoomLevel);
    }
    event.returnValue=true;
})
ipcMain.on('getZoomLevel',(event)=>{
    event.returnValue=desktopState.zoomLevel;
})

ipcMain.on('setRecentlyOpenedWikiFiles',(event,recentlyOpenedWikiFiles)=>{
    desktopState.recentlyOpenedWikiFiles=recentlyOpenedWikiFiles;
    for(const window of Windows.openWindows){
        window.windowObj.webContents.send('setRecentlyOpenedWikiFiles',recentlyOpenedWikiFiles);
    }
    event.returnValue=true;
})

ipcMain.on('getRecentlyOpenedWikiFiles',(event)=>{
    event.returnValue=desktopState.recentlyOpenedWikiFiles;
})

function getStatePath(){
    let pathString=app.getAppPath()
    
    /* Two instances of the application can potentially be installed at once, say one for development and one for production. The state should not be shared between the two instances, especially when the format of the state file could potentially change during development. So we derive the folder name in which to store the state from last two folders in the application path. */
    let stateDir=pathString.split(path.sep).slice(-2).join("_");
    
    // dots in folder names causes errors
    stateDir=stateDir.replace(".","-")
    return path.join(app.getPath("userData"),"state",stateDir);
}

/* In development the app must run in a separate instance in order to prevent it from sharing code with the production app. This way code updates are reflected in the development app without shuting down the production app. */
if(app.isPackaged){
    const gotLock = app.requestSingleInstanceLock()
    if(!gotLock){
        app.quit()
    }

    app.on('second-instance',(event,commandLine,workingDirectory)=>{
        let newWindow=null;
        /* The commandLine here is not the same as process.argv of the second instance, has something to do with the fact the electron using chromes commandline parsing algorithm, which adds commandline parameters.
        So, we must search all of the command line args, to find the file name.
        */
        if(commandLine.length >= 2){        
            for(const arg of commandLine){
                if(arg.endsWith(".hypdoc")){
                    newWindow=arg;
                }
            }
        }
        Windows.createWindow(newWindow);
    })
} 



app.on('ready',()=>{
    let stateDir=getStatePath();
    let desktopStatePath=path.join(stateDir,"desktop_state.json");
    if(fs.existsSync(desktopStatePath)){

        desktopState=DesktopState.fromJSON(JSON.parse(fs.readFileSync(desktopStatePath)));
        Windows.desktopState=desktopState;
    }

    let newWindow=null;
    // When double clicking on a file with an .hypdoc extension, open it in the browser.
    if (process.platform == 'win32' && process.argv.length >= 2) {        
        let filePath = process.argv[1];
        // argv[1] is only the file to open in production when double clicking on a file, not in development when calling electron .
        if(filePath.endsWith(".hypdoc")){
            newWindow=filePath;
        }
    }
    Windows.createWindow(newWindow);
});

app.on('window-all-closed',()=>{
    if(process.platform!=='darwin'){
        app.quit();
    }
    let stateDir=getStatePath();
    if(!fs.existsSync(path.join(app.getPath("userData"),"state"))){
        fs.mkdirSync(path.join(app.getPath("userData"),"state"));
    }    
    if(!fs.existsSync(stateDir)){
        fs.mkdirSync(stateDir);
    }
    fs.writeFileSync(path.join(stateDir,"desktop_state.json"),JSON.stringify(desktopState.toJSON()),'utf8');
})

ipcMain.on('fileExists',(event,fileName)=>{
    event.returnValue=fs.existsSync(fileName);
})

ipcMain.on('loadState',(event,arg)=>{    
    let stateDir=getStatePath();
    event.returnValue=fs.readFileSync(path.join(stateDir,"state.json"),'utf8');
})

ipcMain.on('hasSavedState',(event,arg)=>{
    let stateDir=getStatePath();
    event.returnValue=fs.existsSync(path.join(stateDir,"state.json"));
    
})

ipcMain.on('saveState',(event,data)=>{
    
    let stateDir=getStatePath();
    if(!fs.existsSync(path.join(app.getPath("userData"),"state"))){
        fs.mkdirSync(path.join(app.getPath("userData"),"state"));
    }    
    if(!fs.existsSync(stateDir)){
        fs.mkdirSync(stateDir);
    }
    fs.writeFileSync(path.join(stateDir,"state.json"),data,'utf8');
    event.returnValue=true;
})

ipcMain.on("selectFileToSave",(event)=>{
    let win=Windows.getWindowFromEvent(event).windowObj;
    if(win){
        let fileName=dialog.showSaveDialogSync(win,{
            title:"Select File to Save",
            filters:[{name:"Hyperdocument Files",extensions:['hypdoc']}]
        });
        if(fileName===undefined){
            fileName="";
        }
        event.returnValue=fileName;
    }
    else{
        throw new Error("No window found for given windowId");
    }
})

ipcMain.on("selectFileToLoad",(event)=>{
    let win=Windows.getWindowFromEvent(event).windowObj;
    if(win){
        let fileName=dialog.showOpenDialogSync(win,{
            title:"Select File to Load",properties:["openFile"],
            filters:[{name:"Hyperdocument Files",extensions:['hypdoc']}]
        });
        if(fileName===undefined){
            fileName="";
        }
        else{
            fileName=fileName[0];
        }    
        event.returnValue=fileName;
    }
    else{
        throw new Error("No window found for given windowId");
    }
})

ipcMain.on("selectTextToImport",(event)=>{
    let win=Windows.getWindowFromEvent(event).windowObj;
    if(win){
        let fileName=dialog.showOpenDialogSync(win,{
            title:"Select File to Import",properties:["openFile"],
            filters:[{name:"text file",extensions:['txt']}]
        });
        if(fileName===undefined){
            fileName="";
        }
        else{
            fileName=fileName[0];
        }    
        event.returnValue=fileName;
    }
    else{
        throw new Error("No window found for given windowId");
    }
});

ipcMain.on("selectFileToExport",(event)=>{
    let win=Windows.getWindowFromEvent(event).windowObj;
    if(win){
        let fileName=dialog.showSaveDialogSync(win,{
            title:"Enter Text To Export To",
            filters:[{name:"text file",extensions:['txt']}]
        });
        if(fileName===undefined){
            fileName="";
        }
        else{
            fileName=fileName;
        }    
        event.returnValue=fileName;
    }
    else{
        throw new Error("No window found for given windowId");
    }
});

ipcMain.on("isFileOpened",(event,fileName)=>{
    let currentWin=Windows.getWindowFromEvent(event);    
    if(fileName!==currentWin.fileName){
        let winExists=Windows.getWindowByFileName(fileName);
        if(winExists!==false){
            event.returnValue=true;
        }
        else{
            event.returnValue=false;
        }
    }
    else{
        event.returnValue=false;
    }
})

ipcMain.on("focusOnWindowByFilename",(event,fileName)=>{
    let currentWin=Windows.getWindowFromEvent(event);
    // The window holding the current filename is already being focused on, do nothing.
    if(fileName===currentWin.fileName){
        event.returnValue=true;
        return;
    }
    let win=Windows.getWindowByFileName(fileName);
    if(win!==false){
        win.windowObj.focus();
        event.returnValue=true;        
    }
    else{
        event.returnValue=false;
    }
})

ipcMain.on("saveWikiFile",(event,fileName,data)=>{
    let currentWin=Windows.getWindowFromEvent(event);
    if(fileName!==currentWin.fileName){
        let winExists=Windows.getWindowByFileName(fileName)
        if(winExists!==false){
            throw new Error(`The fileName ${fileName} is already opened in another window.`)
        }
        currentWin.fileName=fileName;
    }
    fs.writeFileSync(fileName,data,'utf8');
    event.returnValue=true;
});

ipcMain.on("loadWikiFile",(event,fileName)=>{
    let currentWin=Windows.getWindowFromEvent(event);
    if(fileName!==currentWin.fileName){
        let winExists=Windows.getWindowByFileName(fileName)
        if(winExists!==false){
            throw new Error(`The fileName ${fileName} is already opened in another window.`)
        }
        currentWin.fileName=fileName;
    }
    if(fs.existsSync(fileName)){
         event.returnValue=fs.readFileSync(fileName,'utf8');
    }
    else{
         event.returnValue=false;
    }
 });

ipcMain.on("saveFile",(event,fileName,data)=>{
    fs.writeFileSync(fileName,data,'utf8');
    event.returnValue=true;
});

/**
 * Given a file name load the file without any checks. This is used to load plain text files.
 */
ipcMain.on("loadFile",(event,fileName)=>{   
   if(fs.existsSync(fileName)){
        event.returnValue=fs.readFileSync(fileName,'utf8');
   }
   else{
        event.returnValue=false;
   }
});

ipcMain.on("setWindowTitle",(event,title)=>{
    let win=Windows.getWindowFromEvent(event).windowObj;    
    if(win){
        win.setTitle(title);
        event.returnValue=true;
    }
    else{
        throw new Error("No window found for given windowId");
    }
    
});

ipcMain.on("toggleDevTools",(event)=>{
    let win=Windows.getWindowFromEvent(event).windowObj;
    if(win){
        win.webContents.toggleDevTools();
        event.returnValue=true;
    }
    else{
        throw new Error("No window found for given windowId");
    }
});

ipcMain.on("reloadPage",(event)=>{
    let win=Windows.getWindowFromEvent(event).windowObj;
    if(win){
        win.reload();
        // loadState must be called here because the page is blow away after a refresh and any code on the client after reloadPage is called will not be executed.
        win.webContents.once('did-finish-load',()=>{
            win.webContents.send("loadState");
        });
        event.returnValue=true;
    }
    else{
        throw new Error("No window found for given windowId");
    }
    
});

ipcMain.on("setUnsavedChanges",(event,value)=>{
    let win=Windows.getWindowFromEvent(event)    
    if(win){
        win.hasUnsavedChanges=value;
        event.returnValue=true;
    }
    else{
        throw new Error("No window found for given windowId");
    }
})

ipcMain.on("newWindow",(event,fileToOpen)=>{
    Windows.createWindow(fileToOpen);
    event.returnValue=true;
})