QUnit.module("Property Graph Tests");    
QUnit.test( "Add a node to the property graph", function( assert ) {
    var Graph= new PropertyGraphModel.PropertyGraph();            
    var expectedNode=new PropertyGraphModel.Node();
    expectedNode.objectId=1;
    expectedNode.propertyGraph=Graph;

    var testNode=new PropertyGraphModel.Node();
    Graph.addNode(testNode);
    assert.strictEqual(testNode.objectId,expectedNode.objectId, "After adding a node to a graph, the node should have a uniqueId");
    assert.strictEqual(testNode.propertyGraph,expectedNode.propertyGraph, "After adding a node to a graph, the node should have a a reference to the property graph");      
  
});

QUnit.test("Add node to property graph where objectId > nextObjectId",function(assert){
    // This can happen when applying a diff
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let node=new PropertyGraphModel.Node();
    node.objectId=2;
    propGraph.addNode(node);
    assert.strictEqual(3,propGraph.nextObjectId);
})

QUnit.test("raise error when node with existing incoming edges is added",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let node=new PropertyGraphModel.Node();
    node.incomingEdges.push(1);
    assert.throws(()=>propGraph.addNode(node));
})

QUnit.test("raise error when node with existing outgoing edges is added",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let node=new PropertyGraphModel.Node();
    node.outgoingEdges.push(1);
    assert.throws(()=>propGraph.addNode(node));
})

QUnit.test("Test getNodeIndexById",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1)
    propGraph.addNode(node2)
    let index=propGraph.getNodeIndexById(node2.objectId)
    assert.strictEqual(index,1)
})

QUnit.test("Test getNodeIndexById wrong type",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    assert.throws(()=>propGraph.getNodeIndexById(node2));
})

QUnit.test("raise error if adding a node with an existing objectId",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let node1=new PropertyGraphModel.Node();
    node1.objectId=1;
    let node2=new PropertyGraphModel.Node();
    node2.objectId=1;
    propGraph.addNode(node1);
    assert.throws(()=>propGraph.addNode(node2));
})

QUnit.test( "Add an edge to the property graph", function( assert ) {
    var Graph= new PropertyGraphModel.PropertyGraph();            
    var expectedSourceNode= new PropertyGraphModel.Node();
    expectedSourceNode.objectId=1;
    expectedSourceNode.propertyGraph=Graph;
    var expectedTargetNode= new PropertyGraphModel.Node();      
    expectedTargetNode.objectId=2;
    expectedTargetNode.propertyGraph=Graph;
    var expectedEdge=new PropertyGraphModel.Edge(expectedSourceNode,expectedTargetNode);
    expectedSourceNode.outgoingEdges.push(expectedEdge);
    expectedTargetNode.incomingEdges.push(expectedEdge);
    expectedEdge.edgeId=1;
    expectedEdge.propertyGraph=Graph;

    var testSourceNode= new PropertyGraphModel.Node();
    var testTargetNode= new PropertyGraphModel.Node();
    var testEdge=new PropertyGraphModel.Edge(testSourceNode,testTargetNode);
    Graph.addNode(testSourceNode);
    Graph.addNode(testTargetNode);
    Graph.addEdge(testEdge);      

    assert.strictEqual(testEdge.edgeId,expectedEdge.edgeId,"After adding an edge to the graph, it should have a uniqueId");
    assert.strictEqual(testEdge.propertyGraph,expectedEdge.propertyGraph,"After adding an edge to the graph, it should have a reference to the property graph");      

    assert.deepEqual(testSourceNode.outgoingEdges,expectedSourceNode.outgoingEdges,"After adding an edge to the graph the source node's outgoingEdges list should contain the edge added");
    assert.deepEqual(testTargetNode.incomingEdges,expectedTargetNode.incomingEdges,"After adding an edge to the graph the target node's incomingEdges list should contain the edge added");
});

QUnit.test("Test getEdgeIndexById",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    let node3=new PropertyGraphModel.Node();
    let edge1=new PropertyGraphModel.Edge(node1,node2);
    let edge2=new PropertyGraphModel.Edge(node2,node3);
    propGraph.addNode(node1)
    propGraph.addNode(node2)
    propGraph.addNode(node3)
    propGraph.addEdge(edge1);
    propGraph.addEdge(edge2);

    let index=propGraph.getEdgeIndexById(edge2.edgeId);
    assert.strictEqual(index,1)
})

QUnit.test("Test getEdgeIndexById wrong type",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    let edge1=new PropertyGraphModel.Edge(node1,node2);
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    propGraph.addEdge(edge1);
    assert.throws(()=>propGraph.getEdgeIndexById(edge1));
})

QUnit.test("raise error if adding edge with same edgeId",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    let node3=new PropertyGraphModel.Node();
    let edge1=new PropertyGraphModel.Edge(node1,node2);
    edge1.edgeId=1;
    let edge2=new PropertyGraphModel.Edge(node2,node3);
    edge2.edgeId=1;
    propGraph.addNode(node1)
    propGraph.addNode(node2)
    propGraph.addNode(node3)
    propGraph.addEdge(edge1);
    assert.throws(()=>propGraph.addEdge(edge2));
})

QUnit.test("Add edge to property graph where edgeId > nextEdgeId",function(assert){
    // This can happen when applying a diff
    let propGraph=new PropertyGraphModel.PropertyGraph(); 
    let testSourceNode= new PropertyGraphModel.Node();
    let testTargetNode= new PropertyGraphModel.Node();
    let testEdge=new PropertyGraphModel.Edge(testSourceNode,testTargetNode);
    testEdge.edgeId=2
    propGraph.addNode(testSourceNode);
    propGraph.addNode(testTargetNode);
    propGraph.addEdge(testEdge);

    assert.strictEqual(3,propGraph.nextEdgeId);
})

QUnit.test("delete node",function(assert){
    var Graph=new PropertyGraphModel.PropertyGraph();
    var node1=new PropertyGraphModel.Node();
    var node2=new PropertyGraphModel.Node();
    Graph.addNode(node1);
    Graph.addNode(node2);
    Graph.deleteNode(node1);        
    assert.strictEqual(Graph.nodes.length,1);
    assert.strictEqual(Graph.nodes[0],node2);
});

QUnit.test("delete node with edge",function(assert){
    var Graph=new PropertyGraphModel.PropertyGraph();
    var node1=new PropertyGraphModel.Node();
    var node2=new PropertyGraphModel.Node();
    Graph.addNode(node1);
    Graph.addNode(node2);
    Graph.addEdge(new PropertyGraphModel.Edge(node1,node2));
    Graph.deleteNode(node1);        
    assert.strictEqual(Graph.edges.length,0);
    assert.strictEqual(node1.outgoingEdges.length,0);
    assert.strictEqual(node2.incomingEdges.length,0);
});

QUnit.test("Serialize Graph to data",function(assert){
    var expectedData={
        "nextObjectId":3,
        "nextEdgeId":2,
        "nodes":[{"objectId":1,"type":"Node","properties":{},"incomingEdges":[],"outgoingEdges":[1]}
                ,{"objectId":2,"type":"Node","properties":{},"incomingEdges":[1],"outgoingEdges":[]}],
        "edges":[{"edgeId":1, "type": "Edge","sourceNode":1,"targetNode":2,"properties":{}}],
        "tagIndex":{}
    };
    
    var Graph=new PropertyGraphModel.PropertyGraph();
    var node1=new PropertyGraphModel.Node();
    var node2=new PropertyGraphModel.Node();
    Graph.addNode(node1);
    Graph.addNode(node2);
    Graph.addEdge(new PropertyGraphModel.Edge(node1,node2));        
    var data=Graph.saveToData();        
    assert.deepEqual(data,expectedData);
    
});

QUnit.test("Load Graph from data",function(assert){
    var expectedGraph=new PropertyGraphModel.PropertyGraph();
    var node1=new PropertyGraphModel.Node();
    var node2=new PropertyGraphModel.Node();
    expectedGraph.addNode(node1);
    expectedGraph.addNode(node2);
    expectedGraph.addEdge(new PropertyGraphModel.Edge(node1,node2));   
    
    var data={
        "nextObjectId":3,
        "nextEdgeId":2,
        "nodes":[{"objectId":1,"type":"Node","properties":{},"incomingEdges":[],"outgoingEdges":[1]}
                ,{"objectId":2,"type":"Node","properties":{},"incomingEdges":[1],"outgoingEdges":[]}],
        "edges":[{"edgeId":1, "type": "Edge","sourceNode":1,"targetNode":2,"properties":{}}],
        "tagIndex":{}
    };
    var graph=PropertyGraphModel.PropertyGraph.loadFromData(data);
    assert.deepEqual(graph,expectedGraph);
});

QUnit.test("generate diff add node",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let oldData= propGraph.saveToData();
    propGraph.addNode(new PropertyGraphModel.Node());
    
    let expectedDiff= new PropertyGraphModel.Diff();
    expectedDiff.nodesAdded=[{
        incomingEdges:[],
        outgoingEdges:[],
        objectId:1,
        type:"Node",
        properties:{}
    }
    ]
    
    let testDiff=PropertyGraphModel.genDiff(oldData,propGraph.saveToData());
    assert.deepEqual(testDiff,expectedDiff);
})


QUnit.test("generate diff remove node",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let node=new PropertyGraphModel.Node()
    propGraph.addNode(node);
    let oldData=propGraph.saveToData();
    propGraph.deleteNode(node);
    
    let expectedDiff= new PropertyGraphModel.Diff();
    expectedDiff.nodesDeleted=[{
        incomingEdges:[],
        outgoingEdges:[],
        objectId:1,
        type:"Node",
        properties:{}
    }
    ]
    
    let testDiff=PropertyGraphModel.genDiff(oldData,propGraph.saveToData());
    assert.deepEqual(testDiff,expectedDiff);
})

QUnit.test("generate diff edit node property",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let node= new DataModels.TextDataModel();
    node.text="test1";
    propGraph.addNode(node);
    let oldData=propGraph.saveToData();
    node.text="test2";
    
    let expectedDiff = new PropertyGraphModel.Diff();
    expectedDiff.nodesChangedProperties=[{
        from:{
            incomingEdges:[],
            outgoingEdges:[],
            objectId:1,
            type:"TextDataModel",
            properties:{"text":"test1"}
        },
        to:{
            incomingEdges:[],
            outgoingEdges:[],
            objectId:1,
            type:"TextDataModel",
            properties:{"text":"test2"}
        }
    }]
    
    let testDiff=PropertyGraphModel.genDiff(oldData,propGraph.saveToData());
    assert.deepEqual(testDiff,expectedDiff);
    
});

QUnit.test("generate diff add edge",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    let oldData= propGraph.saveToData();
    
    let edge=new PropertyGraphModel.Edge(node1,node2);
    propGraph.addEdge(edge);
    
    let expectedDiff= new PropertyGraphModel.Diff();
    expectedDiff.edgesAdded=[{
        sourceNode:node1.objectId,
        targetNode:node2.objectId,
        edgeId:1,
        type:"Edge",
        properties:{}
    }
    ]
    
    let testDiff=PropertyGraphModel.genDiff(oldData,propGraph.saveToData());
    assert.deepEqual(testDiff,expectedDiff);
});

QUnit.test("generate diff remove edge",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    let edge=new PropertyGraphModel.Edge(node1,node2);
    propGraph.addEdge(edge);
    let oldData= propGraph.saveToData();
    propGraph.deleteEdge(edge);
    
    let expectedDiff= new PropertyGraphModel.Diff();
    expectedDiff.edgesDeleted=[{
        sourceNode:node1.objectId,
        targetNode:node2.objectId,
        edgeId:1,
        type:"Edge",
        properties:{}
    }
    ]
    
    let testDiff=PropertyGraphModel.genDiff(oldData,propGraph.saveToData());
    assert.deepEqual(testDiff,expectedDiff);
});

QUnit.test("generate diff edit edge property",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    let edge=new PropertyGraphModel.TreeChildEdge(node1,node2);
    edge.index=1;
    propGraph.addEdge(edge);
    let oldData= propGraph.saveToData();
    edge.index=2;
    
    let expectedDiff= new PropertyGraphModel.Diff();
    expectedDiff.edgesChangedProperties=[{
        from:{
            sourceNode:node1.objectId,
            targetNode:node2.objectId,
            edgeId:1,
            type:"TreeChildEdge",
            properties:{"index":1}
        },
        to:{
            sourceNode:node1.objectId,
            targetNode:node2.objectId,
            edgeId:1,
            type:"TreeChildEdge",
            properties:{"index":2}
        }
    }]
    
    let testDiff=PropertyGraphModel.genDiff(oldData,propGraph.saveToData());
    assert.deepEqual(testDiff,expectedDiff);
});

QUnit.test("generate diff add two nodes with edge.",function(assert){
    /* We are test here that when added to the diff nodes have no edges in the incoming and outgoing edges.
    All of these edges are already stored in edgesAdded.
    */
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    let oldData= propGraph.saveToData();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    let edge=new PropertyGraphModel.Edge(node1,node2);
    propGraph.addEdge(edge);

    let expectedDiff= new PropertyGraphModel.Diff();
    expectedDiff.edgesAdded=[{
            sourceNode:node1.objectId,
            targetNode:node2.objectId,
            edgeId:1,
            type:"Edge",
            properties:{}
        }]
    expectedDiff.nodesAdded=[
            {
                incomingEdges:[],
                outgoingEdges:[],
                objectId:1,
                type:"Node",
                properties:{}
            },
            {
                incomingEdges:[],
                outgoingEdges:[],
                objectId:2,
                type:"Node",
                properties:{}
            }
        ]
    
    let testDiff=PropertyGraphModel.genDiff(oldData,propGraph.saveToData());
    assert.deepEqual(testDiff,expectedDiff);
})

QUnit.test("generate diff remove node with edge",function(assert){
    /* We are testing this for the same reason as "add two nodes with edge" To make sure that when doing a diff,
    the node data in deletedNodes contains no incoming or outgoing edges. All of the edges are already stored in deletedEdges.
    */

   let propGraph= new PropertyGraphModel.PropertyGraph();
   let node1=new PropertyGraphModel.Node();
   let node2=new PropertyGraphModel.Node();   
   propGraph.addNode(node1);
   propGraph.addNode(node2);
   let edge=new PropertyGraphModel.Edge(node1,node2);
   propGraph.addEdge(edge);
   let oldData= propGraph.saveToData();
   propGraph.deleteNode(node1);

   let expectedDiff= new PropertyGraphModel.Diff();
   expectedDiff.edgesDeleted=[{
           sourceNode:node1.objectId,
           targetNode:node2.objectId,
           edgeId:1,
           type:"Edge",
           properties:{}
       }]
   expectedDiff.nodesDeleted=[
           {
               incomingEdges:[],
               outgoingEdges:[],
               objectId:1,
               type:"Node",
               properties:{}
           }
       ]
   
   let testDiff=PropertyGraphModel.genDiff(oldData,propGraph.saveToData());
   assert.deepEqual(testDiff,expectedDiff);

})

QUnit.test("apply diff add node",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let beforeDiff= propGraph.saveToData();
    propGraph.addNode(new PropertyGraphModel.Node());
    let afterDiffExpected=propGraph.saveToData();

    let diff=PropertyGraphModel.genDiff(beforeDiff,afterDiffExpected);
    let testGraph=PropertyGraphModel.PropertyGraph.loadFromData(beforeDiff);
    PropertyGraphModel.applyDiff(testGraph,diff);
    assert.deepEqual(afterDiffExpected,testGraph.saveToData())
});

QUnit.test("apply diff remove node",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let node=new PropertyGraphModel.Node()
    propGraph.addNode(node);
    let beforeDiff=propGraph.saveToData();
    propGraph.deleteNode(node);
    let afterDiffExpected=propGraph.saveToData();
   
    let diff=PropertyGraphModel.genDiff(beforeDiff,afterDiffExpected);
    let testGraph=PropertyGraphModel.PropertyGraph.loadFromData(beforeDiff);
    PropertyGraphModel.applyDiff(testGraph,diff);
    assert.deepEqual(afterDiffExpected,testGraph.saveToData())
});

QUnit.test("apply diff edit node",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let node= new DataModels.TextDataModel();
    node.text="test1";
    propGraph.addNode(node);
    let beforeDiff=propGraph.saveToData();
    node.text="test2";
    let afterDiffExpected=propGraph.saveToData();
    
    let diff=PropertyGraphModel.genDiff(beforeDiff,afterDiffExpected);
    let testGraph=PropertyGraphModel.PropertyGraph.loadFromData(beforeDiff);
    PropertyGraphModel.applyDiff(testGraph,diff);
    assert.deepEqual(afterDiffExpected,testGraph.saveToData())
});

QUnit.test("apply diff add edge",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    let beforeDiff=propGraph.saveToData();
    let edge=new PropertyGraphModel.Edge(node1,node2);
    propGraph.addEdge(edge);
    let afterDiffExpected=propGraph.saveToData();
    
    let diff=PropertyGraphModel.genDiff(beforeDiff,afterDiffExpected);
    let testGraph=PropertyGraphModel.PropertyGraph.loadFromData(beforeDiff);
    PropertyGraphModel.applyDiff(testGraph,diff);
    assert.deepEqual(afterDiffExpected,testGraph.saveToData())
});

QUnit.test("apply diff remove edge",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    let edge=new PropertyGraphModel.Edge(node1,node2);
    propGraph.addEdge(edge);
    let beforeDiff=propGraph.saveToData();
    propGraph.deleteEdge(edge);
    let afterDiffExpected=propGraph.saveToData();
    
    let diff=PropertyGraphModel.genDiff(beforeDiff,afterDiffExpected);
    let testGraph=PropertyGraphModel.PropertyGraph.loadFromData(beforeDiff);
    PropertyGraphModel.applyDiff(testGraph,diff);
    assert.deepEqual(afterDiffExpected,testGraph.saveToData())
})

QUnit.test("apply diff edit edge",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    let edge=new PropertyGraphModel.TreeChildEdge(node1,node2);
    edge.index=1;
    propGraph.addEdge(edge);
    let beforeDiff=propGraph.saveToData();
    edge.index=2;
    let afterDiffExpected=propGraph.saveToData();

    let diff=PropertyGraphModel.genDiff(beforeDiff,afterDiffExpected);
    let testGraph=PropertyGraphModel.PropertyGraph.loadFromData(beforeDiff);
    PropertyGraphModel.applyDiff(testGraph,diff);
    assert.deepEqual(afterDiffExpected,testGraph.saveToData())
})

QUnit.test("generate reverse diff add node",function(assert){
    let oldGraph=new PropertyGraphModel.PropertyGraph()
    let newGraph=new PropertyGraphModel.PropertyGraph()
    let diff=new PropertyGraphModel.Diff();
    diff.nodesAdded=[{
        incomingEdges:[],
        outgoingEdges:[],
        objectId:1,
        type:"Node",
        properties:{}
    }
    ]
    PropertyGraphModel.applyDiff(newGraph,diff);
    let expectedReverseDiff=PropertyGraphModel.genDiff(newGraph.saveToData(),oldGraph.saveToData())
    assert.deepEqual(PropertyGraphModel.reverseDiff(diff),expectedReverseDiff)
})

QUnit.test("generate reverse diff delete node",function(assert){
    let oldGraph=new PropertyGraphModel.PropertyGraph()
    let node=new PropertyGraphModel.Node()
    oldGraph.addNode(node);

    let newGraph=oldGraph.copy()

    let diff=new PropertyGraphModel.Diff();
    diff.nodesDeleted=[{
        incomingEdges:[],
        outgoingEdges:[],
        objectId:1,
        type:"Node",
        properties:{}
    }
    ]
    PropertyGraphModel.applyDiff(newGraph,diff);
    let expectedReverseDiff=PropertyGraphModel.genDiff(newGraph.saveToData(),oldGraph.saveToData())
    assert.deepEqual(PropertyGraphModel.reverseDiff(diff),expectedReverseDiff)
})

QUnit.test("generate reverse diff edit node",function(assert){
    let oldGraph=new PropertyGraphModel.PropertyGraph()
    let node= new DataModels.TextDataModel();
    node.text="test1";
    oldGraph.addNode(node);

    let newGraph=oldGraph.copy()

    let diff=new PropertyGraphModel.Diff();
    diff.nodesChangedProperties=[{
        from:{
            incomingEdges:[],
            outgoingEdges:[],
            objectId:1,
            type:"TextDataModel",
            properties:{"text":"test1"}
        },
        to:{
            incomingEdges:[],
            outgoingEdges:[],
            objectId:1,
            type:"TextDataModel",
            properties:{"text":"test2"}
        }
    }]
    PropertyGraphModel.applyDiff(newGraph,diff);
    let expectedReverseDiff=PropertyGraphModel.genDiff(newGraph.saveToData(),oldGraph.saveToData())
    assert.deepEqual(PropertyGraphModel.reverseDiff(diff),expectedReverseDiff)
})

QUnit.test("generate reverse diff add edge",function(assert){
    let oldGraph=new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    oldGraph.addNode(node1);
    oldGraph.addNode(node2);

    let newGraph=oldGraph.copy()

    let diff = new PropertyGraphModel.Diff();
    diff.edgesAdded=[{
        sourceNode:node1.objectId,
        targetNode:node2.objectId,
        edgeId:1,
        type:"Edge",
        properties:{}
    }
    ]
    PropertyGraphModel.applyDiff(newGraph,diff);
    let expectedReverseDiff=PropertyGraphModel.genDiff(newGraph.saveToData(),oldGraph.saveToData())
    assert.deepEqual(PropertyGraphModel.reverseDiff(diff),expectedReverseDiff)
})

QUnit.test("generate reverse diff delete edge",function(assert){
    let oldGraph= new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    oldGraph.addNode(node1);
    oldGraph.addNode(node2);
    let edge=new PropertyGraphModel.Edge(node1,node2);
    oldGraph.addEdge(edge);

    let newGraph= oldGraph.copy();
    
    let diff = new PropertyGraphModel.Diff();
    diff.edgesDeleted=[{
        sourceNode:node1.objectId,
        targetNode:node2.objectId,
        edgeId:1,
        type:"Edge",
        properties:{}
    }
    ]
    PropertyGraphModel.applyDiff(newGraph,diff);
    let expectedReverseDiff=PropertyGraphModel.genDiff(newGraph.saveToData(),oldGraph.saveToData())
    assert.deepEqual(PropertyGraphModel.reverseDiff(diff),expectedReverseDiff)
})

QUnit.test("generate reverse diff edit edge",function(assert){
    let oldGraph= new PropertyGraphModel.PropertyGraph();
    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    oldGraph.addNode(node1);
    oldGraph.addNode(node2);
    let edge=new PropertyGraphModel.TreeChildEdge(node1,node2);
    edge.index=1;
    oldGraph.addEdge(edge);

    let newGraph= oldGraph.copy();

    let diff = new PropertyGraphModel.Diff();
    diff.edgesChangedProperties=[{
        from:{
            sourceNode:node1.objectId,
            targetNode:node2.objectId,
            edgeId:1,
            type:"TreeChildEdge",
            properties:{"index":1}
        },
        to:{
            sourceNode:node1.objectId,
            targetNode:node2.objectId,
            edgeId:1,
            type:"TreeChildEdge",
            properties:{"index":2}
        }
    }]
    PropertyGraphModel.applyDiff(newGraph,diff);
    let expectedReverseDiff=PropertyGraphModel.genDiff(newGraph.saveToData(),oldGraph.saveToData())
    assert.deepEqual(PropertyGraphModel.reverseDiff(diff),expectedReverseDiff)
})