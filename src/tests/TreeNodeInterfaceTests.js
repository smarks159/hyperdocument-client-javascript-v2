QUnit.module("Tree Node Interface Tests");
QUnit.test( "Append Child Node", function( assert ) {
    var expectedGraph= new PropertyGraphModel.PropertyGraph();
    var expectedNode = new PropertyGraphModel.Node();        
    var expectedChildNode = new PropertyGraphModel.Node();
    expectedChildNode.child=true;
    expectedGraph.addNode(expectedNode);
    expectedGraph.addNode(expectedChildNode);
    var expectedEdge=new PropertyGraphModel.TreeChildEdge(expectedNode,expectedChildNode);
    expectedEdge.index=0;
    expectedGraph.addEdge(expectedEdge);
    
    var testGraph= new PropertyGraphModel.PropertyGraph();
    var testNode= new PropertyGraphModel.Node();
    testGraph.addNode(testNode);
    var TreeNode=new TreeNodeInterface(testNode);
    var childNode=new PropertyGraphModel.Node();
    childNode.child=true;
    TreeNode.appendChild(childNode);              
    assert.deepEqual(testGraph,expectedGraph);
    assert.strictEqual(childNode.incomingEdges[0].index,0);
    
});

QUnit.test( "Append 2 Children", function( assert ) {
    var graph=new PropertyGraphModel.PropertyGraph();
    var parentNode= new PropertyGraphModel.Node();
    graph.addNode(parentNode);
    var TreeNode=new TreeNodeInterface(parentNode);
    var child1Node=new PropertyGraphModel.Node();
    var child2Node=new PropertyGraphModel.Node();
    TreeNode.appendChild(child1Node);
    TreeNode.appendChild(child2Node);
    assert.strictEqual(child1Node.incomingEdges[0].index,0);
    assert.strictEqual(child2Node.incomingEdges[0].index,1);
});

QUnit.test( "Test the Children Property", function( assert ) {
    var graph=new PropertyGraphModel.PropertyGraph();
    var parentNode= new PropertyGraphModel.Node();
    graph.addNode(parentNode);
    var TreeNode=new TreeNodeInterface(parentNode);
    var child1Node=new PropertyGraphModel.Node();
    var child2Node=new PropertyGraphModel.Node();
    TreeNode.appendChild(child1Node);
    TreeNode.appendChild(child2Node);
    var children=TreeNode.children;
    assert.strictEqual(children[0],child1Node);
    assert.strictEqual(children[1],child2Node);        
});

QUnit.test( "Test the Parent Property", function( assert ) {
    var graph=new PropertyGraphModel.PropertyGraph();
    var parentNode= new PropertyGraphModel.Node();
    graph.addNode(parentNode);
    var TreeNode=new TreeNodeInterface(parentNode);
    var child1Node=new PropertyGraphModel.Node();
    var child2Node=new PropertyGraphModel.Node();
    TreeNode.appendChild(child1Node);
    TreeNode.appendChild(child2Node);
    
    var TreeNodeChild1=new TreeNodeInterface(child1Node);
    var TreeNodeChild2=new TreeNodeInterface(child2Node);
    
    assert.strictEqual(TreeNodeChild1.parent,parentNode);
    assert.strictEqual(TreeNodeChild2.parent,parentNode);        
});

QUnit.test("Test the getNodeIndex method", function(assert){
    var graph=new PropertyGraphModel.PropertyGraph();
    var parentNode= new PropertyGraphModel.Node();
    graph.addNode(parentNode);
    var TreeNode=new TreeNodeInterface(parentNode);
    var child1Node=new PropertyGraphModel.Node();
    var child2Node=new PropertyGraphModel.Node();
    TreeNode.appendChild(child1Node);
    TreeNode.appendChild(child2Node);
    
    assert.strictEqual(new TreeNodeInterface(child1Node).getNodeIndex(),0);
    assert.strictEqual(new TreeNodeInterface(child2Node).getNodeIndex(),1);
});

QUnit.test("Test the appendNext method", function(assert){
    var graph=new PropertyGraphModel.PropertyGraph();
    var parentNode= new PropertyGraphModel.Node();
    graph.addNode(parentNode);
    var TreeNode=new TreeNodeInterface(parentNode);
    var child1Node=new PropertyGraphModel.Node();
    TreeNode.appendChild(child1Node);
    var ChildTreeNode=new TreeNodeInterface(child1Node);
    var child2Node=new PropertyGraphModel.Node();
    ChildTreeNode.appendNext(child2Node);        
    
    // calling appendNext at the end of the level, should be the same as appending the node to that level.
    assert.strictEqual(new TreeNodeInterface(child1Node).getNodeIndex(),0);
    assert.strictEqual(new TreeNodeInterface(child2Node).getNodeIndex(),1);
    
    var child3Node=new PropertyGraphModel.Node();
    ChildTreeNode.appendNext(child3Node);
    // calling appendNext in the middle of the level, should increase all of the indexes of the
    // nodes to the right by one.
    assert.strictEqual(new TreeNodeInterface(child1Node).getNodeIndex(),0);
    assert.strictEqual(new TreeNodeInterface(child3Node).getNodeIndex(),1);
    assert.strictEqual(new TreeNodeInterface(child2Node).getNodeIndex(),2);
});

QUnit.test("Test the appendBefore method", function(assert){
    var graph=new PropertyGraphModel.PropertyGraph();
    var parentNode= new PropertyGraphModel.Node();
    graph.addNode(parentNode);
    var TreeNode=new TreeNodeInterface(parentNode);
    var child1Node=new PropertyGraphModel.Node();
    TreeNode.appendChild(child1Node);
    var ChildTreeNode=new TreeNodeInterface(child1Node);
    var child2Node=new PropertyGraphModel.Node();
    ChildTreeNode.appendBefore(child2Node);        
    
    // calling appendNext at the beginning of the level, should increase all the indexes to the right of the node
    // being inserted including the currentNode        
    assert.strictEqual(new TreeNodeInterface(child2Node).getNodeIndex(),0);
    assert.strictEqual(new TreeNodeInterface(child1Node).getNodeIndex(),1);
    
    var child3Node=new PropertyGraphModel.Node();
    ChildTreeNode.appendBefore(child3Node);
    assert.strictEqual(new TreeNodeInterface(child2Node).getNodeIndex(),0);
    assert.strictEqual(new TreeNodeInterface(child3Node).getNodeIndex(),1);
    assert.strictEqual(new TreeNodeInterface(child1Node).getNodeIndex(),2);
    
});

QUnit.test("Test the delete method",function(assert){
    var graph=new PropertyGraphModel.PropertyGraph();
    var parentNode= new PropertyGraphModel.Node();
    graph.addNode(parentNode);
    var TreeNode=new TreeNodeInterface(parentNode);
    var child1Node=new PropertyGraphModel.Node();
    var child2Node=new PropertyGraphModel.Node();
    TreeNode.appendChild(child1Node);
    TreeNode.appendChild(child2Node);
    new TreeNodeInterface(child1Node).delete();
    assert.strictEqual(new TreeNodeInterface(child2Node).getNodeIndex(),0);
});

QUnit.test("Test the delete method with child",function(assert){
    var graph=new PropertyGraphModel.PropertyGraph();
    var parentNode= new PropertyGraphModel.Node();
    graph.addNode(parentNode);        
    var childNode=new PropertyGraphModel.Node();
    var grandChildNode=new PropertyGraphModel.Node();
    new TreeNodeInterface(parentNode).appendChild(childNode);
    new TreeNodeInterface(childNode).appendChild(grandChildNode);        
    new TreeNodeInterface(childNode).delete();
    assert.strictEqual(graph.nodes.length,1);
    assert.strictEqual(graph.edges.length,0);
});

QUnit.test("Test the replaceChildMethod",function(assert){
    var graph=new PropertyGraphModel.PropertyGraph();
    var parentNode= new PropertyGraphModel.Node();
    graph.addNode(parentNode);
    var TreeNode=new TreeNodeInterface(parentNode);
    var child1Node=new PropertyGraphModel.Node();
    var child2Node=new PropertyGraphModel.Node();
    TreeNode.appendChild(child1Node);
    TreeNode.appendChild(child2Node);
    var replacementNode=new PropertyGraphModel.Node();
    
    TreeNode.replaceChild(replacementNode,child2Node);
    
    assert.strictEqual(new TreeNodeInterface(child2Node).parent,null);
    assert.strictEqual(new TreeNodeInterface(replacementNode).parent,parentNode);
    assert.strictEqual(new TreeNodeInterface(replacementNode).getNodeIndex(),1);
});

QUnit.test("Test Copy Branch child",function(assert){
    var Graph=new PropertyGraphModel.PropertyGraph();
    var topNode=new DataModels.TextDataModel();        
    topNode.text="topNode";
    var nodeToCopy=new DataModels.TextDataModel();   
    nodeToCopy.text="nodeToCopy";
    var childToCopy=new DataModels.TextDataModel();      
    childToCopy.text="childToCopy";        
    Graph.addNode(topNode);
    new TreeNodeInterface(topNode).appendChild(nodeToCopy);
    var TreeNodeToClone=new TreeNodeInterface(nodeToCopy);
    TreeNodeToClone.appendChild(childToCopy);      
    
    var newGraph=new PropertyGraphModel.PropertyGraph();
    // add two nodes to make sure that the objectIds of the copied branch should not be the sames
    var node1=new PropertyGraphModel.Node();
    var node2=new PropertyGraphModel.Node();
    newGraph.addNode(node1);
    newGraph.addNode(node2);
    TreeNodeInterface.copyBranch(nodeToCopy,node1,"child");
    assert.strictEqual(new TreeNodeInterface(node1).children.length,1,"a child object should have been added to the target node");
    var clonedNode=new TreeNodeInterface(node1).children[0];
    assert.strictEqual(clonedNode.objectId,3,"a new objectId should be created for the copied object, it should not be the same as the source.");
    assert.notStrictEqual(clonedNode,nodeToCopy,"the copied object should be a copy, not a reference");
    assert.strictEqual(clonedNode.type,"TextDataModel","the type of the node should be the same");
    assert.strictEqual(clonedNode.text,"nodeToCopy","the data of the node should be the same");
    
    assert.strictEqual(new TreeNodeInterface(clonedNode).children.length,1,"The grand child should have been added to the clonedNode");
    var clonedChildNode=new TreeNodeInterface(clonedNode).children[0];
    assert.strictEqual(clonedChildNode.objectId,4,"the grand child should have a new objectId, not the same as the source");
    assert.notStrictEqual(clonedChildNode,childToCopy,"the copied grand child should be a copy, not a reference");
    assert.strictEqual(clonedChildNode.type,"TextDataModel","the copied grand child's type should be the same as the source");
    assert.strictEqual(clonedChildNode.text,"childToCopy","the copied grand child's data should be the same as the source");
});    

QUnit.test("Test move branch different property graphs",function(assert){
    var Graph=new PropertyGraphModel.PropertyGraph();
    var topNode=new DataModels.TextDataModel();        
    topNode.text="topNode";
    var nodeToCopy=new DataModels.TextDataModel();   
    nodeToCopy.text="nodeToCopy";
    var childToCopy=new DataModels.TextDataModel();      
    childToCopy.text="childToCopy";        
    Graph.addNode(topNode);
    new TreeNodeInterface(topNode).appendChild(nodeToCopy);
    var TreeNodeToClone=new TreeNodeInterface(nodeToCopy);
    TreeNodeToClone.appendChild(childToCopy);      
    
    var newGraph=new PropertyGraphModel.PropertyGraph();
    // add two nodes to make sure that the objectIds of the copied branch should not be the sames
    var node1=new PropertyGraphModel.Node();
    var node2=new PropertyGraphModel.Node();
    newGraph.addNode(node1);
    newGraph.addNode(node2);
    TreeNodeInterface.moveBranch(nodeToCopy,node1,"child");
    assert.strictEqual(Graph.nodes.length,1,"the src node should be removed from the source graph");
    
    // the rest is the same as copyBranch
    assert.strictEqual(new TreeNodeInterface(node1).children.length,1,"a child object should have been added to the target node");
    var clonedNode=new TreeNodeInterface(node1).children[0];
    assert.strictEqual(clonedNode.objectId,3,"a new objectId should be created for the copied object, it should not be the same as the source.");
    assert.notStrictEqual(clonedNode,nodeToCopy,"the copied object should be a copy, not a reference");
    assert.strictEqual(clonedNode.type,"TextDataModel","the type of the node should be the same");
    assert.strictEqual(clonedNode.text,"nodeToCopy","the data of the node should be the same");
    
    assert.strictEqual(new TreeNodeInterface(clonedNode).children.length,1,"The grand child should have been added to the clonedNode");
    var clonedChildNode=new TreeNodeInterface(clonedNode).children[0];
    assert.strictEqual(clonedChildNode.objectId,4,"the grand child should have a new objectId, not the same as the source");
    assert.notStrictEqual(clonedChildNode,childToCopy,"the copied grand child should be a copy, not a reference");
    assert.strictEqual(clonedChildNode.type,"TextDataModel","the copied grand child's type should be the same as the source");
    assert.strictEqual(clonedChildNode.text,"childToCopy","the copied grand child's data should be the same as the source");
    
});

QUnit.test("Test move branch same property graph",function(assert){
    var Graph=new PropertyGraphModel.PropertyGraph();
    var topNode=new DataModels.TextDataModel();        
    topNode.text="topNode";
    var childNode=new DataModels.TextDataModel();   
    childNode.text="childNode";
    var nodeToMove=new DataModels.TextDataModel();      
    nodeToMove.text="nodeToMove"; 
    var childToMove=new DataModels.TextDataModel();
    childToMove.text="childToMove";
    Graph.addNode(topNode);
    new TreeNodeInterface(topNode).appendChild(childNode);
    new TreeNodeInterface(childNode).appendChild(nodeToMove);
    new TreeNodeInterface(nodeToMove).appendChild(childToMove);
    
    TreeNodeInterface.moveBranch(nodeToMove,childNode,"next");
    assert.strictEqual(new TreeNodeInterface(nodeToMove).parent,topNode,"The parent edge of the node being move should be the parent of the target");
    assert.strictEqual(new TreeNodeInterface(childNode).children.length,0,"The parent edge to the previous parent of the node being mode should be removed");
    assert.strictEqual(nodeToMove.objectId,3,"The objectId should remain the same in the node being moved");
    assert.strictEqual(nodeToMove.text,"nodeToMove","The data should remain the same in the node being moved");        
    assert.strictEqual(childToMove.objectId,4,"The objectId should remain the same in the children of the node being moved");
    assert.strictEqual(childToMove.text,"childToMove","The data should remain the same in the children of the node being moved");
});