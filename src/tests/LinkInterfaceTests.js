QUnit.module("Link Interface Tests")

QUnit.test("addNewLink",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let linkInterface= new LinkInterface(propGraph)

    let srcNode= new PropertyGraphModel.Node();
    let targetNode= new PropertyGraphModel.Node();
    propGraph.addNode(srcNode);
    propGraph.addNode(targetNode);
    
    let newEdge=linkInterface.addLink(srcNode,targetNode);

    let edgeInDB=propGraph.getEdgeById(newEdge.edgeId);
    assert.notStrictEqual(edgeInDB,null,"Edge should be added to the database");
    assert.strictEqual(edgeInDB.type,"LinkEdge","The edge should be a LinkEdge type");

    assert.strictEqual(edgeInDB.sourceNode.objectId,srcNode.objectId,"The source of the link should be the source node passed into addLink");
    assert.strictEqual(edgeInDB.targetNode.objectId,targetNode.objectId,"The target of the link should be the target node passed into addLink");
})

QUnit.test("delete link",function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let linkInterface= new LinkInterface(propGraph)

    let srcNode= new PropertyGraphModel.Node();
    let targetNode= new PropertyGraphModel.Node();
    propGraph.addNode(srcNode);
    propGraph.addNode(targetNode);

    let linkEdge=linkInterface.addLink(srcNode,targetNode);

    linkInterface.deleteLink(linkEdge)

    assert.strictEqual(propGraph.getEdgeById(linkEdge.edgeId),null,"Edge should not exist in the database anymore");
})

