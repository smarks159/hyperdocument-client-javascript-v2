QUnit.module("Tags Interface Tests");

QUnit.test("create a new tag", function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let tagsInterface= new TagsInterface(propGraph);
    tagsInterface.createTag("test")

    // A tag is a property graph node in the database that contains a set of objectIds to which it points
    assert.strictEqual(propGraph.nodes.length,1,"A new tag node is added to the database")
    assert.strictEqual(propGraph.nodes[0] instanceof PropertyGraphModel.TagNode,true)
    
    // When creating a new tag it also create a new key in the tagIndex
    let tagsInIndex=Object.keys(propGraph.tagIndex)
    assert.strictEqual(tagsInIndex.length,1,"one new key should be added to the tags index")
    assert.strictEqual(tagsInIndex[0],"test","the new tag in the index should be the tag 'test'")

    assert.throws(()=>tagsInterface.createTag("test"),"Cannot create a tag that is already defined in the database. This would delete any of the current tag associations.");
})

QUnit.test("get tag by name", function(assert){
    let propGraph= new PropertyGraphModel.PropertyGraph();
    let tagsInterface= new TagsInterface(propGraph);
    tagsInterface.createTag("test")

    let tag=tagsInterface.getTagByName("test")

    assert.strictEqual(tag.objectId,propGraph.nodes[0].objectId,"should get the same tag as the one created");
    assert.strictEqual(tagsInterface.getTagByName("test2"),null,"If the tag does not exist return null.")
})

QUnit.test("create a tag with one node associated",function(assert){    
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let tagsInterface=new TagsInterface(propGraph);

    // A tag can be added using a node object
    tagsInterface.createTag("test")
    let node1=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    tagsInterface.addTagToNode("test",node1);
    let testTag=tagsInterface.getTagByName("test");    
    assert.strictEqual(testTag.has(node1.objectId),true)

    // A tag can be added using an objectId
    tagsInterface.createTag("test2")
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node2);
    tagsInterface.addTagToNode("test2",node2.objectId)
    testTag=tagsInterface.getTagByName("test2");    
    assert.strictEqual(testTag.has(node2.objectId),true);

    assert.throws(()=>tagsInterface.addTagToNode("test3",node1), "A tag must exist in the database when adding it to a node")

    let node3=new PropertyGraphModel.Node()
    assert.throws(()=>tagsInterface.addTagToNode("test",node3),"A node must be added to the database before adding a tag")
})

QUnit.test("add a single tag to multiple nodes",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let tagsInterface=new TagsInterface(propGraph);
    tagsInterface.createTag("test");

    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    tagsInterface.addTagToNode("test",node1);
    tagsInterface.addTagToNode("test",node2);

    let testTag=tagsInterface.getTagByName("test");    
    assert.strictEqual(testTag.numObjects(),2);
    assert.strictEqual(testTag.has(node1.objectId),true);
    assert.strictEqual(testTag.has(node2.objectId),true);

    assert.throws(()=>tagsInterface.addTagToNode("test",node1),"Cannot add the same node to the same tag twice");
})

QUnit.test("remove a tag from a node",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let tagsInterface=new TagsInterface(propGraph);
    tagsInterface.createTag("test");

    let node1=new PropertyGraphModel.Node();
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    propGraph.addNode(node2);
    tagsInterface.addTagToNode("test",node1);
    tagsInterface.addTagToNode("test",node2);
    
    // remove tag using node object
    tagsInterface.removeTagFromNode("test",node1);
    // remove tag using objectId
    tagsInterface.removeTagFromNode("test",node2.objectId);

    let testTag=tagsInterface.getTagByName("test");
    assert.strictEqual(testTag.numObjects(),0);

    assert.throws(()=>tagsInterface.removeTagFromNode("test",node1),"cannot remove a tag from a node that does not have that tag")

    assert.throws(()=>tagsInterface.addTagToNode("test3",node1), "A tag must exist in the database when removing from a node");

    let node3=new PropertyGraphModel.Node()
    assert.throws(()=>tagsInterface.addTagToNode("test",node3),"A node exist in the database before removing a tag");
})

QUnit.test("delete a tag",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let tagsInterface=new TagsInterface(propGraph);

    // delete a tag with no nodes associated with it
    tagsInterface.createTag("test");
    tagsInterface.deleteTag("test");
    assert.strictEqual(propGraph.nodes.length,0,"remove tag node from the database");
    assert.strictEqual(Object.keys(propGraph.tagIndex).length,0,"remove entry from index");


    // delete a tag with a node associated with it.
    tagsInterface.createTag("test");
    let node1=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    tagsInterface.addTagToNode("test",node1);
    tagsInterface.deleteTag("test");

    assert.strictEqual(propGraph.nodes.length,1,"remove tag node but not the node associated with it");
    assert.strictEqual(Object.keys(propGraph.tagIndex).length,0,"remove entry from index");

    assert.throws(()=>tagsInterface.deleteTag("test2"),"trying to delete a tag that does not exist");
})

QUnit.test("rename a tag",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let tagsInterface=new TagsInterface(propGraph);
    tagsInterface.createTag("test");

    testTag=tagsInterface.getTagByName("test");
    tagsInterface.renameTag("test","test1");

    assert.strictEqual(testTag.name,"test1","The name property of the tag should be changed");
    assert.strictEqual(Object.keys(propGraph.tagIndex).length,1,"The entry for the old tag name should be removed from the tagIndex and the entry for the new tag name should be added, leaving 1 entry in the index");
    assert.strictEqual(Object.keys(propGraph.tagIndex)[0],"test1","The entry in the tag index should match the new name");

    tagsInterface.createTag("test2");
    assert.throws(()=>tagsInterface.rename("test1","test2"),"cannot rename a node to a tag that already exists.");
})

QUnit.test("query nodes from a list of tags", function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let tagsInterface=new TagsInterface(propGraph);
    tagsInterface.createTag("test1");
    tagsInterface.createTag("test2");

    let node1=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node2);
    let node3=new PropertyGraphModel.Node();
    propGraph.addNode(node3);
    let node4=new PropertyGraphModel.Node();
    propGraph.addNode(node4);

    tagsInterface.addTagToNode("test1",node1);
    tagsInterface.addTagToNode("test2",node2);

    tagsInterface.addTagToNode("test1",node3);
    tagsInterface.addTagToNode("test2",node3);

    tagsInterface.addTagToNode("test1",node4);
    tagsInterface.addTagToNode("test2",node4);

    let results=tagsInterface.queryTagList("test1","test2");

    assert.strictEqual(results.length,2);
    assert.strictEqual(results[0].objectId,node3.objectId);
    assert.strictEqual(results[1].objectId,node4.objectId);    

})

QUnit.test("query nodes from a list of tags when one tag does not exist",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let tagsInterface=new TagsInterface(propGraph);
    tagsInterface.createTag("test1");

    let node1=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node2);

    tagsInterface.addTagToNode("test1",node1);
    tagsInterface.addTagToNode("test1",node2);

    let results=tagsInterface.queryTagList("test1","test2");
    assert.strictEqual(results.length,0,"When one tag does not exist, there should be no results");
})

QUnit.test("Test the tagIndex is serialized and deserialized properly.",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let tagsInterface=new TagsInterface(propGraph);
    tagsInterface.createTag("test1");
    tagsInterface.createTag("test2");

    let data=propGraph.saveToData();
    assert.deepEqual(data.tagIndex,{"test1":1,"test2":2},"When serialized, the tag index data is represented by a 'tagName':objectId pair");

    propGraph=PropertyGraphModel.PropertyGraph.loadFromData(data);
    assert.strictEqual(propGraph.tagIndex["test1"].objectId,1,"Loading from data should recreate the tagIndex where the value in a tag object");
    assert.strictEqual(propGraph.tagIndex["test2"].objectId,2,"Loading from data should recreate the tagIndex where the value in a tag object");
})

QUnit.test("Test the tag node serialization and deserialization",function(assert){
    let propGraph=new PropertyGraphModel.PropertyGraph();
    let tagsInterface=new TagsInterface(propGraph);
    tagsInterface.createTag("test1");

    let node1=new PropertyGraphModel.Node();
    propGraph.addNode(node1);
    let node2=new PropertyGraphModel.Node();
    propGraph.addNode(node2);

    tagsInterface.addTagToNode("test1",node1);
    tagsInterface.addTagToNode("test1",node2);

    let data=propGraph.saveToData();
    let tagData=data.nodes[0];
    assert.strictEqual(tagData.type,"Tag","The type of a tag node should be Tag")
    assert.deepEqual(tagData.properties,{name:"test1",nodesWithTag:[2,3]},"test the serialization of the properties");

    propGraph=PropertyGraphModel.PropertyGraph.loadFromData(data);
    let tagNode=propGraph.nodes[0];
    assert.strictEqual(tagNode.name,"test1","The properties should be the same as before");
    assert.deepEqual(Array.from(tagNode.nodesWithTag),[2,3],"The properties should be the same as before. Test an array of the set values because it is possible to directly test the values")
})