/* GUIDataActions do the following in addition to normal dataActions
        - Record the diff of before and after the GUI actions and place it on the undo stack.
        - take an additional parameter called visualData, which is the information required by undo or redo to indicate to the user what is being changed in the GUI. For instance when calling undo when typing text the cursor needs to be moved to the position of the character that is being changed.
*/
var GUIDataActions={};
var undoStack=[];
var redoStack=[];

/**
 * Given a document node, get the fileObject that the node is associated with. This is needed to store which file was changed by a given operation so it can be recorded in the visualData metadata.
 * @param node 
 */
GUIDataActions.getFileFromNode=function(node){
    let document=new TreeNodeInterface(node).findParentByType(DataModels.StructuredDocumentDataModel);
    return document.getFile();
}

GUIDataActions.moveBranch=function(src,target,position){
    let srcFileChanged=GUIDataActions.getFileFromNode(src).fileName;
    let targetFileChanged=GUIDataActions.getFileFromNode(target).fileName;
    let diff=dataActions.executeTransaction(()=>{
        dataActions.moveBranch(src,target,position);
    });
    
    dataActions.addToUndo({"diff":diff,"visualData":{type:"MoveBranch",node:src.objectId,"srcFileChanged":srcFileChanged,"targetFileChanged":targetFileChanged}});
}

GUIDataActions.moveRange=function(srcNodes,target,position){
    let srcFileChanged=GUIDataActions.getFileFromNode(srcNodes[0]).fileName;
    let targetFileChanged=GUIDataActions.getFileFromNode(target).fileName;
    let diff=dataActions.executeTransaction(()=>{
        dataActions.moveRange(srcNodes,target,position);
    });
    dataActions.addToUndo({"diff":diff,"visualData":{type:"MoveRange",nodes:srcNodes.map(node=>node.objectId),"srcFileChanged":srcFileChanged,"targetFileChanged":targetFileChanged}});
}

GUIDataActions.copyRange=function(srcNodes,target,position){
    let srcFileChanged=GUIDataActions.getFileFromNode(srcNodes[0]).fileName;
    let targetFileChanged=GUIDataActions.getFileFromNode(target).fileName;
    let newNodes;
    let diff=dataActions.executeTransaction(()=>{
        newNodes=dataActions.copyRange(srcNodes,target,position);
    });
    dataActions.addToUndo({"diff":diff,"visualData":{type:"CopyRange",newNodes:newNodes.map(node=>node.objectId),oldNodes:srcNodes.map(node=>node.objectId),"srcFileChanged":srcFileChanged,"targetFileChanged":targetFileChanged}});
    return newNodes;
}

GUIDataActions.insertNode=function(targetNode,newNode,position){
    let fileChanged=GUIDataActions.getFileFromNode(targetNode).fileName;
    let diff=dataActions.executeTransaction(()=>{
        dataActions.insertNode(targetNode,newNode,position);
    });
    dataActions.addToUndo({"diff":diff,"visualData":{type:"InsertNode",newNode:newNode.objectId,targetNode:targetNode.objectId,"fileChanged":fileChanged}})
}

GUIDataActions.insertTextAtNode=function(textToInsert,targetNode,position){
    let fileChanged=GUIDataActions.getFileFromNode(targetNode).fileName;
    let firstNode;
    let diff=dataActions.executeTransaction(()=>{
        let topLevelNodes=dataActions.insertTextAtNode(textToInsert,targetNode,position);
        firstNode=topLevelNodes[0];
    });
    dataActions.addToUndo({"diff":diff,"visualData":{type:"InsertNode",newNode:firstNode.objectId,targetNode:targetNode.objectId,"fileChanged":fileChanged}})
    return firstNode;
}

GUIDataActions.insertHTMLAtNode=function(textToInsert,targetNode,position){
    let fileChanged=GUIDataActions.getFileFromNode(targetNode).fileName;
    let firstNode;
    let diff=dataActions.executeTransaction(()=>{
        let topLevelNodes=dataActions.insertHTMLAtNode(textToInsert,targetNode,position);
        firstNode=topLevelNodes[0];
    });
    dataActions.addToUndo({"diff":diff,"visualData":{type:"InsertNode",newNode:firstNode.objectId,targetNode:targetNode.objectId,"fileChanged":fileChanged}})
    return firstNode;
}

GUIDataActions.createFileFromText=function(fileName,textString){
    let newFile;
    let diff=dataActions.executeTransaction(()=>{
        newFile=dataActions.addFile(fileName,false);
        dataActions.insertTextAtNode(textString,newFile.fileContent,"child");
    });
    dataActions.addToUndo({"diff":diff,"visualData":{"type":"createFile","fileData":newFile}});
    return newFile;
}

GUIDataActions.deleteNode=function(node){
    let fileChanged=GUIDataActions.getFileFromNode(node).fileName;
    let diff=dataActions.executeTransaction(()=>{
        dataActions.deleteNode(node);
    });
    dataActions.addToUndo({"diff":diff,"visualData":{type:"DeleteNode",node:node.objectId,"fileChanged":fileChanged}})
}

GUIDataActions.deleteRange=function(nodeList){
    let fileChanged=GUIDataActions.getFileFromNode(nodeList[0]).fileName;
    let diff=dataActions.executeTransaction(()=>{
        dataActions.deleteRange(nodeList)
    });
    dataActions.addToUndo({"diff":diff,"visualData":{type:"DeleteRange",nodes:nodeList.map(node=>node.objectId),"fileChanged":fileChanged}});
}

GUIDataActions.updateProseMirrorData=function(dataNode,proseMirrorState){
    let diff=new PropertyGraphModel.Diff();
    let oldDataNode=dataNode.saveToData();
    let newDataNode=dataNode.saveToData();
    newDataNode.properties.doc=proseMirrorState.doc.toJSON();
    diff.nodesChangedProperties=[{"from":oldDataNode,"to":newDataNode}];
    PropertyGraphModel.applyDiff(wikiDesktop.wikiData.propertyGraph,diff);
    let fileName=GUIDataActions.getFileFromNode(dataNode).fileName;
    
    dataActions.addToUndo({"diff":PropertyGraphModel.reverseDiff(diff),"visualData":{type:"updateProseMirrorData",node:dataNode.objectId,"selection":proseMirrorState.selection.toJSON(),"fileChanged":fileName}});
}

GUIDataActions.createFile=function(fileName,addBlankNode=true){
    let newFile;
    let diff=dataActions.executeTransaction(()=>{
        newFile=dataActions.addFile(fileName,addBlankNode);
    });    
    let fileData=wikiDesktop.wikiData.getFileObjectByName(fileName);
    dataActions.addToUndo({"diff":diff,"visualData":{"type":"createFile","fileData":fileData}})
    return newFile;
}

GUIDataActions.removeFile=function(fileName){
    let fileData=wikiDesktop.wikiData.getFileObjectByName(fileName);
    let diff=dataActions.executeTransaction(()=>{
        dataActions.removeFile(fileName);
    });    
    dataActions.addToUndo({"diff":diff,"visualData":{"type":"removeFile","fileData":fileData}});
}

GUIDataActions.renameFile=function(fileObject,newName){
    let oldName=fileObject.fileName;
    let diff=dataActions.executeTransaction(()=>{
        dataActions.renameFile(fileObject,newName);
    });   
    dataActions.addToUndo({"diff":diff,"visualData":{"type":"renameFile","oldName":oldName,"newName":newName}});
}

GUIDataActions.splitText=function(textPresenter){
    let targetNode=textPresenter.dataNode;
    let fileChanged=GUIDataActions.getFileFromNode(targetNode).fileName;
    let newNode=new DataModels.ProseMirrorDataModel();
    let diff=dataActions.executeTransaction(()=>{
        let cursorPos=textPresenter.textEditorEl.getCursorStartPos();
        // Need to get the slice to get the proper size of the range to delete and getNodeSubset to get a document with the contents the same as the slice. 
        let sliceToSplit=textPresenter.textEditorEl.getSlice(cursorPos);
        let docToSplit=textPresenter.textEditorEl.getNodeSubset(cursorPos);

        // must update state of text presenter directly because dispatching a transaction in the ProseMirrorLeaf fragment calls GUIDataActions.updateProseMirrorData model, which would create a separate undo entry for deleting the split text in the current mode and creating a new node with the split text. Only one entry in the undo should be presenter for the entire split text operation. So, the presenter is updated here in a way that does not call dispatch.
        let editor=textPresenter.textEditorEl.editor;
        let toKeepState=editor.state
        let tr=toKeepState.tr
        tr.deleteRange(cursorPos,cursorPos+sliceToSplit.size)
        let newState=toKeepState.apply(tr);
        editor.updateState(newState)

        dataActions.updateProseMirrorData(targetNode,newState);
        newNode.doc=docToSplit;
        dataActions.insertNode(targetNode,newNode,"next");
    })
    dataActions.addToUndo({"diff":diff,"visualData":{type:"InsertNode",fileChanged:fileChanged,newNode:newNode.objectId,targetNode:targetNode.objectId}})
    return newNode
}
/**
 * The undo/redo function expects the file being updated to be visible and the window in which the file is contained to be selected. When execute undo/redo this is not always the case, so we must first select the appropriate window and show the file if necessary before updating the Gui.
 * @param {String} fileName 
 */

GUIDataActions.selectFileInGui=function(fileChanged){
    let allWindows=wikiDesktop.topWindowPanel.windowManager.getAllWindows();
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    // If the file to update is not loaded in the currently selected window
    if(selectedWindow.windowName!==fileChanged){
        let windowToSelect=null;
        // Is the file currently open in another window, if so selected that
        for (const window of allWindows.filter(window=>window.isVisible())) {
            if(window.windowName===fileChanged){
                windowToSelect=window;
                break;
            }
        }
        if(windowToSelect!==null){
            GuiActions.WikiActions.selectWindow(windowToSelect.getWindowId());
        }
        // If the file is not currently open in any visible window. Select the file in the currently selected window
        else{
            GuiActions.WikiActions.selectFile(fileChanged);
        }
    }
}

/**
 * When updating the visual, the presenter must be drawn on the screen. If it is not visible when undo/redo is called then jump to the parent node of the node to change and expand it so the child is drawn. Jumping to the parent node instead of the child is necessary to handle insertion and deletion of nodes in addition to editing a node.
 * 
 * @param {Presenters.StructredDocumentPresenter} - The document presenter containing the node
 * @param {PropertyGraphModel.Node} - the node to jump to.
 */
GUIDataActions.jumpToParentAndExpand=function(document,node){
    let parent=new TreeNodeInterface(node).parent;
    GuiActions.StructuredDocumentActions.jumpToNode(document,parent);
    if(!(parent instanceof DataModels.StructuredDocumentDataModel)){
        let presenter=document.findFirstChildByObjectId(parent.objectId);
        GuiActions.StructuredDocumentActions.expandPresenter(presenter);
    } 
}

GUIDataActions.getDocumentOfSelectedWindow=function(){
    let window=GuiActions.WikiActions.getSelectedWindow();
    let doc=window.findFirstChildByType(Presenters.StructuredDocumentPresenter);
    return doc;
}

GUIDataActions.isPresenterDrawn=function(doc,node){
    if(doc.findFirstChildByObjectId(node.objectId)){
        return true;
    }
    else{
        return false;
    }
}

/**
 * This function changes the visual state of the GUI back to where it was when executing an undo or redo. The metadata required to update the GUI is stored in visualData. This is used for things like updating the cursor postion when undoing typing a character or updating the selected node.
 * @param visualData - metadata required to update the visual elements of the GUI properly. The visualData has a type attribute which specifies the operation that was executed to change the GUI. The visualData updates the GUI based on the operation type.
 */
GUIDataActions.updateVisual=function(visualData){
    // After the graph is changed the node object in the visualData might not be the same object as the one currently in the property graph, so when must get all nodes by there objectId.
    let propGraph=wikiDesktop.wikiData.propertyGraph;
    if(visualData.type==="MoveBranch"){
        let node=propGraph.getNodeById(visualData.node)
        wikiDesktop.topWindowPanel.windowManager
            .getAllWindows()
            .forEach(window=>window.refresh());
        let fileToSelect=visualData.isUndo ? visualData["srcFileChanged"]: visualData["targetFileChanged"];
        GUIDataActions.selectFileInGui(fileToSelect);
        let doc=GUIDataActions.getDocumentOfSelectedWindow();
        if(!GUIDataActions.isPresenterDrawn(doc,node)){
            GUIDataActions.jumpToParentAndExpand(doc,node);
        }
        GuiActions.WikiActions.setSelectedNode(node);
    }
    else if(visualData.type==="MoveRange"){
        let startNode=propGraph.getNodeById(visualData.nodes[0]);
        let endNode=propGraph.getNodeById(visualData.nodes.slice(-1)[0]);
        wikiDesktop.topWindowPanel.windowManager
            .getAllWindows()
            .forEach(window=>window.refresh());
        GuiActions.WikiActions.unselectRange();
        let fileToSelect=visualData.isUndo ? visualData["srcFileChanged"]: visualData["targetFileChanged"];
        GUIDataActions.selectFileInGui(fileToSelect);
        let doc=GUIDataActions.getDocumentOfSelectedWindow();
        if(!GUIDataActions.isPresenterDrawn(doc,startNode)){
            GUIDataActions.jumpToParentAndExpand(doc,startNode);
        }
        GuiActions.WikiActions.setSelectedRange(startNode,endNode);
        
    }
    else if(visualData.type==="InsertNode"){
        let node;
        if(visualData.isUndo){
            node=propGraph.getNodeById(visualData.targetNode)
        }
        else{
            node=propGraph.getNodeById(visualData.newNode)
        }
        wikiDesktop.topWindowPanel.windowManager
            .getAllWindows()
            .forEach(window=>window.refresh());
        GUIDataActions.selectFileInGui(visualData.fileChanged);
        let doc=GUIDataActions.getDocumentOfSelectedWindow();
        if(!GUIDataActions.isPresenterDrawn(doc,node)){
            GUIDataActions.jumpToParentAndExpand(doc,node);
        }
        GuiActions.WikiActions.setSelectedNode(node);
    }
    else if(visualData.type==="DeleteNode"){
        wikiDesktop.topWindowPanel.windowManager
            .getAllWindows()
            .forEach(window=>window.refresh());
        GUIDataActions.selectFileInGui(visualData.fileChanged);
        if(visualData.isUndo){
            let node=propGraph.getNodeById(visualData.node)
            let doc=GUIDataActions.getDocumentOfSelectedWindow();
            if(!GUIDataActions.isPresenterDrawn(doc,node)){
                GUIDataActions.jumpToParentAndExpand(doc,node);
            }
            GuiActions.WikiActions.setSelectedNode(node);
        }
        else{
            wikiDesktop.selectedNode=null;
            wikiDesktop.selectedRange.clearRange();
        }
    }
    else if(visualData.type==="DeleteRange"){
        wikiDesktop.topWindowPanel.windowManager
            .getAllWindows()
            .forEach(window=>window.refresh());
        GuiActions.WikiActions.unselectRange();
        GUIDataActions.selectFileInGui(visualData.fileChanged);
        if(visualData.isUndo){
            let startNode=propGraph.getNodeById(visualData.nodes[0]);
            let endNode=propGraph.getNodeById(visualData.nodes.slice(-1)[0]);
            let doc=GUIDataActions.getDocumentOfSelectedWindow();
            if(!GUIDataActions.isPresenterDrawn(doc,startNode)){
                GUIDataActions.jumpToParentAndExpand(doc,startNode);
            }
            GuiActions.WikiActions.setSelectedRange(startNode,endNode);
        }
        else{
            wikiDesktop.selectedNode=null;
            wikiDesktop.selectedRange.clearRange();
        }
    }
    else if(visualData.type==="CopyRange"){        
        let startNode,endNode;
        wikiDesktop.topWindowPanel.windowManager
            .getAllWindows()
            .forEach(window=>window.refresh());
        let fileToSelect=visualData.isUndo ? visualData["srcFileChanged"]: visualData["targetFileChanged"];
        GUIDataActions.selectFileInGui(fileToSelect);
        if(visualData.isUndo){
            startNode=propGraph.getNodeById(visualData.oldNodes[0]);
            endNode=propGraph.getNodeById(visualData.oldNodes.slice(-1)[0]);
        }
        else{
            startNode=propGraph.getNodeById(visualData.newNodes[0]);
            endNode=propGraph.getNodeById(visualData.newNodes.slice(-1)[0]);
        }
        let doc=GUIDataActions.getDocumentOfSelectedWindow();
        if(!GUIDataActions.isPresenterDrawn(doc,startNode)){
            GUIDataActions.jumpToParentAndExpand(doc,startNode);
        }
        GuiActions.WikiActions.setSelectedRange(startNode,endNode);
        
    }
    else if(visualData.type==="updateProseMirrorData"){
        // Refreshing the entire window is way too slow for undo/redo inside of a text edit. Instead only refresh the nodes that have data that changes.
        let node=propGraph.getNodeById(visualData.node);
        for(const window of wikiDesktop.topWindowPanel.windowManager.getAllWindows()){
            let textNode=window.findFirstChildByObjectId(node.objectId);
            if(textNode){
                textNode.presenter.refresh();
            }
        }
        GUIDataActions.selectFileInGui(visualData["fileChanged"]);
        let doc=GUIDataActions.getDocumentOfSelectedWindow();
        if(!GUIDataActions.isPresenterDrawn(doc,node)){
            GUIDataActions.jumpToParentAndExpand(doc,node);
        }
        GuiActions.WikiActions.setSelectedNode(node);
        let presenter=doc.findFirstChildByObjectId(node.objectId).presenter;
        // This code sets the cursor position, by restoring the selection state in the prosemirror editor. The selection in the visual data is disconnected from a prosemirror document on refresh, so we must create a new selection first that is associate with the current document.
        let selection=Selection.fromJSON(presenter.textEditorEl.editor.state.doc,visualData.selection);        
        presenter.textEditorEl.setSelection(selection);
        presenter.focus();
    }
    else if(visualData.type==="createFile"){
        // this is the same as deleting a file, so we update the GUI state the same as the deleteFile wikiAction
        if(visualData.isUndo){
            GuiActions.WikiActions.clearFileGUIState(visualData.fileData);
        }
        else {
            wikiDesktop.topWindowPanel.filePanelPresenter.refresh();
        }
    }
    else if(visualData.type==="renameFile"){
        let oldName,newName;
        if(visualData.isUndo){
            oldName=visualData.newName;
            newName=visualData.oldName;
        }
        else{
            oldName=visualData.oldName;
            newName=visualData.newName;
        }
        wikiDesktop.topWindowPanel.filePanelPresenter.refresh();
        let windows=wikiDesktop.topWindowPanel.windowManager.getAllWindows();
        for(const window of windows){
            if(window.windowName===oldName){
                window.setWindowName(newName);
            }
        }
    }
    else if(visualData.type==="removeFile"){
        // redoing a deleteFile command is the same as deleting a file
        if(visualData.isUndo===false){
            GuiActions.WikiActions.clearFileGUIState(visualData.fileData);
        }
        else{
            wikiDesktop.topWindowPanel.filePanelPresenter.refresh();
        }
    }
    else{
        throw Error(`Visual Data Type:${visualData.type} not supported`);
    }
}

GUIDataActions.undo=function(){
    let undoData=undoStack.pop();
    
    if(undoData){             
        let propGraph=wikiDesktop.wikiData.propertyGraph;
        PropertyGraphModel.applyDiff(propGraph,undoData.diff);
        GUIDataActions.updateVisual(undoData.visualData);
        undoData.visualData.isUndo=false;
        redoStack.push({"diff":PropertyGraphModel.reverseDiff(undoData.diff),"visualData":undoData.visualData});
    }
}

GUIDataActions.redo=function(){
    let redoData=redoStack.pop();
    if(redoData){

        let propGraph=wikiDesktop.wikiData.propertyGraph;
        PropertyGraphModel.applyDiff(propGraph,redoData.diff);
        GUIDataActions.updateVisual(redoData.visualData);
        redoData.visualData.isUndo=true;
        undoStack.push({"diff":PropertyGraphModel.reverseDiff(redoData.diff),"visualData":redoData.visualData}); 
    }
}