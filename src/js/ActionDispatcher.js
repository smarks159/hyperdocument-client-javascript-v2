class ActionDispatcher {
    /**
     * An action dispatcher maps an action name to a function to call. A seperate callback function can be passed in that is called after each action function call.
     * @param nameToAction - A object that maps names to an action function
     * @param callback - A callback function that is called after the action function is called.
     */
    constructor(nameToAction={},callback=null){
        this.nameToAction=nameToAction;
        this.callback=callback;
    }
    
    addAction(name,callback){
        this.nameToAction[name]=callback;
    }

    addCallback(callback){
        this.callback=callback;
    }
    
    execute(actionName,...args){
        if(!(actionName in this.nameToAction)){
            throw new Error(`The action:${actionName} does not exist in the action dispatcher`);
        }
        let results=this.nameToAction[actionName](...args)
        if(this.callback !== null){
            this.callback();
        }
        return results;
    }
}