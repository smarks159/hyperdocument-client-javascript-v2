Presenters.StructuredDocumentNodePresenter=class StructuredDocumentNodePresenter extends Presenters.Presenter{
    constructor(dataNode,nLevelsDown){
        super();
        this.dataNode=dataNode;
        this.nLevelsDown=nLevelsDown;
        this.presenter=null;
    }
    
    definition(){
        let treeNode=new TreeNodeInterface(this.dataNode);
        let col;
        if(treeNode.hasChildren()){            
            if(this.nLevelsDown>0){
                this.expanded=true;
            }
            col=this.column(
            [this.row([
                this.defAddressDisplay(),
                this.toggleDefinition().css({"margin-right":'5px'}),
                this.definePresenter()
                ]).css({"margin-bottom":'0.5em'})
            ]);
            
            if(this.expanded){
                let childDefs=this.column([]).css({"margin-left":'2em'});
                for(let child of treeNode.children){
                    let nLevelsDownChild=this.nLevelsDown>0 ? this.nLevelsDown-1 : this.nLevelsDown;
                    childDefs.appendChild(new Presenters.StructuredDocumentNodePresenter(child,nLevelsDownChild));
                }
                col.appendChild(childDefs);
            }            
        }
        else{
            col=this.column([this.row([
                this.defAddressDisplay(),
                this.definePresenter().css({"margin-bottom":'0.5em'})
            ])]);            
        }        
        
        if(this.highlighted){
            col.addClass("highlighted");
        }
        
        let index=wikiDesktop.clipboardObjects.indexOf(this.dataNode.objectId);
        // If there is more than one object in the clipboard one border should be drawn around the entire range instead of an individual border for each node.
        if(index!==-1){
            if(wikiDesktop.copyClipboardObject){
                col.addClass("copy-document-node")
            }
            else{
                col.addClass("cut-document-node")
            }
            if(wikiDesktop.clipboardObjects.length!==1){
                if(index===0){
                    col.addClass("no-border-bottom");
                }
                else if(index===wikiDesktop.clipboardObjects.length-1){
                    col.addClass("no-border-top");
                }
                else{
                    col.addClass("no-border-bottom");
                    col.addClass("no-border-top");
                }
                
            }
        }
        col.onMouseDown((e)=>{ GuiActions.WikiActions.documentNodeOnMouseDownCallback(e,this)});
        return col;
    }
    
    definePresenter(){
        this.presenter=createPresenterFromDataModel(this.dataNode);
        this.presenter.documentNodePresenter=this;
        return this.presenter;
    }    
    
    focus(){
        this.presenter.focus();
    }
    
    toggleDefinition(){
        if(this.expanded){
            return this.textLabel("&#10134").css({'font-weight':'bold','font-size':"0.9em"})
            .onClick(()=>{this.toggleAction()});
        }
        else{
            return this.textLabel("&#10133").css({'font-weight':'bold','font-size':"0.9em"})
            .onClick(()=>{this.toggleAction()});
        }
    }

    defAddressDisplay(){
        let windowId=this.findFirstParentByType(Presenters.SingleNodeWindow).getWindowId()
        let docViewModel=wikiDesktop.documentViewModels[windowId];
        let row=[];
        if (docViewModel.showStructuralPath){
            let doc=this.getDocumentPresenter().dataNode;
            let structuralPath=doc.getNodeStructuralAddress(this.dataNode);
            row.push(this.textLabel(structuralPath).css({"color":"green","font-weight":"bold"}));
            row.push(this.blank(5));
        }
        if(docViewModel.showId){            
            row.push(this.textLabel("0"+this.dataNode.objectId).css({"color":"green","font-weight":"bold"}));
            row.push(this.blank(5));
        }
        if(docViewModel.showRelativePath){
            let docPresenter=this.getDocumentPresenter()
            let doc=docPresenter.dataNode;
            let rootDisplay=docPresenter.rootDisplayNode;
            let relativePath=doc.getNodeRelativeAddress(this.dataNode,rootDisplay);
            row.push(this.textLabel(relativePath).css({"color":"green","font-weight":"bold"}));
            row.push(this.blank(5));
        }

        return this.row(row);
    }
    
    toggleAction(){
        if(this.expanded){
            this.expanded=false;
        }
        else{
            this.expanded=true;
        }
        this.refresh();
    }
    
    get expanded(){
        let defaultValue=false;
        let document=this.getDocumentPresenter()
        if(this.dataNode===null || 
           this.document===null ||           
           !("expanded" in document.getPresenterAttributes(this.dataNode))
        ){
            return defaultValue
        }       
        return document.getPresenterAttributes(this.dataNode)["expanded"];
    }
    
    set expanded(value){
        this.getDocumentPresenter().getPresenterAttributes(this.dataNode)["expanded"]=value;        
    }
    
    getDocumentPresenter(){
        return this.findFirstParentByType(Presenters.StructuredDocumentPresenter);
    }
    
    highlight(){
        this.highlighted=true;
        if(this.definitionObj){
            this.definitionObj.addClass("highlighted");
        }
    }
    
    unhighlight(){
        this.highlighted=false;
        if(this.definitionObj){
            this.definitionObj.removeClass("highlighted");
        }
    }
}   
