Presenters.SelectDropdownPresenter=class SelectDropdownPresenter extends Presenters.Presenter{
    constructor(buttonText){
        super();
        this._choices=[];
        this.buttonText=buttonText
    }
    
    // The function takes a list of choices that contain a map with the keys "text" and "callback"
    choices(choiceArray){
        this._choices=choiceArray;
        return this;
    }
    
    definition(){
        return this.column([
            this.defButton(),
            this.defDropDown().hide()
        ])
    }
    
    defButton(){
        this._button=this.button().text(this.buttonText).onClick(()=>{this.toggleDropDown()})
        return this._button;
    }
    
    defDropDown(){
        this._dropDown=new Presenters.ContextMenuPresenter();
        // close the dropDown if the user clicks outside of the Select Presenter rather than just outside the dropdown. This prevents the dropdown from closing immediately without breaking the behavior of other popups.
        this._dropDown.closePopupOnClickOutside=(event)=>{
            if(!$(event.target).closest(this.htmlEl).length) {
                if($(this.htmlEl).is(":visible")){
                    this._dropDown.closePopup();
                    $(document).off("click",this._dropDown.closePopupOnClickOutside);
                }   
            }
        }
        return this._dropDown;
    }
    
    toggleDropDown(){
        if(!this._dropDown.isVisible()){
            let contextMenuChoices=[];
            for(let choice of this._choices){
                contextMenuChoices.push({
                    text:choice.text,
                    callback:()=>{
                        this._dropDown.closePopup();
                        choice.callback();
                    }
                })
            }
            this._dropDown.choices(contextMenuChoices);
            this._dropDown.openAsPopup(this);
        }
        else{
            this._dropDown.closePopup();
        }
    }
}