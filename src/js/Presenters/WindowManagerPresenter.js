"use strict";

/**
 * The WindowManagerPresenter handles maintaining the state of windows in the system and is responsible for rendering the window layout. 
 * Currently, the window manager only supports a left window and a right window.
 */
Presenters.WindowManagerPresenter=class WindowManagerPresenter extends Presenters.Presenter{
    constructor(){
        super();
        this.leftWindow=new Presenters.SingleNodeWindow()
            .flex()
            .windowId("left");
        this.rightWindow=new Presenters.SingleNodeWindow()
            .flex()
            .hide()
            .windowId("right");
        this.windowSelected="left";
    }

    definition(){
        let row=[];
        if(this.leftWindow.isVisible()){
            row.push(this.leftWindow);
        }
        if(this.rightWindow.isVisible() && this.leftWindow.isVisible()){
            row.push(this.blank(5));
        }
        if(this.rightWindow.isVisible()){
            row.push(this.rightWindow)
        }
        if(this.windowSelected){
            this.selectWindow(this.windowSelected);
        }
        return this.row(row).flex();
    }

    selectWindow(windowId){
        this.windowSelected=windowId;
        if(windowId==="left" && this.leftWindow.isVisible()){
            this.leftWindow.selectWindow();
            this.rightWindow.unselectWindow();
        }
        else if(windowId==="right" && this.rightWindow.isVisible()){
            this.rightWindow.selectWindow();
            this.leftWindow.unselectWindow();
        }
    }

    /**
     * Returns a list of window objects
     */
    getAllWindows(){
        return [this.leftWindow,this.rightWindow];
    }

    getWindowById(windowId){
        if(windowId==="left"){
            return this.leftWindow;
        }
        else if(windowId==="right"){
            return this.rightWindow;
        }
    }

    saveNodeToData(){
        let data={
            "leftWindow":this.leftWindow.saveNodeToData(),
            "rightWindow":this.rightWindow.saveNodeToData(),
            "windowSelected":this.windowSelected
        }
        return data;
    }

    resetState(){
        this.windowSelected="left";
        this.rightWindow.setWindowName(null);
        this.rightWindow.dataNode=null;
        this.leftWindow.setWindowName(null);
        this.leftWindow.dataNode=null;
    }

    static loadFromData(data){
        let newObj= new Presenters.WindowManagerPresenter();
        // The flex and add class attributes are not preserved so they must be set again here
        newObj.leftWindow=Presenters.SingleNodeWindow.loadFromData(data["leftWindow"]).flex().addClass("panel");
        newObj.rightWindow=Presenters.SingleNodeWindow.loadFromData(data["rightWindow"]).flex().addClass("panel");
        newObj.leftWindow.parent=newObj;
        newObj.rightWindow.parent=newObj;
        newObj.windowSelected=data["windowSelected"];
        return newObj;
    }
}