Presenters.ContextMenuPresenter=class ContextMenuPresenter extends Presenters.Presenter{
    constructor(){
        super();
        this._choices=[];
        this.inputEl=null;
        this.listEl=null;
        this.filter="";
        this._onCloseCallback=null;
        this.keyboardListener=null;
    }
    
    choices(choicesArray){
        this._choices=choicesArray;
        return this;
    }
    
    definition(){
        return this.column([
            this.textLabel("Commands").css({
                "font-weight":"bold",
                "margin-bottom":"0.5em"
            }),            
            this.defineSearchBox().css({"margin-bottom":"0.2em"}),            
            this.defineList()
        ])    
    }
    
    defineSearchBox(){
        this.inputEl=this.input().autocomplete("off").onInput(()=>{
            this.filter=this.inputEl.getValue();            
            this.listEl.children=this.defineListChildren()
            this.listEl.refresh();
        }).value(this.filter);
        return this.inputEl;
    }
    
    defineListChildren(){
        let toRender;
        if(this.filter!==""){
            toRender=this._choices.filter((choice)=>{return choice["text"].toLowerCase().startsWith(this.filter)});
        }
        else{
            toRender=this._choices;
        }
        toRender.sort((a,b)=>a["text"].localeCompare(b["text"]))
        let listEls=[];
        
        toRender.forEach((choice)=>{
            listEls.push(
                this.textLabel(choice["text"]).css({
                    "margin-top":"4px"
                })
                .onClick((event)=>{
                    choice["callback"]();
                    // Event propagation causes problems when the callback closes the contextMenu and opens an inputForm dialog. The inputForm will be closed.
                    event.stopPropagation();
                })
                .onMouseOver((event)=>{
                    event.currentTarget.classList.add("highlighted");
                })
                .onMouseOut((event)=>{
                    event.currentTarget.classList.remove("highlighted");
                    event.currentTarget.style.color="";
                })
            );
        });
        return listEls;
    }
    
    defineList(){
        this.listEl=this.column(this.defineListChildren());        
        return this.listEl;
    }
    
    openAsPopup(offset,width=null){
        super.openAsPopup(offset,width);
        this.inputEl.focus();
    }
    
    openAsPopupWithCallbacks(offset,onOpenCallback,onCloseCallback){
        this._onCloseCallback=onCloseCallback;
        this.openAsPopup(offset);
        onOpenCallback();
    }
    
    closePopup(){  
        if(this._onCloseCallback){            
            this._onCloseCallback();
            this._onCloseCallback=null;
        }
        this.filter="";
        super.closePopup();
    }
    
    _initJavascript(){
        this.keyboardListener=createKeypress(this.htmlEl);
        this.keyboardListener.register_many([
            {
                "keys":"esc",
                "is_exclusive":true,
                "on_keyup":(e)=>{
                    this.closePopup();
                }
            }
        ])
    }

    _destroyJavascript(){
        this.keyboardListener.destroy();
        this.keyboardListener=null;
    }
}