"use strict";

Presenters.FileObjectPresenter=class FileObjectPresenter extends Presenters.Presenter{
    constructor(fileObject){
        super();
        this.fileObject=fileObject;
        this.highlight=false;
    }
    definition(){       
        let filePresenter=this.textLabel(this.fileObject.fileName).css({
            "font-weight":"bold",
            "padding":"0.2em",
            "border-bottom":"1px solid rgba(230,230,230,0.9)",
            "word-break":"break-all"
        })       
        .onClick(()=>{
            GuiActions.WikiActions.selectFile(this.fileObject.fileName)
            GuiActions.StructuredDocumentActions.addToJumpHistory(this.fileObject.fileContent,this.fileObject.fileContent);
        })
        .onContextMenu((e)=>{
            GuiActions.WikiActions.openFileContextMenu(this,this.fileObject.fileName)
            e.stopPropagation();
            return;
        })
        .onMouseOver((event)=>{
            event.currentTarget.classList.add("highlighted");
        })
        .onMouseOut((event)=>{
            event.currentTarget.classList.remove("highlighted");
            event.currentTarget.style.color="";
        })
        ;
        
        if(this.highlight){
            filePresenter.addClass("highlighted")
        }        
        return filePresenter;
    }
}