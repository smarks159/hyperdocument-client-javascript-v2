Presenters.KeyboardChoiceDialog=class KeyboardChoiceDialog extends Presenters.Presenter{
    constructor(keyboardChoices){
        super();
        this._textToDisplay=null;
        this._onResponse=null;
        this._keyboardListener=null;
        this._keyboardChoices=[];
        this.choices(keyboardChoices);
    }

    choices(choiceList){
        let uniqueKeys={}; 
        choiceList.forEach(choice=>{
            if(choice.key in uniqueKeys){
                throw new Error("Can only have one choice per key combination");
            }
            uniqueKeys[choice.key]=true;
        })
        this._keyboardChoices=choiceList;
    }
    
    onResponse(callback){
        this._onResponse=callback;
        return this;
    }
    
    textToDisplay(text){
        this._textToDisplay=text;
        return this;
    }
    
    definition(){
        return this.column([
            this.textLabel(this._textToDisplay).css({'font-weight':"bold","font-size":"1.1em",'text-align':"center"}),
            this.blank("0.5em"),
            this.textLabel("press a key or click on a button").css({'text-align':"center",'font-size':"0.8em"}),
            this.blank("0.5em"),
            this.defChoiceButtons()
        ]).css({"max-width":"250px"});
    }
    
    defChoiceButtons(){
        return this.row(this._keyboardChoices.map(choice=>{
            return this.row([
                this.button().text(choice.text)
                    .onClick(()=>{
                        this._onResponse(choice.value)
                    })
                    .css({"font-size":"1.1em","width":"75px","padding":"0"}),
                this.blank("0.4em"),
                this.textLabel("("+choice.key+")").css({"font-weight":"bold"}),
                this.blank("0.6em")
            ]).css({"align-items":"center","width":"100px","margin-bottom":"5px"})
        })).css({"flex-wrap":"wrap","justify-content":"center"})
    }

    hide() {
        super.hide();
        this.clearKeyboardListener();
        return this;
    }

    show(){
        super.show();
        this.buildKeyboardListener();
        this.focus();
        return this;
    }

    _initJavascript(){
        this.buildKeyboardListener();
    }

    _destroyJavascript(){
        this.clearKeyboardListener();
    }

    buildKeyboardListener(){
        if(this._keyboardListener){
            this.clearKeyboardListener();
        }
        this._keyboardListener=createKeypress(this.htmlEl);
        this._keyboardChoices.forEach(choice=>{
            this._keyboardListener.register_combo({
                "keys":choice.key,
                "is_exclusive":true,
                "prevent_default":true,
                "on_keydown":(e)=>{
                    this._onResponse(choice.value)
                    e.stopPropagation();
                }
            })
        })
    }

    clearKeyboardListener(){
        if(this._keyboardListener){
            this._keyboardListener.destroy();
            this._keyboardListener=null;
        }
    }

    openAsPopup(offset,width=null){
        super.openAsPopup(offset,width);
        this.focus();
    }
}