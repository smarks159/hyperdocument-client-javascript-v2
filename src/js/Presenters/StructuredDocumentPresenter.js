Presenters.StructuredDocumentPresenter=class StructuredDocumentPresenter extends Presenters.Presenter{
    constructor(structuredDocumentDataModel=null){
        super()
        // this is the node associated with the document
        this.dataNode=structuredDocumentDataModel;
        
        // this is the node displayed in the view, it does not have to be the document.
        this.rootDisplayNode=structuredDocumentDataModel;
        // map objectId to presenter attributes to perserve the presenter state for each object when refreshing the window. When refreshing a window, the top level presenter in the window is not destroyed so the attributes of the document will be preserved, but the same is not true for the children, so the state must be stored here. This is how the expand/collapse state is preserved.
        
        this.presenterAttributes={};
        this.viewModel=new DocumentViewModel();
    }
    
    set rootDisplayNode(value){
        if(value instanceof PropertyGraphModel.Node){
            this._rootDisplayNodeId=value.objectId;
        }
        else{
            this._rootDisplayNodeId=value;
        }
    }
    
    get rootDisplayNode(){
        if(this._rootDisplayNodeId!==null){
            return wikiDesktop.wikiData.propertyGraph.getNodeById(this._rootDisplayNodeId)
        }
        else{
            return null;
        }
    }

    isSelectedNodeTopLevel(){
        if(this.rootDisplayNode==this.dataNode){
            return true;
        }
        else{
            return false;
        }
    }
    
    definition(){     
        let col=this.column([]);
        if(!this.isSelectedNodeTopLevel()){
            col.appendChild(
                this.textLabel("&#9650; up one level").css({'font-weight':'bold',"margin-bottom":"0.5em"}).class("internalLink")
                .onClick(()=>{
                    GuiActions.StructuredDocumentActions.jumpUpFromPresenter(this);
                })
            )
        }

        let windowId=this.findFirstParentByType(Presenters.SingleNodeWindow).getWindowId()
        let docViewModel=wikiDesktop.documentViewModels[windowId];
        let nLevelsDown=docViewModel.nLevelsDown
        // When the top level document is display, start counting the nLevels down from the rootNode and display all top level nodes. So, for an nLevelsDown of 1, when the top of the document is displayed, all the top level nodes would be displayed, but not their children. 
        if(this.isSelectedNodeTopLevel()){
            nLevelsDown-=1;
            for (const child of new TreeNodeInterface(this.rootDisplayNode).children) {
                col.appendChild(new Presenters.StructuredDocumentNodePresenter(child,nLevelsDown));
            }
        }
        // When selecting a node display only that node and the nodes under it by nLevelsDown.
        else{
            col.appendChild(new Presenters.StructuredDocumentNodePresenter(this.rootDisplayNode,nLevelsDown));
        }
        
        return col;
    }
    
    saveNodeToData(){
        return {
                "type":"StructuredDocumentPresenter",
                "presenterAttributes":this.presenterAttributes,"rootDisplayNode":this.rootDisplayNode.objectId,
                "dataNode":this.dataNode.objectId
               }
    }
    
    static loadFromData(data){
        let presenter=new Presenters.StructuredDocumentPresenter();
        presenter.rootDisplayNode=wikiDesktop.wikiData.propertyGraph.getNodeById(data.rootDisplayNode);
        presenter.dataNode=wikiDesktop.wikiData.propertyGraph.getNodeById(data.dataNode);
        presenter.presenterAttributes=data.presenterAttributes;
        return presenter;
                
    }
    
    getPresenterAttributes(dataNode){
        let key=dataNode.objectId.toString();
        if(!(key in this.presenterAttributes)){
            this.presenterAttributes[key]={};            
        }
        return this.presenterAttributes[key];
    }
    
    /**
     * When the StructuredDocumentPresenter is refreshed, we must draw the highlighted range again. This needs to be defined in the render method, because only the render method is called recursively. If you call refresh on a parent object, the render method will be called but not if you define a refresh method in this class. The render method is still called when calling refresh directly on the StructuredDocumentPresenter class.
     */
    render(){
        super.render()
        if(wikiDesktop.selectedRange!==null){
            GuiActions.WikiActions.highlightSelectedRange()
        }
        
    }

    /**
     * Clear the expanded/collapsed state of the nodes. This is used when changing the view level.
     */
    clearExpandedState(){
        this.presenterAttributes={};
    }
    
    
}