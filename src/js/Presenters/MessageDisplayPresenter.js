Presenters.MessageDisplayPresenter=class MessageDisplayPresenter extends Presenters.Presenter{
    constructor(){
        super();
        this.messageText=null;
        this.messageColor="black";
        this.showClearButton=false;
        this.okCallback=()=>{
            this.clearMessage();
        }
    }
    
    definition(){
        let def=this.row([
        this.textLabel(this.messageText).css({
            color:this.messageColor,
            minHeight:"1em",
            border:"1px solid black", 
            padding:"0.2em"}).flex()
        ]);        
        if(this.showClearButton===true){
            def.children.push(                
                this.button().text("O.K.").onClick(this.okCallback).css({"margin-left":"1em"})
            );
        }
        return def.addClass("panel");
    }
    
    displaySuccessMessage(message){
        this.messageText=message;
        this.messageColor="green";
        this.refresh();
    }
    
    displayErrorMessage(message){
        this.messageText=message;
        this.messageColor="red";
        this.showClearButton=true;
        this.refresh();
    }
    
    clearMessage(){
        this.messageText="";
        this.messageColor="black";
        this.showClearButton=false;
        this.refresh();
    }
}