"use strict";

var InputFormPresenters={};

/*
    InputForm
        Capabilities:              
            - display one or more types of input fields
                - each field type is defined by a seperate javascript object type with a common interface.
            - display instructions about what input the user should enter
                - the displayTextField type is used to show this text.                         
            - collect the data from the input fields and send it back 
                - we should only collect data from input fields, which are defined by the isInputField in the
                field object.
            - listen for submit and cancel button click events
                - use the submitButtonEl and cancelButtonEl to get the buttons to add a click event to listen to.

    InputFields Interface:
        - render (function): build an elements html and return a dom object
        - isInputField (boolean): determines whether the object holds user input data
        - getData (only if isInputField is true): get the data value from the input field.
        - fieldName (only if isInputField is true): the getData of the inputForm returns a dictionary of values
        the key in the dictionary corresponds to the fieldName property of the object
 */
 
 InputFormPresenters.inputForm=class inputForm extends Presenters.Presenter{
     constructor(){
        super();
        this.fields=[];
        // store errorMsgs by fieldName
        this.errorMsgs={};
        this.submitButton=null;
        this.cancelButton=null;
        this._showCancel=true;
        this._showSubmit=true;
        
        // this is used to display a form error message at the bottom of the form, this is going to go away in the future.
        this.messageDisplayPresenter=new Presenters.MessageDisplayPresenter();
        this.messageDisplayPresenter.hide();
        this.messageDisplayPresenter.okCallback=(event)=>{
            this.clearErrorMessage()
            // stop event propagation to prevent popup input forms from closing.
            event.stopPropagation();
        };
        this._onSubmitClick=null;
        this._onCancelClick=null;
     }

     onSubmitClick(callback){
        this._onSubmitClick=callback;
        return this;
     }

     onCancelClick(callback){
         this._onCancelClick=callback;
         return this;
     }

     showSubmit(show){
         this._showSubmit=show;
         return this;
     }

     showCancel(show){
         this._showCancel=show;
         return this;
     }
     
     definition(){
        let def=this.column([]);
        for(let field of this.fields){
            def.children.push(field.css({"margin-bottom":"10px"}));
            if(field.isInputField){
                let errorField=new InputFormPresenters.errorMsgField();
                errorField.hide();
                this.errorMsgs[field.fieldName]=errorField;
                def.children.push(errorField.css({"margin-bottom":"10px"}));
            }
            
        }

        if(this._showSubmit || this._showCancel){
            def.children.push(this.buttonRow());
        }
        def.children.push(this.messageDisplayPresenter);
        return def;         
     }
     
     displayFieldErrorMsg(fieldName,errorMsg){
         this.errorMsgs[fieldName].text(errorMsg).show();
     }
     
     clearErrorMsgs(){
         Object.keys(this.errorMsgs).forEach((key)=>{
             this.errorMsgs[key].text("").hide();
         })
     }
     
     hasErrors(){
         let hasErrs=false;
         Object.keys(this.errorMsgs).forEach((key)=>{
             if(this.errorMsgs[key].getText()!==""){
                 hasErrs=true;
             }
         })
         return hasErrs;
     }
     
     buttonRow(){
        let row=[]
        if(this._showSubmit){
            this.submitButton=this.button().text("Submit");
            if(this._onSubmitClick){
                this.submitButton.onClick(this._onSubmitClick);
            }
            row.push(this.submitButton)
        }
        if(this._showCancel){
            this.cancelButton=this.button().text("Cancel")
            this.cancelButton.onClick((event)=>{
                this.clearData();
                if(this._onCancelClick){
                    this._onCancelClick(event)
                }
            })
            row.push(this.cancelButton);
        }
        return this.row(row);
        
     }
     
     addField(fieldObj){
         this.fields.push(fieldObj);
         return this;
     }
     
     clearData(){
        for (const field of this.fields) {
            if(field.isInputField){
                field.clearData();
            }
        }
        this.clearErrorMsgs();
        this.refresh()
     }
     
     getData(){    
        let data={};
        for(let field of this.fields){
            if(field.isInputField){
                data[field.fieldName]=field.getData();
            }
        }          
        return data;
     }
     
     openAsPopup(offset,width=null){         
        super.openAsPopup(offset,width);
        this.focus();
     }

     focus(){
        let firstInputField=this.fields.filter((field)=>{return field.isInputField})[0];
        firstInputField.focus();
     }
     
     clearErrorMessage(){
         this.messageDisplayPresenter.clearMessage();
         this.messageDisplayPresenter.hide();
     }
     
     displayErrorMessage(msg){
         this.messageDisplayPresenter.displayErrorMessage(msg);
         this.messageDisplayPresenter.show();
     }

     fireSubmitClick(){
         this.submitButton.htmlEl.click()
     }
}

InputFormPresenters.errorMsgField=class errorMsgField extends Presenters.Presenter{
    constructor(){
        super();
        this._text=this.textLabel();
    }
    definition(){
        return this.row([
            this._text.css({
                "margin-left":"5px",
                "color":"red",
                "word-break":"break-all"
            })
        ]);
    }
    text(textVal){
        this._text.text(textVal);
        return this;
    }
    getText(){
        return this._text.textAttr;
    }
}

InputFormPresenters.displayTextField=class displayTextField extends Presenters.Presenter{
    constructor(text){
        super();
        this.text=text;
        this.isInputField=false;
    }
    
    definition(){
        return this.textLabel(this.text).css({
            "font-weight":"bold"
        });
    }
}


InputFormPresenters.stringLineField=class stringLineField extends Presenters.Presenter{
    constructor(fieldName,defaultValue=""){
        super();
        this.isInputField=true;
        this.fieldName=fieldName;
        this.defaultValue=defaultValue;
        this.inputEl=null;
        this._onEnter=null;
        this._keyboardListener=null;
    }

    onEnter(callback){
        this._onEnter=callback;
        return this;
    }
    
    definition(){
        this.inputEl= this.input().name(this.fieldName).autocomplete("off").value(this.defaultValue).css({
            "border":'1px solid lightgrey'
        });
        if(this._onEnter){
            this.inputEl.onEnter(this._onEnter);
        }
        return this.inputEl;
    }
    
    getData(){
        return this.inputEl.getValue();
    }
    
    setData(text){
        this.inputEl.value(text);
    }

    clearData(){
        this.inputEl.value("");
    }
}

InputFormPresenters.intergerField=class intergerField extends InputFormPresenters.stringLineField{
    getData(){
        return parseInt(this.getData(),10);
    }
}

InputFormPresenters.fileInput=class fileInputSave extends Presenters.Presenter{
    constructor(fieldName,defaultValue=""){
        super()
        this.isInputField=true;
        this.fieldName=fieldName;
        this.defaultValue=defaultValue;
        this.inputFieldAttr=null;
        this._selectFileAction=()=>{
            let fileName=GuiActions.WikiActions.selectFileToSave();
            if(fileName!==""){
                this.inputFieldAttr.text=fileName;
                this.inputFieldAttr.refresh();
            }
        };
    }
    
    definition(){
        return this.row([
            this.inputField().flex().css({"margin-right":"10px"}),
            this.button().text("selectFile").onClick(()=>{
                if(this._selectFileAction!==null){
                    this._selectFileAction(this);
                }
            })
        ]).flex();
    }
    
    selectFileAction(callback){
        this._selectFileAction=callback;
        return this;
    }
    
    inputField(){       
        this.inputFieldAttr=new InputFormPresenters.displayTextField(this.defaultValue).css({"border":'1px solid lightgrey'});
        return this.inputFieldAttr;
    } 
    
    getData(){
        return this.inputFieldAttr.text;
    }
    
    clearData(){
        this.inputFieldAttr.text="";
    }
}

