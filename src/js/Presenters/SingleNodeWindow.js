"use strict";
/*
This class defines a window that displays a single data node that expands to fit the entire window and handles the creation of the presenter based on the type of the data node.
*/

let createPresenterFromDataModel=function(data){
    let typeToFactoryMap={
        
        "ProseMirrorDataModel":function(data){
            let view=new Presenters.TextPresenter();
            return view
        },
        
        "StructuredDocumentDataModel":function(data){
            return new Presenters.StructuredDocumentPresenter(data);
        },

        "SearchDataModel":function(data){
            return new Presenters.SearchPanelPresenter(data);
        }
    };
    if(typeToFactoryMap[data.type]===undefined){
        throw new Error("The node type "+data.type+" is not supported");
    }
    let newObj=typeToFactoryMap[data.type](data);    
    if(newObj===null || newObj===undefined){
        throw new Error("loadFromData does not return an object for type: "+data.type);
    }    
    let newPresenter=typeToFactoryMap[data.type](data);
    newPresenter.dataNode=data;
    return newPresenter;
}

Presenters.SingleNodeWindow=class SingleNodeWindow extends Presenters.Presenter{
    constructor(){
        super();
        this.nodePresenter=null;
        this.windowName=null;
        this.windowNameEl=null;
        this._windowId=null;
        this.selected=false;
    }

    windowId(id){
        this._windowId=id;
        return this;
    }

    getWindowId(){
        return this._windowId;
    }
    
    definition(){
        let header=this.defineHeader();
        let bodyContents;
        if(this.dataNode!==null){
            bodyContents=this.column([this.defineNodePresenter()]);
        }
        else{
            bodyContents=this.column([]);
        }
        let body=this.scrollingPanel(bodyContents,`window_${this._windowId}`);
        return this.column([header,body]).class("panel")
            .onContextMenu((event)=>{
                    GuiActions.WikiActions.openWindowMenu({"top":event.clientY,"left":event.clientX});
                    GuiActions.WikiActions.selectWindow(this._windowId);
                })
            .onClick(()=>GuiActions.WikiActions.selectWindow(this._windowId));
    }

    defineHeader(){
        let windowName=this.windowName!==null? this.windowName : "No File Selected";
        let fontWeight=this.selected? "bold" : "normal"

        this.windowNameEl=this.textLabel(windowName).css({"font-weight":fontWeight})
        return this.row([
            this.windowNameEl,
        ]).class("panelHeader")
    }

    setWindowName(fileName){
        this.windowName=fileName;
        if(this.windowNameEl){
            this.windowNameEl.text(fileName);
        }
    }
    
    defineNodePresenter(){
        if(this.nodePresenter===null){
            this.nodePresenter=createPresenterFromDataModel(this.dataNode);
            this.nodePresenter.dataNode=this.dataNode;
            this.nodePresenter.parent=this;
        }
        return this.nodePresenter.css({"padding":"0.2em"});
    }

    selectWindow(){
        if(this.windowNameEl){
            this.windowNameEl.css({"font-weight":"bold"});
        }
        this.selected=true;
    }

    unselectWindow(){
        if(this.windowNameEl){
            this.windowNameEl.css({"font-weight":"normal"});
        }
        this.selected=false;
    }
    
    saveNodeToData(){
        var data={
            "type":"SingleNodeWindow",
            "windowId":this._windowId,
            "visible":this.visible,
            "windowName":this.windowName
        };
        if(this.dataNode!==null){
            data.dataNode=this.dataNode.objectId;
            data.presenterData=this.nodePresenter.saveNodeToData();            
        }
        else{
            data.dataNode=null;
        }
        return data;
    }
    
    static loadFromData(data){
        var newWindow=new Presenters.SingleNodeWindow();
        newWindow.windowId(data.windowId);
        newWindow.visible=data.visible;
        newWindow.windowName=data.windowName;
        if(data.dataNode!==null){
            newWindow.dataNode=wikiDesktop.wikiData.propertyGraph.getNodeById(data.dataNode)
            if(data.presenterData!==null && data.presenterData!==undefined){
                newWindow.nodePresenter=BuildPresenterFromPersistenceData(data.presenterData);            
                newWindow.nodePresenter.parent=newWindow;
            }
        }
        return newWindow;
    }
    
    set dataNode(value){
        super.dataNode=value;        
        this.nodePresenter=null;
    }
    
    // must redefine the getter as well when overriding a setter for a property
    get dataNode(){
        return super.dataNode;
    }
    
};