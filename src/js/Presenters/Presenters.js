"use strict";
var Presenters={};

Presenters.Presenter=class Presenter extends Fragments.Fragment{
    constructor(){
        super();
        this._dataNodeId=null;
        this.definitionObj=null;
        this.cssOrig=null;
        this.type=null;
    }
    
    set dataNode(value){
        if(value instanceof PropertyGraphModel.Node){
            this._dataNodeId=value.objectId;
        }
        else{
            this._dataNodeId=value;
        }
    }
    
    get dataNode(){
        if(this._dataNodeId!==null){
            return wikiDesktop.wikiData.propertyGraph.getNodeById(this._dataNodeId);
        }
        else{
            return null;
        }
    }
    
    textArea(){
        return new Fragments.ProseMirrorEditor();
    }
    
    button(){
        return new Fragments.button();
    }
    
    textLabel(text=""){
        return new Fragments.textLabel(text);
    }
    
    input(){
        return new Fragments.input();
    }
    
    blank(size,direction="horizontal"){
        return new Fragments.blank().size(size).direction(direction);
    }
    
    column(children=[]){
        return new Fragments.flexbox(children);
    }
    
    row(children=[]){
        return new Fragments.flexbox(children).direction("row");
    }

    scrollingPanel(innerObj,viewModelName){
        return new Fragments.ScrollingPanel(innerObj,viewModelName);
    }

    contentHolder(){
        return new Fragments.HolderFragment();
    }
    
    render(){
        let def=this.definition();
        if(def===undefined){
            throw Error("The definition method does not return an object");
        }
        if(!(def instanceof Fragments.Fragment)){
            throw Error("The definition does not return a fragment or presenter");
        }
        this.definitionObj=def;
        this.definitionObj.parent=this;
        def.render();
        this.htmlEl=def.htmlEl;
        // There is no reliable way to get the class name in javascript, so we must define the type as an attribute in each presenter class. This is required by the command interpreter for the selectObject command.
        if(this.type!==null){
            this.htmlEl.dataset.presentertype=this.type;
        }
        this.renderAttributes();
    }
    
    get children(){
        if(this.definitionObj!==null){
            return [this.definitionObj];
        }
        else{
            return [];
        }
    }
    
    hasChildren(){
        return this.definitionObj!==null;
    }

    definition(){
        throw new Error("This method needs to be implemented in the child class");
    }
    
    openAsPopup(offset,width=null){
        if(offset instanceof Fragments.Fragment){
            let fragment=offset;
            if(fragment.htmlEl===null){
                throw Error("The Fragment has not been rendered yet.")
            }
            offset=fragment.htmlEl.getBoundingClientRect();  
        }    
        // Open the popup based given a specific offset. The offset may not contain height or width, such as in the case where the coordinates of the mouse pointer are passed in.
        else{
            if(offset.height===undefined){
                offset.height=0
            }
            if(offset.width===undefined){
                offset.width=0
            }
        }
        
        
        let cssProps={
            "position":"absolute",
            "z-index":1000,
            "padding":"10px",
            "border-radius":"5px",
            "background":"white",
            "border":"1px solid black"
        };
        this.cssOrig=this.cssAttr;
        if(width){
            cssProps["width"]=width+"px";
        }
        this.css(cssProps);
        // Must show the popup first the form must be visible in order to get its dimensions. It also must be visible before rendering because certain properties, like scrollHeight, only apply when an element is visible. One place were scrollHeight is used is to calculate the height of the input form based on the contents.
        this.show();
        this.refresh();
        
        let viewportHeight=$(window).height();
        let viewportWidth=$(window).width();
        
        let popupOffset=this.htmlEl.getBoundingClientRect();
        
        let top=0;
        let left=0;
        // If the popup is in the lower half of the screen and it is too big to fit in the view port, open it above the offset instead of below it.
        if(offset.top+offset.height+popupOffset.height<=viewportHeight-5){
            top=(offset.top+offset.height)
        }
        else{
            top=(offset.top-popupOffset.height)
        }
        // If the popup is in the right half of the screen and it is too big to fit in the view port, open it to the left of the offset.
        if(offset.left+popupOffset.width<=viewportWidth-5){
            left=offset.left
        }
        else{
            left=(offset.left-popupOffset.width)
        }

        // If the screen is really small or the width or height of the popup is really big, then the calculations above can be produce negative coordinates, which causes the popup to be drawn offscreen. In these cases set the coordinates to zero. The current use cases involve popups with really long text, like file paths, in this case setting the coordinates to zero and having the text wrap around works well.
        if(top<0){
            top=0;
        }
        if(left<0){
            left=0;
        }

        $(this.htmlEl).css("top",top+"px");
        $(this.htmlEl).css("left",left+"px");
        
        // when a user clicks outside of the popup, with any button, close
        $(document).on("mousedown",(event)=>{this.closePopupOnClickOutside(event)});
    }
    
    closePopupOnClickOutside(event){
        if(!$(event.target).closest(this.htmlEl).length) {
            if(this.isVisible()){
                this.closePopup();
                $(document).off("mousedown");
            }   
        }
    }
    
    closePopup(){        
        this.cssAttr=this.cssOrig;
        this.cssOrig=null;        
        this.refresh();
        this.hide();
    }

    /**
     * Attributes are copied from the presenter to the top level defintion object and override any attributes. This method must be called in the Presenter render function and in any methods setting attributes.
     */
    renderAttributes(){
        this.definitionObj.copyAttributes(this);
        this.definitionObj.renderAttributes();
    }

    /**
     * When removing a class we must explictly remove it from the definitionObj, if it exists in the definitionObj. We must explicity remove it because there is no way to tell whether a class was originally defined in the Presenter class or the definitionObject. This is not a problem when adding a class, because classes are stored as sets and adding a duplicate class has not effect. Removing a class, on the other hand needs to be handled explicitly.
     */
    removeClass(className){
        this._classes.delete(className);
        if(this.definitionObj){
            this.definitionObj.removeClass(className);
            this.definitionObj.renderAttributes();
        }
    }
}