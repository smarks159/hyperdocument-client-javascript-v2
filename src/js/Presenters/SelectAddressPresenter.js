/**
 * When selecting an address the following data is return:
 *      - node: The node object corresponding to the node selected.
 *      - mode: How did the user select the node. The two possible values are "TypedAddress" and "SelectedInGUI". This is necessary because the GUI may be updated in slightly different ways depending on how the user entered the address.
 *      - addressInput: When mode is "TypedAddress". This attribute contains the address typed in by the user.
 *      - presenterType: When mode is "SelectedInGUI". This attributes contains the type of presenter that has been selected.
 *      - selectedColor: When mode is "SelectedInGUI". This attribute contains color that the selected presenter is highlighted in.
 */
class SelectAddressResponse{
    constructor(node,mode,addressInput=null){
        this.node=node;
        if(mode!="TypedAddress" && mode!="SelectedInGUI"){
            throw new Error("Invalid mode for select address response. Must be either 'TypedAddress' or 'SelectedInGui.");
        }
        this.mode=mode;
        this.addressInput=addressInput;

        // GUI response attributes
        this.selectedColor=null;
        this.presenterType=null;
    }
}

Presenters.SelectAddressPresenter=class SelectAddressPresenter extends Presenters.Presenter{
    constructor(){
        super();
        this._objectListeners=[];
        this._selectedPresenters=[];
        this.addressInput=null;
        this._onSelectNode=null;
        this._onCancel=()=>{
            this.addressInput.hide();
        }
    }

    onSelectNode(callback){
        this._onSelectNode=callback;
        return this;
    }

    onCancel(callback){
        this._onCancel=callback;
        return this;
    }

    definition(){
        let inputForm=new InputFormPresenters.inputForm();
        inputForm.addField(new InputFormPresenters.displayTextField("Enter address to select"));
        inputForm.addField(new InputFormPresenters.stringLineField("address").onEnter(()=>inputForm.fireSubmitClick()));
        inputForm.onSubmitClick(()=>{
            let addressInput=inputForm.getData()["address"];
            this.deactivateObjectSelection();
            if(addressInput.trim()===""){
               inputForm.displayFieldErrorMsg("address","You must enter an address");
               return;
            }
            let parts=addressInput.split(",");
            let fileName,address;

            if(parts.length==1){
                fileName=null;
                address=parts[0].trim();
            }
            else if(parts.length==2){
                fileName=parts[0].trim();
                address=parts[1].trim();
            }
            else{
                inputForm.displayFieldErrorMsg("address","The address you entered has two many commas in it.");
                return;
            }
            let node=GuiActions.StructuredDocumentActions.getNodeFromAddress(address,fileName);
            if(node===null){
                inputForm.displayFieldErrorMsg("address","The address you entered is invalid or does not exist");
                return;
            }
            if(this._onSelectNode){
                inputForm.clearData();
                inputForm.hide();
                this._onSelectNode(new SelectAddressResponse(node,"TypedAddress",addressInput));
            }
        })
        inputForm.onCancelClick(()=>{
            inputForm.clearData();
            inputForm.hide();
            if(this._onCancel){
                this._onCancel();
            }
        })
        this.addressInput=inputForm;
        return this.addressInput;
    }

    activateObjectSelection(presenterType,selectedColor){
        let callback=(event)=>{
            let presenterSelected=wikiDesktop.topWindowPanel.findPresenterByHtmlEl(event.currentTarget)

            // The background color is changed manually instead of using a css class and calling highlight, because the color used is dynamically taken from the Config. The documentNodePresenter should be changed to handle this by calling a highlight method.
            let documentNode=presenterSelected.parent;
            documentNode.htmlEl.style.backgroundColor=selectedColor;
            this._selectedPresenters.push(presenterSelected);
            if(this._onSelectNode){
                let response=new SelectAddressResponse(presenterSelected.dataNode,"SelectedInGUI");
                response.selectedColor=selectedColor;
                response.presenterType=presenterType;
                this._onSelectNode(response);
            }
        }
        for(const node of document.querySelectorAll(`[data-presentertype=${presenterType}]`)){
            node.addEventListener("click",callback);
            this._objectListeners.push([node,callback]);
        }
    }

    deactivateObjectSelection(){
        if(this._objectListeners.length>0){
            for(const [node,callback] of this._objectListeners){                
                node.removeEventListener("click",callback)
            }
            this._objectListeners=[];
        }
        this.addressInput.clearData();
    }

    _initJavascript(){
        this.buildKeyboardListener();
    }

    _destroyJavascript(){
        this.clearKeyboardListener();
    }

    buildKeyboardListener(){
        if(this._keyboardListener){
            this.clearKeyboardListener();
        }
        this._keyboardListener=createKeypress(this.htmlEl);
    }

    clearKeyboardListener(){
        if(this._keyboardListener){
            this._keyboardListener.destroy();
            this._keyboardListener=null;
        }
    }

    unselectPresenters(){
        for(const presenter of this._selectedPresenters){
            let documentNode=presenter.parent;
            documentNode.htmlEl.style.backgroundColor="";
        }
    }

    focusOnFirstField(){
        let firstInputField=this.addressInput.fields.filter((field)=>{return field.isInputField})[0]
        firstInputField.focus();
    }
}
