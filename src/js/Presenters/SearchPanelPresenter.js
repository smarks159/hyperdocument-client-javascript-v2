"use strict";

Presenters.TextSearchResultPresenter= class TextSearchResultPresenter extends Presenters.Presenter{
    constructor(searchResult){
        super();
        this.resultToDisplay=searchResult;
    }

    definition(){
        this.editor=this.textArea().readOnly(true).css({"margin-left":"1em"});
        this.editor.dataNode=this.resultToDisplay.node;
        return this.column([
            this.addressLinkDef().css({"margin-bottom":"0.1em"}),
            this.editor
        ])
    }

    addressLinkDef(){
        let [fileName,address]=this.resultToDisplay.getStructuralAddress()
        let addressLabel=this.textLabel(`File:${fileName}, address: ${address}`).class("internalLink")
            .onClick(()=>{
                GuiActions.SearchActions.openAddressFromSearchResult(this.resultToDisplay,addressLabel)
            })
        return this.row([addressLabel])
    }

    _initJavascript(){
        for (const pos of this.resultToDisplay.positions) {
            this.editor.highlightRange(pos);
        }
    }
}

Presenters.SearchPanelPresenter=class SearchPanelPresenter extends Presenters.Presenter{
    constructor(data){
        super();
        this.searchResults=[];
        this.searchNode=data;
    }

    definition(){
        return this.column([
            this.textLabel("Search Input"),
            this.searchForm(),
            this.blank(5),
            this.searchResultsHeader(),
            this.blank(5),
            this.searchResultsBody()
        ]);
    }

    searchForm(){
        let inputForm=new InputFormPresenters.inputForm();
        let searchInputField=new InputFormPresenters.stringLineField("text");
        inputForm.addField(new InputFormPresenters.displayTextField("text to search"));
        inputForm.addField(searchInputField);
        let onSubmitCallback=()=>{
            this.clearSearchResults()
            inputForm.clearErrorMsgs();
            let text=inputForm.getData()["text"];
            if(text.trim()===""){
                inputForm.displayFieldErrorMsg("text","This field cannot be blank");
            }
            this.searchText(text);
        }
        inputForm.onSubmitClick(onSubmitCallback);
        searchInputField.onEnter(onSubmitCallback);
        inputForm.onCancelClick(()=>{
            searchInputField.setData("")
            this.clearSearchResults()
            
        })
        return inputForm;
    }

    searchResultsHeader(){
        this.searchHeaderDisplay=this.contentHolder();
        this.renderSearchHeader();
        return this.searchHeaderDisplay;
    }
    
    renderSearchHeader(){
        this.searchHeaderDisplay.content=this.textLabel(`Displaying ${this.searchResults.length} results`).css({"border":"1px solid black","padding":"0.1em"});
    }

    searchResultsBody(){
        this.searchResultsDisplay=this.contentHolder();
        this.renderSearchResults();
        return this.searchResultsDisplay;
    }

    renderSearchResults(){
        let resultsPresenters=[]
        for (const result of this.searchResults) {
            resultsPresenters.push(new Presenters.TextSearchResultPresenter(result).css({"border":"1px solid gray","padding":"0.1em"}))
        }
        let innerObj=this.column([
            ...resultsPresenters
        ])
        this.searchResultsDisplay.content=this.scrollingPanel(innerObj,`searchNode_${this.searchNode.objectId}`).css({"padding":"0.1em"});
    }

    searchText(text){
        this.searchResults=GuiActions.SearchActions.searchText(text);
        this.refreshSearchResults();
    }

    refreshSearchResults(){
        this.renderSearchHeader();
        this.renderSearchResults();
    }

    clearSearchResults(){
        this.searchResults=[];
        this.refreshSearchResults();
    }
}