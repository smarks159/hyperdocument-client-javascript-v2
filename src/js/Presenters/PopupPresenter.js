"use strict";

Presenters.PopupPresenter=class PopupPresenter extends Presenters.Presenter{
    constructor(){
        super();
        this.content=this.contentHolder();
    }

    definition(){
        return this.column([this.content]);
    }

    setContent(fragment){
        this.content.setContent(fragment);
        return this;
    }

    openAsPopup(offset,width=null){
        super.openAsPopup(offset,width);
        this.content.focus();
    }
}