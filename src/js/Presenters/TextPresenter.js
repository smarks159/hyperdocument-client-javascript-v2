"use strict";

Presenters.TextPresenter=class TextPresenter extends Presenters.Presenter{
    constructor(){
        super();
        this.textEditorEl=null;
        this.type="TextPresenter";
    }
    
    definition(){
        let readOnly=!wikiDesktop.commandInterpreterActive;

        this.textEditorEl=this.textArea().css({            
            "border-style": "none",
            "minHeight":"0.8em",
            "overflow":'hidden',
            "word-break":"break-all"
        }).flex()
        .onBlur(()=>this.drawOutline())
        .onFocus(()=>this.drawOutline())
        .onContextMenu((event)=>{
            if(this.textEditorEl.isLinkSelected() && !this.textEditorEl.isRangeSelected()){
                GuiActions.StructuredDocumentActions.openLinkMenu(this);
            }
            else if(this.textEditorEl.isInternalLinkSelected() && !this.textEditorEl.isRangeSelected()){
                GuiActions.StructuredDocumentActions.openInternalLinkMenu(this);
            }
            else if(this.textEditorEl.isRangeSelected()){
                GuiActions.StructuredDocumentActions.openTextRangeContextMenu(this);
            }
            else{
                GuiActions.StructuredDocumentActions.openTextNodeContextMenu(this);
            }
            return false;
        })
        .onDblClick(()=>{
            if(this.textEditorEl.isLinkSelected()){
                GuiActions.StructuredDocumentActions.openLink(this);
            }
            else if(this.textEditorEl.isInternalLinkSelected()){
                GuiActions.StructuredDocumentActions.openInternalLink(this);
            }
            return false;
        })
        .onChange((newState,editor)=>GuiActions.StructuredDocumentActions.updateTextState(this.dataNode,newState,editor))
        .readOnly(readOnly)
        this.textEditorEl.dataNode=this.dataNode;

        return this.textEditorEl;
    }
    
    /*
    This method should be called after the initJavascript is called on the textEditorEl, which creates the keyboardListener.
    */
    registerKeyboardEvents(){
        this.textEditorEl.keyboardListener.register_many([
            {
                "keys":'ctrl b',
                "is_exclusive":true,
                "on_keydown":(e)=>{
                    this.textEditorEl.toggleBold();
                }
            },
            {
                "keys":"enter",
                "is_exclusive":true,
                "on_keyup":(e)=>{
                    GuiActions.StructuredDocumentActions.insertTextCommand(this);
                }
            },
            {
                "keys":'tab',
                "is_exclusive":true,
                "on_keyup":(e)=>{
                    GuiActions.StructuredDocumentActions.indent(this);                    
                }
            },
            {
                "keys":"shift tab",
                "is_exclusive":true,
                "prevent_default":true,
                "on_keyup":(e)=>{
                    GuiActions.StructuredDocumentActions.deIndent(this); 
                }
            },
            {
                "keys":"up",
                "is_exclusive":true,                
                "on_keydown":(e)=>{
                    if(this.getCursorLineNumber()===1){
                        GuiActions.StructuredDocumentActions.selectPrevious(this); 
                        e.preventDefault();
                    }
                    else{
                        return true;
                    }
                }
            },
            {
                "keys":"down",
                "is_exclusive":true,
                "on_keydown":(e)=>{
                    if(this.getCursorLineNumber()===this.getLineCount()){
                        GuiActions.StructuredDocumentActions.selectNext(this); 
                        e.preventDefault();
                    }
                    else{
                        return true;
                    }
                }
            },
            {
                "keys":"ctrl p",
                "is_exclusive":true,
                "on_keydown":(e)=>{                
                    GuiActions.StructuredDocumentActions.moveToPrevious(this);
                }
            },
            {
                "keys":"ctrl n",
                "is_exclusive":true,
                "on_keydown":(e)=>{                
                    GuiActions.StructuredDocumentActions.moveToNext(this);
                }
            },
            {
                "keys":"ctrl d",
                "is_exclusive":true,
                "on_keydown":(e)=>{
                    if(!wikiDesktop.selectedRange.isEmpty()){
                        GuiActions.StructuredDocumentActions.ConfirmAndDeleteSelectedRange();
                    }
                    else{
                        GuiActions.StructuredDocumentActions.ConfirmAndDelete(this);
                    }
                }
            },
            {
                "keys":"ctrl left",
                "is_exclusive":true,
                "on_keydown":(e)=>{
                    GuiActions.StructuredDocumentActions.collapseTextPresenter(this);
                }
            },
            {
                "keys":"ctrl right",
                "is_exclusive":true,
                "on_keydown":(e)=>{
                    GuiActions.StructuredDocumentActions.expandPresenter(this);
                }
            }
        ])   
    }
    
    isEmpty(){
        let text=this.textEditorEl.getText();
        if(text===""){
            return true
        }
        // If text only contains whitespace
        if(/^\s+$/.test(text)){
            return true
        }
        return false;
    }
    
    focus(){
        this.textEditorEl.focus();
    }
    
    _initJavascript(){
        this.registerKeyboardEvents();
        this.drawOutline();
    }
    
    drawOutline(){
        if(this.isEmpty() || this.textEditorEl.hasFocus()){
            this.textEditorEl.css({"border":"solid 1px grey"});
        }
        else{
            this.textEditorEl.css({"border":"none transparent"});
        }
    }
    
    highlight(){
        this.findFirstParentByType(Presenters.StructuredDocumentNodePresenter).highlight();
    }
    
    unhighlight(){
        this.findFirstParentByType(Presenters.StructuredDocumentNodePresenter).unhighlight();
    }

    /**
     * Get the line number that the text appears on. This function relies on the fact that the height of the cursor is always equal to the height of a single line.
     * This function is used when pressing the up or down key and used to determine when to select the next paragraph.
     */
    getCursorLineNumber(){
        let {top:selectionTop,height:lineHeight}=this.textEditorEl.getSelectionOffset();
        let {top:paragraphTop}=this.htmlEl.getBoundingClientRect();
        let distanceFromTop=selectionTop-paragraphTop
        let linenumber=Math.floor(distanceFromTop/lineHeight)+1
        return linenumber;
    }

    /**
     * Get the total number of lines. This function relies on the fact that the height of the cursor is always equal to the height of a single line. 
     */
    getLineCount(){
        let {height:cursorHeight}=this.textEditorEl.getSelectionOffset();
        let {height:paragraphHeight}=this.htmlEl.getBoundingClientRect();
        return Math.floor(paragraphHeight/cursorHeight);
    }
}