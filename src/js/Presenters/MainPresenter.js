"use strict";

Presenters.MainPresenter=class MainPresenter extends Presenters.Presenter
{
    constructor(){   
        super();    
        this.wikiActionDropDown=null;
        this.isPersistent=true;
        this.windowManager=new Presenters.WindowManagerPresenter();
        this.filePanelPresenter=new Presenters.FilePanelPresenter();
        this.messageDisplayPresenter=new Presenters.MessageDisplayPresenter();
        this.inputFormPresenter=new InputFormPresenters.inputForm().hide();
        this.contextMenu=new Presenters.ContextMenuPresenter().hide();
        this.confirmationDialog=new Presenters.KeyboardChoiceDialog([
            {
                "key":"y",
                "text":"yes",
                "value":"yes"
            },
            {
                "key":"n",
                "text":"no",
                "value":"no"
            } 
        ]).hide();
        this.recentlyOpenedWikiPanel=new Presenters.RecentlyOpenedWikiPanelPresenter().hide();
        this.sequencePresenter=new CommandSeqContextMenu().hide();
        this.commandInterpreter=new CommandInterpreterPresenter();
        this.popupPanel=new Presenters.PopupPresenter().hide();
    }
    
    definition(){
        return this.column([
            this.confirmationDialog,
            this.contextMenu,
            this.inputFormPresenter,
            this.recentlyOpenedWikiPanel,
            this.sequencePresenter,
            this.popupPanel,
            this.column([
                this.header(),
                this.blank(5),
                this.row([                       
                    this.filePanelPresenter,
                    this.blank(5),
                    this.windowManager.css({"border":"solid 1px black"}).flex()
                ]).flex()
            ]).flex()
        ]).flex();
    }

    header(){
        if(wikiDesktop.commandInterpreterActive){
            return this.commandInterpreterDef();
        }
        else{
            return this.editorGUI();
        }
    }

    editorGUI(){
        return this.row([
            this.buildWikiActionDropDown().css({"width":"200px"}),
            this.blank(5),
            this.messageDisplayPresenter.flex()
        ])
    }

    commandInterpreterDef(){
        return this.column([
            this.commandInterpreter,
            this.blank(5),
            this.messageDisplayPresenter.flex()
        ])
    }
    
    buildWikiActionDropDown(){
        this.wikiActionDropDown=new Presenters.SelectDropdownPresenter("Wiki Actions").choices([
            {text:"",callback:()=>{}},
            {text:"Save Wiki",callback:()=>{GuiActions.WikiActions.saveWiki();}},
            {text:"Load Wiki",callback:()=>{GuiActions.WikiActions.loadWiki();}},
            {text:"New Wiki",callback:()=>{GuiActions.WikiActions.createNewWiki();}},
            {text:"New Window",callback:()=>{GuiActions.WikiActions.newWindow()}},
            {text:"Import Text File", callback:()=>{GuiActions.WikiActions.importTextFile()}},
            {text:"Recently Opened Wikis", callback:()=>{GuiActions.WikiActions.displayRecentlyOpenedWikiPanel()}},
            {text:"search text",callback:()=>{GuiActions.WikiActions.displayGlobalSearch()}}
        ]);
        return this.wikiActionDropDown.addClass("panel")
    }
    
    saveNodeToData(){
        let data={
            "type":"MainPresenter",
            "windowManagerPresenter":this.windowManager.saveNodeToData(),
            "filePanelPresenter":this.filePanelPresenter.saveNodeToData()
        }
        return data;
    }
    
    static loadFromData(data){
        let newObj= new Presenters.MainPresenter();
        newObj.windowManager=Presenters.WindowManagerPresenter.loadFromData(data["windowManagerPresenter"]);
        newObj.filePanelPresenter=Presenters.FilePanelPresenter.loadFromData(data["filePanelPresenter"]);
        newObj.windowManager.parent=newObj;
        newObj.filePanelPresenter.parent=newObj;
        return newObj;
    }
    
    resetWiki(){
        this.filePanelPresenter.clearFileSelected();
        this.windowManager.resetState();
        super.refresh();
    }
}