Presenters.RecentlyOpenedWikiPanelPresenter=class RecentlyOpenedWikiPanelPresenter extends Presenters.Presenter{
    constructor(){
        super();
    }
    
    definition(){
        let col=[];
        if(wikiDesktop.recentlyOpenedWikiFiles.length!==0){
            for(let filePath of wikiDesktop.recentlyOpenedWikiFiles){
                col.push(
                    this.textLabel(filePath).css(
                        {   
                            "font-weight":"bold",
                            "padding":"0.2em",
                            "border-bottom":"1px solid lightgrey",
                            "word-break":"break-all"
                        }
                    ).onClick((event)=>{
                        this.closePopup();
                        if(event.ctrlKey==true){
                            GuiActions.WikiActions.newWindow(filePath);
                        }
                        else{
                            GuiActions.WikiActions.loadWikiFile(filePath);
                        }
                    })
                )
            }
            return this.column(col)
        }
        else{
            return this.textLabel("No recently opened files").css(
                {
                    "font-weight":"bold",
                    "padding":"0.2em",
                    "border-bottom":"1px solid lightgrey",
                    "word-break":"break-all"
                })
        }
        
    }
}