"use strict";

Presenters.FilePanelPresenter=class FilePanelPresenter extends Presenters.Presenter{
    constructor(){
        super();
        this.fileSelected=null;
        this.showPanel=true;
    }
    
    get filesToDisplay(){
        let tmp=[...wikiDesktop.wikiData.RootFileNode.children]
        tmp.sort((a,b)=>{
            return a.fileName.localeCompare(b.fileName, 'en', {'sensitivity': 'base'});
        });
        return tmp;
    }
    
    fileColumn(){
        var fileColumn=this.column([]);
        for(var fileObj of this.filesToDisplay){
            var filePresenter= new Presenters.FileObjectPresenter(fileObj)
            
            if(this.fileSelected===fileObj.fileName)
            {
                filePresenter.highlight=true;
            }
            else{
                filePresenter.highlight=false;
            }

            fileColumn.children.push(filePresenter);
        }
        return fileColumn;
    }
    
    filePanelHeaderExpanded(){
        return this.row(
        [
            this.textLabel("File Browser"),
            this.blank().flex(),
            this.textLabel("&#10134;")
            .css({'font-weight':'bold',"margin-right":"2px"})
            .onClick(()=>{
                this.showPanel=false;
                this.refresh();
            })
             
        ]).class("panelHeader")
    }
    
    filePanelHeaderExpandedMinimized(){
        return this.row(
        [
            this.textLabel("&#10133;")
            .css({'font-weight':'bold'})
            .onClick(()=>{
                this.showPanel=true;
                this.refresh();
            }),
             
        ]).css({"justify-content":"center"});
    }
    
    definition(){
        if(this.showPanel){
            return this.column(
            [
                    this.filePanelHeaderExpanded(),
                    this.scrollingPanel(this.fileColumn(),"filePanel").flex(1)
            ]).css({"border":"1px solid black","width":"200px"}).addClass("panel")
            .onContextMenu((e)=>{
                GuiActions.WikiActions.openFilePanelContextMenu({"top":e.clientY,"left":e.clientX});
            })
        }
        else{
            return this.column(
            [
                this.filePanelHeaderExpandedMinimized()
            ]).addClass("panel").css({"width":"20px"});
        }
    }
    
    selectFile(fileName){
        this.fileSelected=fileName;
        this.refresh();
    }
    
    clearFileSelected(){
        this.fileSelected=null;
        this.refresh();
    }
    
    saveNodeToData(){
        var data={"type":"FilePanelPresenter","fileSelected":this.fileSelected};
        return data;
    }
    
    static loadFromData(data){
        var newObj=new Presenters.FilePanelPresenter();
        newObj.fileSelected=data.fileSelected;
        return newObj;
    }
}