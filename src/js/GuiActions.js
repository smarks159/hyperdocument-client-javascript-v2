"use strict";

/* * 
Actions are implemented as seperate methods in a seperate namespace, because they can effect more than one object
and it doesn't make sense to put them in one object or the other.In the future, other objects may be involved, such
as adding an to a history log for version control purposes each time a tree manipulation method is called.
 */

var GuiActions={};

GuiActions.state={};

GuiActions.state.saveState=function(){      

    var stateData={
        "presenterData":wikiDesktop.topWindowPanel.saveNodeToData(),
        "currentWikiFile":wikiDesktop.currentWikiFile,
        "clipboardObjects":wikiDesktop.clipboardObjects,
        "copyClipboardObject":wikiDesktop.copyClipboardObject,
        "selectedWindow":wikiDesktop.selectedWindow,
        "viewModels":wikiDesktop.viewModels.propGraph.saveToData()
    }
    // handle case were the wiki has not been saved yet
    if(wikiDesktop.currentWikiFile===null){
        stateData["wikiData"]=wikiDesktop.wikiData.propertyGraph.saveToData()
    }
    ipcRenderer.sendSync("saveState",JSON.stringify(stateData));
};

GuiActions.state.loadState=function(){
    let wikiData,MainPresenter;
    if(GuiActions.state.hasSavedState()){
        let state=JSON.parse(ipcRenderer.sendSync("loadState"));
        // If the wiki file set in the state has been moved or deleted then do not load the data from state.
        let currentWikiFile=state["currentWikiFile"];
        if(currentWikiFile && !ipcRenderer.sendSync("fileExists",currentWikiFile)){
            return false;
        }        
        else{
            if(currentWikiFile===null){
                wikiData=state["wikiData"];
            } else{
                wikiData=JSON.parse(ipcRenderer.sendSync("loadWikiFile",currentWikiFile));
            }
            wikiDesktop.createDBInterfaces(wikiData);
            wikiDesktop.initializeDefaultSearchNode();
            wikiDesktop.currentWikiFile=currentWikiFile;
            if(currentWikiFile){
                GuiActions.WikiActions.setWindowTitle(wikiDesktop.currentWikiFile);
            }
            else{
                GuiActions.WikiActions.setWindowTitle("new wiki");
            }
        }

        let viewPropGraph=PropertyGraphModel.PropertyGraph.loadFromData(state["viewModels"]);
        wikiDesktop.viewModels=new ViewModelInterface(viewPropGraph);
        
        if(state["clipboardObjects"]){
            wikiDesktop.clipboardObjects=state["clipboardObjects"];
            wikiDesktop.copyClipboardObject=state["copyClipboardObject"];
        }
        
        MainPresenter=Presenters.MainPresenter.loadFromData(state["presenterData"])

        if(state["selectedWindow"]){
            wikiDesktop.selectedWindow=state["selectedWindow"];
        }
        
        GuiActions.Initialization.createMainPresenter(wikiData,MainPresenter);
        document.body.innerHTML="";
        document.body.appendChild(MainPresenter.htmlEl);
    }
};

GuiActions.state.hasSavedState=function(){
    return ipcRenderer.sendSync("hasSavedState");
}

GuiActions.Initialization={};

GuiActions.Initialization.Initialize=function(){
    let MainPresenter=GuiActions.Initialization.createMainPresenter();
    document.body.appendChild(MainPresenter.htmlEl);
    GuiActions.WikiActions.selectWindow("left");
    
};

GuiActions.Initialization.createMainPresenter=function(wikiData=null,MainPresenter=null){    
    if(wikiData==null && MainPresenter==null){
        wikiDesktop.createDBInterfaces(new PropertyGraphModel.PropertyGraph());
        wikiDesktop.initializeDefaultSearchNode();
        // View information is stored in a separate property graph model than the wiki data.
        wikiDesktop.viewModels=new ViewModelInterface(new PropertyGraphModel.PropertyGraph());
        MainPresenter=new Presenters.MainPresenter();
        GuiActions.WikiActions.setWindowTitle("new wiki");
        
        let commandInterpreter=new CommandInterpreterPresenter();
        // add the command interpreter to the wikiDesktop here so the base subsystem initialization function can access the interpreter before the GUI is initialized.
        wikiDesktop.commandInterpreter=commandInterpreter;
        GuiActions.StructuredDocumentActions.loadBaseCommandInterpreterSubsystem();

        // add the command interpreter presenter to the GUI.
        MainPresenter.commandInterpreter=commandInterpreter;
    }
    wikiDesktop.messageDisplay=MainPresenter.messageDisplayPresenter;
    wikiDesktop.inputForm=MainPresenter.inputFormPresenter;
    wikiDesktop.contextMenu=MainPresenter.contextMenu;
    wikiDesktop.confirmationDialog=MainPresenter.confirmationDialog;
    wikiDesktop.recentlyOpenedWikiPanel=MainPresenter.recentlyOpenedWikiPanel
    wikiDesktop.sequencePresenter=MainPresenter.sequencePresenter;
    wikiDesktop.topWindowPanel=MainPresenter;
    wikiDesktop.popupPanel=MainPresenter.popupPanel;
    MainPresenter.render();
    MainPresenter.initJavascript();
    return MainPresenter;
};