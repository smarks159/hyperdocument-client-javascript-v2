"use strict";
const {ipcRenderer,webFrame,shell,clipboard}=nodeRequire('electron');

const {Node:ProseMirrorNode,Slice} = nodeRequire("prosemirror-model");
const {EditorState,Selection} = nodeRequire("prosemirror-state");
const {EditorView} = nodeRequire("prosemirror-view");

const {toggleMark} = nodeRequire("prosemirror-commands");

const path = nodeRequire("path");

var wikiDesktop;
var dataActionDispatcher;

ipcRenderer.on("setZoomLevel",(event,zoomLevel)=>{
    webFrame.setZoomLevel(zoomLevel);
    event.returnValue=true;
})

ipcRenderer.on("setRecentlyOpenedWikiFiles",(event,recentlyOpenedWikiFiles)=>{
    wikiDesktop.recentlyOpenedWikiFiles=recentlyOpenedWikiFiles;
})

ipcRenderer.on("loadFile",(event,fileName)=>{
    GuiActions.WikiActions.loadWikiFile(fileName);
    event.returnValue=true;
});

ipcRenderer.on("loadState",(event)=>{
    GuiActions.state.loadState();
});

window.onbeforeunload=function(){
    GuiActions.state.saveState();
}

// must use the onload event, not jquery's ready event to garuntee that did-finish-load in the electron code fires after onload.
window.onload=function(){

    wikiDesktop=new Desktop.WikiDesktop();
    // Everytime a change in the data is made, we setUnsavedChanges to true. This information is used to remind the user to save unsaved changes before closing the program.
    let setUnsavedChangesCallback=()=>{
        GuiActions.WikiActions.setUnsavedChanges(true);
    }
    dataActionDispatcher=new ActionDispatcher({
        "moveBranch":GUIDataActions.moveBranch,
        "moveRange":GUIDataActions.moveRange,
        "copyRange":GUIDataActions.copyRange,
        "insertNode":GUIDataActions.insertNode,
        "insertTextAtNode":GUIDataActions.insertTextAtNode,
        "insertHTMLAtNode":GUIDataActions.insertHTMLAtNode,
        "deleteNode":GUIDataActions.deleteNode,
        "deleteRange":GUIDataActions.deleteRange,
        "updateProseMirrorData":GUIDataActions.updateProseMirrorData,
        "createFile":GUIDataActions.createFile,
        "removeFile":GUIDataActions.removeFile,
        "renameFile":GUIDataActions.renameFile,
        "createFileFromText":GUIDataActions.createFileFromText,
        "splitText":GUIDataActions.splitText
    },setUnsavedChangesCallback)
    GuiActions.Initialization.Initialize();
}

$(window).bind('mousedown',function(event){
    // event.button 1 is middle click
    if(event.button==1){
        GuiActions.WikiActions.toggleCommandInterpreter();
    }
});

let GlobalShortcuts=createKeypress(document.body);

GlobalShortcuts.register_many([
    {
        "keys":"ctrl s",
        "is_exclusive":true,
        "on_keyup":(e)=>{
            if(wikiDesktop.currentWikiFile!==null){
                GuiActions.WikiActions.saveFile(wikiDesktop.currentWikiFile,wikiDesktop.wikiData.propertyGraph.saveToData());
                wikiDesktop.messageDisplay.displaySuccessMessage(`The wiki has been saved to ${wikiDesktop.currentWikiFile}`);

                setTimeout(function(){
                    wikiDesktop.messageDisplay.clearMessage();
                },1000);
            }
            else{
                GuiActions.WikiActions.saveWiki()
            }
        }
    },
    {
        "keys":"f12",
        "is_exclusive":true,
        "on_keyup":(e)=>{
            ipcRenderer.sendSync("toggleDevTools");
        }
    },
    {
        "keys":"f5",
        "is_exclusive":true,
        "on_keyup":(e)=>{
            // If a wiki file is open when calling refresh make sure to save it to prevent losing any data.
            if(wikiDesktop.currentWikiFile!==null){
                GuiActions.WikiActions.saveFile(wikiDesktop.currentWikiFile,wikiDesktop.wikiData.propertyGraph.saveToData());
            }
            ipcRenderer.sendSync("reloadPage");
        }
    },
    {
        "keys":"esc",
        "is_exclusive":true,
        "on_keyup":(e)=>{
            GuiActions.StructuredDocumentActions.clearClipboard()
        }
    },
    {
        "keys":"ctrl z",
        "is_exclusive":true,
        "on_keydown":(e)=>{
            GUIDataActions.undo();
        }
    },
    {
        "keys":"ctrl y",
        "is_exclusive":true,
        "on_keydown":(e)=>{
            GUIDataActions.redo();
        }
    },
    {
        "keys":"ctrl m",
        "is_exclusive":true,
        "on_keydown":(e)=>{
            GuiActions.WikiActions.toggleCommandInterpreter();
        }
    },
    {
        "keys":"ctrl =",
        "is_exclusive":true,
        "on_keydown":(e)=>{
            let currentZoomLevel=ipcRenderer.sendSync("getZoomLevel");
            ipcRenderer.sendSync("setZoomLevel",currentZoomLevel+1);
        }
    },
    {
        "keys":"ctrl -",
        "is_exclusive":true,
        "on_keydown":(e)=>{
            let currentZoomLevel=ipcRenderer.sendSync("getZoomLevel");
            ipcRenderer.sendSync("setZoomLevel",currentZoomLevel-1);
        }
    },
    {
        "keys":"ctrl 0",
        "is_exclusive":true,
        "on_keydown":(e)=>{
            ipcRenderer.sendSync("setZoomLevel",0);
        }
    },


]);
