"use strict";

Fragments.ComposerFragment=class extends Fragments.Fragment{
    
}

Fragments.flexbox=class flexbox extends Fragments.ComposerFragment{
    constructor(children){
        super();        
        this._children=children;
        this._children.forEach((child,index)=>{
            if(!(child instanceof Fragments.Fragment)){
                throw new Error("Child #"+index+" is not a Fragment");
            }
            child.parent=this;
        })
        this.shrinkAttr=0;
        this.directionAttr="column";
        this.align_itemsAtrr="";
    }
    
    get children(){
        return this._children;
    }
    
    set children(value){
        this._children=value;
    }
    
    direction(directionAttr){
        this.directionAttr=directionAttr;
        return this;
    }
    
    align_items(alignVal){
        this.align_itemsAtrr=alignVal;
        return this;
    }
    
    render(){
        var div=document.createElement("div");
        // this is required in order for the focus method to work properly. Some events, such as keypress events, will not work properly
        // unless this property is set and the element has the focus.
        div.setAttribute("tabindex","0")
        //min-height and min-width is needed on every flex layout div to make scrolling of flex panels work properly.
        $(div).css({
            display:"flex",
            "flex-direction":this.directionAttr,        
            "align-items":this.align_itemsAtrr,
            "min-height":"0px",
            "min-width":"0px"
        });          
        for(var i=0;i<this.children.length;i++){
            if(this.children[i]!==null){
                if(this.children[i] instanceof Fragments.blank){
                    if(this.directionAttr==="column"){
                        this.children[i].direction("vertical");
                    }
                    else if(this.directionAttr==="row"){
                        this.children[i].direction("horizontal");
                    }
                }
                this.children[i].render();
                div.appendChild(this.children[i].htmlEl);
            }
        }
        this.htmlEl=div;
        this.renderAttributes();
    }
    
    appendChild(fragment){
        fragment.parent=this;
        this.children.push(fragment);
    }
}

/**
 * The ScrollingPanel class takes a fragment or presenter and wraps it in a scrollable panel. The class is responsible for maintaining the scrolling position when the element is refreshed. In order to main the view state in a way that is compatible with the current GUI system, a panel is required to have a unique name, which is passed in through the constructor. This name is used to query the view model in which the scroll state is stored. 
 */
Fragments.ScrollingPanel=class Panel extends Fragments.ComposerFragment{
    constructor(innerObj,viewModelName){
        super();
        if(!(innerObj instanceof Fragments.Fragment)){
            throw new Error("Inner object is not a fragment");
        }
        this.innerObj=innerObj;
        this.innerObj.parent=this;
        // Store viewModelName for debugging purposes.
        this.viewModelName=viewModelName;
        this.viewModel=wikiDesktop.viewModels.getViewModel(viewModelName);        
        this.onScroll(event=>{
            let htmlEl=event.target;
            this.viewModel.setAttribute("scrollTop",htmlEl.scrollTop);
            this.viewModel.setAttribute("scrollLeft",htmlEl.scrollLeft);
        })
    }

    get children(){
        return [this.innerObj];
    }

    initJavascript(){
        super.initJavascript();
        if(this.viewModel.hasAttribute("scrollTop")){
            this.htmlEl.scrollTop=this.viewModel.getAttribute("scrollTop");
        }
        if(this.viewModel.hasAttribute("scrollLeft")){
            this.htmlEl.scrollLeft=this.viewModel.getAttribute("scrollLeft");
        }
    }

    render(){
        var div=document.createElement("div");
        // this is required in order for the focus method to work properly. Some events, such as keypress events, will not work properly
        // unless this property is set and the element has the focus.
        div.setAttribute("tabindex","0");        
        div.setAttribute("data-presentertype","scrollingPanel")
        this.innerObj.render();
        div.appendChild(this.innerObj.htmlEl);
        this.htmlEl=div;
        this.addClass("scrollingPanel");
        this.renderAttributes(); 
    }
}

/**
 * This is a fragment that contains content that can be changed dynamically.
 */
Fragments.HolderFragment=class HolderFragment extends Fragments.ComposerFragment{
    constructor(){
        super();
        this._fragmentToDisplay=null;
    }

    get children(){
        if(this._fragmentToDisplay){
            return [this._fragmentToDisplay];
        }
        else{
            return [];
        }
    }

    get content(){
        return this._fragmentToDisplay;
    }

    set content(fragment){
        this.setContent(fragment);
        return this;
    }

    setContent(fragment){
        this.updateContent(fragment);
        return this;
    }

    updateContent(fragment){
        let previousFragment=this._fragmentToDisplay;
        this._fragmentToDisplay=fragment;
        if(this.htmlEl){
            if(previousFragment){
                previousFragment.destroyJavascript();
                let previousDiv=this.htmlEl;
                this.render();
                previousDiv.replaceWith(this.htmlEl);
            }
            else{
                this._fragmentToDisplay.render()
                this.htmlEl.appendChild(this._fragmentToDisplay.htmlEl)
            }
            this._fragmentToDisplay.initJavascript();
        }
    }

    render(){
        let div=document.createElement("div");
        // this is required in order for the focus method to work properly. Some events, such as keypress events, will not work properly
        // unless this property is set and the element has the focus.
        div.setAttribute("tabindex","0");        
        div.setAttribute("data-presentertype","holder")
        if(this._fragmentToDisplay){
            this._fragmentToDisplay.render();
            div.appendChild(this._fragmentToDisplay.htmlEl);
        }
        this.htmlEl=div;
        this.renderAttributes();
    }

    focus(){
        if(this._fragmentToDisplay){
            this._fragmentToDisplay.focus();
        }
    }
}