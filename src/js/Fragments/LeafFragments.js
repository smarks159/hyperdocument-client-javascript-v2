"use strict";

Fragments.LeafFragment=class LeafFragment extends Fragments.Fragment{
    get children(){
        return [];
    }
}

Fragments.textLabel=class textLabel extends Fragments.LeafFragment{
    constructor(text){
        super();
        this.textAttr=text;
    }

    renderAttributes(){
        super.renderAttributes();
        if(this.textAttr!==null){
            this.htmlEl.innerHTML=this.textAttr;
        }
        return this.htmlEl;
    }

    render(){
        this.htmlEl=document.createElement("div");
        this.renderAttributes();
    }

    text(text){
        this.textAttr=text;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
}

/*
This is an input box that supports content that is bigger than the width of the input element. It uses a textarea with the enter key disabled to simulate an input box with wrapping of text that is too big.
*/
Fragments.input=class input extends Fragments.LeafFragment{
    constructor(){
        super();
        this.nameAttr=null;
        this.autocompleteAttr=null;
        this._onInput=null;
        this._onEnter=null;
        this.keyboardListener=null;
    }

    name(name){
        this.nameAttr=name;
        return this;
    }

    autocomplete(autocomplete){
        this.autocompleteAttr=autocomplete;
        return this;
    }

    value(value){
        this.valueAttr=value;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }

    getValue(){
        return this.htmlEl.value;
    }

    onInput(callback){
        this._onInput=callback;
        return this;
    }

    onEnter(callback){
        this._onEnter=callback;
    }

    resizeToContent(){
        this.htmlEl.style.height=this.htmlEl.scrollHeight+"px";
    }

    render(){
        this.htmlEl=document.createElement("textarea");
        // The default spellcheck has been disabled because right clicking on a mispelled word because the default browser context menu is overridden by the hyperdocument system's context menu.
        this.htmlEl.setAttribute("spellcheck",'false');
        this.css({
            "overflow":"hidden",
            "resize":"none",
            "padding":"5px",
            "min-height":"2em"
        })
        this.renderAttributes();
    }

    _initJavascript(){
        this.resizeToContent();
        this.keyboardListener=createKeypress(this.htmlEl);
        this.keyboardListener.register_many([
            {
                "keys":"enter",
                "is_exclusive":true,
                "on_keydown":(e)=>{
                    if(this._onEnter){
                        this._onEnter()
                    }
                    e.preventDefault()
                    return false;
                }
            },
            // prevent ctrl z and ctrl y from triggering the global undo/redo shortcut.
            {
                "keys":"ctrl z",
                "is_exclusive":true,
                "on_keydown":(e)=>{
                     e.stopPropagation()
                     return false;
                }
            },
            {
                "keys":"ctrl y",
                "is_exclusive":true,
                "on_keydown":(e)=>{
                     e.stopPropagation()
                     return false;
                }
            }

        ])
    }

    _destroyJavascript(){
        this.keyboardListener.destroy();
        this.keyboardListener=null;
    }
    
    renderAttributes(){        
        super.renderAttributes();
        if(this.nameAttr!==null){
            this.htmlEl.name=this.nameAttr;
        };
        if(this.autocompleteAttr!==null){
            this.htmlEl.setAttribute("autocomplete",this.autocompleteAttr);
        }
        if(this.valueAttr!==null){
            this.htmlEl.value=this.valueAttr;
            this.resizeToContent();
        }
        let onInput=null;
        if(this._onInput!==null){
            onInput=(event)=>{
                this.resizeToContent();
                this._onInput(event);
            }
        } else{
            onInput=(event)=>this.resizeToContent();
        }
        this.htmlEl.oninput=onInput;
    }
}

Fragments.ProseMirrorEditor= class ProseMirrorEditor extends Fragments.LeafFragment{
    constructor(){
        super();
        this.editor=null;
        // used to determine whether the contents of the document has changed.
        this.previousDoc=null;
        this.keyboardListener=null
        this._onChange=null;
        this._readOnly=false;
    }

    render(){
        this.htmlEl=document.createElement("div");
        // tabindex is required to get the focus method to scroll to the element when it is not displayed on the screen.
        this.htmlEl.setAttribute("tabindex","0")

        /**
         * The default behavior for spellcheck does not work for two reasons:
         *   1) Spell Checking works when a single paragraph is selected. The misspelled words will be highlighted with a red squiggle, however, this is not applied when a paragraph is not selected.
         *   2) Right Clicking on the red squiggle when it appears does not work either because my program overrides the default browser menu and does not have menu commands related to spell check.
         * 
         * For these reasons the default spellcheck behavior has been disabled.
         * */ 
        this.htmlEl.setAttribute("spellcheck",'false');
        this.renderAttributes();
    }

    readOnly(readOnly){
        this._readOnly=readOnly;
        return this;
    }

    onChange(callback){
        this._onChange=callback;
        return this;
    }

    _initJavascript(){
        this.keyboardListener= createKeypress(this.htmlEl);
        let state;
        if(this.dataNode && this.dataNode.doc){
            state = EditorState.create({doc:this.dataNode.doc})
        }
        // When the dataNode is first created, before the user types in anything the data should be set to an empty document.
        else if(this.dataNode && !this.dataNode.doc){
            state = EditorState.create({schema:DataModels.ProseMirrorDataModel.Schema});
            this.dataNode.doc=state.doc;
        }
        else{
            state = EditorState.create({schema:DataModels.ProseMirrorDataModel.Schema});
        }

        let domEvents={};
        for(let key in Object.keys(this.domEventHandlers)){
            domEvents[key]=(view,event)=>{
                this.domEventHandlers[key](event);
            }
        }
        // By default prosemirror allows you the drag and drop text, this is not the behavior that I want. This behavior is defined in the dragstart event handler, so we must override it to prevent it from happening.
        domEvents["dragstart"]=(view,event)=>{
            event.preventDefault();
        }
        this.editor= new EditorView(this.htmlEl,{
            handleDOMEvents:domEvents,
            editable:(state)=>this._readOnly,
            state,
            dispatchTransaction:(transaction)=>{
                let newState = this.editor.state.apply(transaction);

                // only update the data when the content has changed.
                if((!this.previousDoc.eq(newState.doc))){
                    if(this._onChange){
                        this._onChange(newState,this);
                    }
                }
                this.editor.updateState(newState);
                this.previousDoc=this.editor.state.doc;
            },
            /** In prosemirror, holding down ctrl while pressing click will select the entire node. In this program that means the selection will be expanded to the entire paragraph without highlighting the text and it will deselect the paragraph. Right clicking on this node afterwards will bring up the wrong menu.
             * It will bring up the range menu because a range is selected, instead of the that should be brought up when a single node is selected. Checking that the ctrl clicked when pressed and returning true prevents the default behavior. This check must be done in handleClickOn and not in handleClick.
             */
            handleClickOn:(view,pos,node,nodePos,event,direct)=>{
                if(event.ctrlKey){
                    return true;
                }
            }
        });

        this.previousDoc=this.editor.state.doc
    }

    _destroyJavascript(){
        this.keyboardListener.destroy();
        this.keyboardListener=null;
        this.editor.destroy();
    }

    setSelection(selectionState){
        let tr = this.editor.state.tr;
        tr.setSelection(selectionState);
        this.editor.dispatch(tr);
    }

    hasFocus(){
        return this.editor.hasFocus();
    }

    focus(){
        // calling focus on the html element is necessary to get the browser to scroll to the element when it is not on the screen.
        this.htmlEl.focus();
        this.editor.focus();
    }

    getText(){
        return this.editor.state.doc.textContent;
    }

    insertTextOverSelection(text){
        let tr=this.editor.state.tr;
        tr.insertText(text);
        this.editor.dispatch(tr);
    }

    insertSliceOverSelection(slice){
        let tr=this.editor.state.tr;
        tr.replaceSelection(slice)
        this.editor.dispatch(tr);
    }

    isRangeSelected(){
        let selection=this.editor.state.selection
        if(selection.to===selection.from){
            return false;
        }
        else{
            return true;
        }
    }

    isMarkSelected(markObject){
        let state=this.editor.state;
        let selection=state.selection;
        // rangeHasMark does not work when from and to are the same so add one to, to in order to check the first character after the cursor.
        let from=selection.from;
        let to=selection.to;
        if(from===to){
            to+=1;
        }
        if(state.doc.rangeHasMark(from,to,markObject)){
            return true;
        }
        else{
            return false;
        }
    }

    isInternalLinkSelected(){
        let state=this.editor.state;
        return this.isMarkSelected(state.schema.marks.internalLink)
    }

    isLinkSelected(){
        let state=this.editor.state;
        return this.isMarkSelected(state.schema.marks.link);
    }
    
    getSelectedLinkData(){
        let state=this.editor.state;
        let selection=state.selection;
        if(selection.from!==selection.to && !this.isLinkSelected()){
            return [false,false];
        }
        let node=state.doc.nodeAt(selection.from);
        let linkMark=node.marks.find(mark=>{
            return mark.type===state.schema.marks.link;
        })
        return [linkMark.attrs.href,node.text];
    }

    getSelectedInternalLinkData(){
        let state=this.editor.state;
        let selection=state.selection;
        if(selection.from!==selection.to && !this.isInternalLinkSelected()){
            return [false,false];
        }
        let node=state.doc.nodeAt(selection.from);
        let linkMark=node.marks.find(mark=>{
            return mark.type===state.schema.marks.internalLink;
        })
        return [linkMark.attrs.objectId,linkMark.attrs.edgeId,node.text];
    }
    
    /**
     * When the cursor is over a link and a range is not selected, return the indexes of the start and end of the link. This is used for highlighting a link when right clicking on the context menu.
     * @returns {[startIndex,endIndex]}
     */
    getSelectedLinkIndexRange(){
        let state=this.editor.state;
        let selection=state.selection;
        if(selection.from!==selection.to && (!this.isLinkSelected() || !this.isInternalLinkSelected())){
            return [false,false];
        }
        let node=state.doc.nodeAt(selection.from);
        
        let startIndex=selection.from;
        let endIndex=selection.from;
        
        while(startIndex > 0 && state.doc.nodeAt(startIndex-1)===node){
            startIndex--;
        }
        
        while(endIndex < state.doc.nodeSize && state.doc.nodeAt(endIndex+1)===node){
            endIndex++;
        }
        return [startIndex,endIndex+1];
        
    }
    
    highlightRange([startIndex,endIndex]){        
        let state=this.editor.state;
        let tr=state.tr;
        tr.addMark(startIndex,endIndex,state.schema.marks.highlightText.create());
        this.editor.dispatch(tr);
    }
    
    unhighlightRange([startIndex,endIndex]){
        let state=this.editor.state;
        let tr=state.tr;
        tr.removeMark(startIndex,endIndex,state.schema.marks.highlightText);
        this.editor.dispatch(tr);
    }

    getCursorStartPos(){
        return this.editor.state.selection.from;
    }

    getSelectedTextAsPlainText(){
        if(!this.isRangeSelected()){
            return null;
        }
        let [start,end]=this.getSelectionRange()
        return this.getText().slice(start-1,end);
    }
    
    getSelectionRange(){
        return [this.editor.state.selection.from,this.editor.state.selection.to];
    }

    getSelectionOffset(){
        let coords=this.editor.coordsAtPos(this.editor.state.selection.from);
        coords.height=coords.bottom-coords.top;
        return coords;
    }

    /**
     * 
     * @returns The content of the select as a prosemirror fragment.
     */
    getSelectionContent(){
        if(!this.isRangeSelected()){
            return null;
        }
        else{
            return this.editor.state.selection.content().content
        }
    }

    getNodeSubset(from,to=null){
        if(to){
            return this.editor.state.doc.cut(from,to)
        }
        else{
            return this.editor.state.doc.cut(from)
        }
    }

    getSlice(from,to=null){
        if(to){
            return this.editor.state.doc.slice(from,to)
        }
        else{
            return this.editor.state.doc.slice(from)
        }
    }

    toggleBold(){
        toggleMark(this.editor.state.schema.marks.strong)(this.editor.state,this.editor.dispatch);
    }

    addLink(url){
        let state=this.editor.state;
        let selection=state.selection;

        if(!selection.empty){
            let tr=state.tr
            this.editor.dispatch(tr.addMark(selection.from,selection.to,state.schema.marks.link.create({href:url})));
        }
    }

    addLinkInternal(objectId,edgeId){
        let state=this.editor.state;
        let selection=state.selection;

        if(!selection.empty){
            let tr=state.tr
            this.editor.dispatch(tr.addMark(selection.from,selection.to,state.schema.marks.internalLink.create({objectId:objectId,edgeId:edgeId})));
        }
    }
    
    editLink(startIndex,endIndex,url){
        let state=this.editor.state;
        let tr=state.tr;
        tr.removeMark(startIndex,endIndex+1,state.schema.marks.link);
        tr.addMark(startIndex,endIndex+1,state.schema.marks.link.create({href:url}));
        this.editor.dispatch(tr);
    }
    
    removeLink(startIndex,endIndex){
        let state=this.editor.state;
        let tr=state.tr;
        tr.removeMark(startIndex,endIndex,state.schema.marks.link);
        this.editor.dispatch(tr);
    }

    removeInternalLink(startIndex,endIndex){
        let state=this.editor.state;
        let tr=state.tr;
        tr.removeMark(startIndex,endIndex,state.schema.marks.internalLink);
        this.editor.dispatch(tr);
    }
}


Fragments.button=class button extends Fragments.LeafFragment{
    constructor(){
        super();
        this.textAttr="";
    }

    text(text){
        this.textAttr=text;
        return this;
    }

    click(){
        this.htmlEl.click();
    }

    render(){
        var div=document.createElement("button");
        div.innerHTML=this.textAttr;
        this.htmlEl=div;
        this.renderAttributes();
    }
}

Fragments.SelectElement=class SelectElement extends Fragments.LeafFragment{
    constructor(){
        super();
        this.choices=[];
        this.onSelectAttr=null;
    }

    render(){
        var selectEl=document.createElement("select");
        var optionEl;
        for(var i=0;i<this.choices.length;i++){
            optionEl=document.createElement("option");
            optionEl.text=this.choices[i].text;
            optionEl.value=this.choices[i].value.toString();
            selectEl.add(optionEl);
        }
        this.htmlEl=selectEl;
        this.htmlEl.onchange=this.onSelectAttr;
        this.renderAttributes();
        return selectEl;
    }

    getSelectedValue(){
        return this.htmlEl.options[this.htmlEl.selectedIndex].value;
    }

    onSelect(callback){
        this.onSelectAttr=callback;
        return this;
    }
}

Fragments.blank=class blank extends Fragments.LeafFragment{
    constructor(){
        super();
        this._size=null;
        this._direction="horizontal";
    }
    
    size(size){
        this._size=size;
        return this;
    }
    
    direction(direction){
        this._direction=direction;
        return this;
    }
    
    render(){
        let size;
        if(typeof this._size==="number"){
            size=this._size.toString()+"px";
        }
        else{
            size=this._size
        }
        this.htmlEl=document.createElement("div");
        if(this._direction==="horizontal" && this.size){
            this.htmlEl.style.width=size;
        }
        else if(this._direction==="vertical" && this.size){
            this.htmlEl.style.height=size;
        }
        this.renderAttributes();
    }
}


