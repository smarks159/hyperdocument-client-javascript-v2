"use strict";

var Fragments={};
var BuildPresenterFromPersistenceData=function(data){
    var typeToFactoryMap={        
        "SingleNodeWindow":Presenters.SingleNodeWindow.loadFromData,
        "MainPresenter":Presenters.MainPresenter.loadFromData,
        "StructuredDocumentPresenter":Presenters.StructuredDocumentPresenter.loadFromData
    };
    if(typeToFactoryMap[data.type]===undefined){
        throw new Error("The node type "+data.type+" is not supported");
    }
    let newObj=typeToFactoryMap[data.type](data);
    
    if(newObj===null || newObj===undefined){
        throw new Error("loadFromData does not return an object for type: "+data.type);
    }
    
    return typeToFactoryMap[data.type](data);
};


Fragments.Fragment=class Fragment{
    constructor(){
        this.parent=null;
        
        this._classes=new Set();        
        this.flexAttr=null;
        this.shrinkAttr=null;
        this.domEventHandlers={}
        this.cssAttr=null;
        this.htmlEl=null;
        this.visible=true;
        this.javascriptInitialized=false;
    }
    
    get children(){
        throw Error("This function must be implemented in the child");
    }    
    
    hasChildren(){
        return this.children.length>0;
    }
    
    hasParent(){
        return this.parent!==null;        
    }

    /**
     * When rendering a fragment, the fragment is thrown out and a new one is created, so the state is not preserved. Some of the presenters rely on this behavior in their definition methods, so this behavior cannot be changed. In order to maintain state in the current system you need to pass a unique name to the presenter or fragment when they are defined, in the presenter definition method. The state of the presenter should then be stored in a view model that has the same name. This way the same view model can be retrieved. See the panel composer class as an example of how to do this. 
     * 
     * Creating a view model in the fragment constructor and storing a reference to the view model will not work because the fragment object is recreated every single time, including any references. To solve this problem properly would most likely require a major rewrite of sometype. For most cases, the current system works well and I don't think a rewrite is justified at this time.
     */
    render(){
        throw Error("This function must be implemented in the child");
    }

    /**
     * Refresh, is a thin wrapper over render. Refresh is not called recursively on children and will instead call render recursively. Due to this fact, most of the time you do not want to override the refresh method of a fragment unless you plan on calling in directly. Since refresh calls render, it does not preserve state either. See the render method documentation about the caveats about storing persistent state.
     */
     refresh(){
        // Only execute refresh on Presenters that have been rendered already. Calling refresh on non-rendered windows can happen when initially opening a wiki and one window is hidden and has not been rendered yet.;
        if(this.htmlEl){
            this.destroyJavascript()
            let oldHtmlEl=this.htmlEl;
            this.render();
            oldHtmlEl.replaceWith(this.htmlEl);
            this.initJavascript();
        }
    }
    
    /**
     * Fragments are not currently implement as property graph nodes, so loadFromData behaves differently that graph serialization. In the future, all fragments should be implemented as property graph nodes, but it is not currently. This is currently only used when refreshing the browser window in debug mode.
     * 
     * The differences are as follows:
     *    - First every fragment does not implements serialization and serialization does not happen automatically. Typically what happens is the entire presenter is rebuilt from scratch. In the cases where serialization is implemented serialization is initialized in MainPresenter and the hierarchy is manually serialized and deserialized. You must be careful to explicitly save child elements in saveFromData and you must be careful to set the parent field in loadFromData.
     *    - The problem with manually setting loadFromData is that the fragment hierarchy is not reproduced in full as defined in the definition method, so far there has not been any issues with this because the use of serializing/deserializing presenters is limited to refreshing the browser window, which is only used in debug mode. This is a bug in the architecture and should be fixed at some point by implementing fragments and presenters as property graph nodes.
     */
    loadFromData(data){
        throw Error("This function must be implemented in the child");
    }
    
    saveNodeToData(){
        throw Error("This function must be implemented in the child");
    }

    /** Some javscript libraries require that javascript be attach to the element after it has been attached to the DOM so initJavascript should be called after inserting the html element into the document.
     * 
     * Initialize Javascript starts at the bottom of the tree because some parents call javascript libraries of the children upon initialization, so the children must be initialized first.
     * 
     * Define _initJavascript in any fragment class to initialize javascript
     */
    initJavascript(){
        for(let child of this.children){
            child.initJavascript();
        }
        if(this._initJavascript){
            this.javascriptInitialized=true;
            this._initJavascript();
        }
    }

    /** Some javascript libraries must be explicitly destroyed after using them to prevent memory leaks 
     * Define _destroyJavascript in any fragment class to initialize javascript
     * **/
    destroyJavascript(){
        for(let child of this.children){
            child.destroyJavascript();
        }
        if(this._destroyJavascript){            
            if(this.javascriptInitialized){
                this._destroyJavascript();
                this.javascriptInitialized=false;
            }
        }
    }

    findPresenterByHtmlEl(htmlEl){
        if(this.htmlEl===htmlEl){
            return this;
        }
        for(let child of this.children){            
            let results=child.findPresenterByHtmlEl(htmlEl);
            if(results!==null){
                return results;
            }
        }
        return null;
    }
    
    findFirstChildByType(objectType){
        for(let child of this.children){        
            if(child instanceof objectType){
                return child;
            }
            let results=child.findFirstChildByType(objectType);
            if(results!==null){
                return results;
            }
        }
        return null;        
    }
    
    findFirstParentByType(objectType){
        let match=null;
        let currentNode=this;
        
        while(match===null && currentNode.parent!==null){
            currentNode=currentNode.parent;
            if(currentNode instanceof objectType){
                return currentNode;
            }
        }
        return null;
    }
    
    findFirstChildByObjectId(objectId){
        for(let child of this.children){
            
            if(child instanceof Presenters.Presenter && child.dataNode!==null && child.dataNode.objectId===objectId){
                return child;
            }
            let results=child.findFirstChildByObjectId(objectId);
            if(results!==null){
                return results;
            }
        }
        return null;
    }

    /**
     * Given an objectId find all the presenters associated with the objectId, starting from the current node and searching down the tree. This is used to synchronize TextPresenter operations. Instead of refreshing all windows after every keystroke, just refresh the presenter that changed.
     */
    findAllChildrenByObjectId(objectId){
        let foundPresenters=[];        
        this.findAllChildrenByObjectInner(objectId,foundPresenters);
        return foundPresenters
    }

    findAllChildrenByObjectInner(objectId,foundPresenters){
        for(let child of this.children){
            if(child instanceof Presenters.Presenter && child.dataNode!==null && child.dataNode.objectId===objectId){
                foundPresenters.push(child);
            }
            child.findAllChildrenByObjectInner(objectId,foundPresenters)
        }
    }

    /**
     * This method is used to copy attributes from a presenter object instance, which is not rendered, to the top level object returned by the definition object. 
     * If an attribute is set in the presenter instance, it will generally override any attributes set in the definition object. Though, this is not always the case and this method should be called before calling renderAttribute.
     * 
     */
    copyAttributes(otherFragment){
        for(let attr of ["flexAttr","shrinkAttr","onClickAttr"]){
            if(otherFragment[attr]!==null && otherFragment[attr]!==undefined){
                this[attr]=otherFragment[attr];
            }
        }
        if(otherFragment["_classes"].size>0){
            // If a class is set on the instance of the presenter object and also in the definition class, then we need to combine the classes and not override them.
            for(const cls of otherFragment["_classes"]){
                this["_classes"].add(cls);
            }
        }
        if(otherFragment.cssAttr!==null && otherFragment.cssAttr!==undefined){
            this.css(otherFragment.cssAttr);
        }
    }
    
    renderAttributes(){
        if(this._classes.size>0){
            this.htmlEl.className=Array.from(this._classes).join(" ")
        }
        else{
            this.htmlEl.className="";
        }
        if(this.flexAttr!==null){
            this.htmlEl.style.flex=this.flexAttr.toString();
        }
        if(this.shrinkAttr!==null){
            this.htmlEl.style.flexShrink=this.shrinkAttr.toString();
        }
        
        if(this.cssAttr!==null){
            $(this.htmlEl).css(this.cssAttr);
        }
        
        for(let eventName of Object.keys(this.domEventHandlers)){
            // prevent callbacks from being called multiple times
            this.htmlEl.removeEventListener(eventName,this.domEventHandlers[eventName],false);
            this.htmlEl.addEventListener(eventName,this.domEventHandlers[eventName],false);
        }
    }
    
    class(className){
        this._classes=new Set([className]);
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    addClass(className){
        this._classes.add(className);
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    removeClass(className){
        this._classes.delete(className);
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    flex(flexNum=1){
        this.flexAttr=flexNum;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    shrink(shrinkNum=1){
        this.shrinkAttr=shrinkNum;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    css(css){
        if(this.cssAttr!==null){
            for(let key in css){
                this.cssAttr[key]=css[key];
            }
        } 
        else{
            this.cssAttr=css;
        }    
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    onClick(callback){
        this.domEventHandlers.click=callback;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    onDblClick(callback){
        this.domEventHandlers.dblclick=callback;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    onMouseDown(callback){
        this.domEventHandlers.mousedown=callback;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    onContextMenu(callback){
        let callbackwrapper=(event)=>{
            // // Disable the context menu when the command interpreter is activate. The input keys in all input boxes trigger the command interpreter.
            if(wikiDesktop.commandInterpreterActive===true){
                return false;
            }
            callback(event);
             // context menus should not have bubbling events, otherwise when having nested objects, only the top level context menu will be displayed.
             event.stopPropagation();
        }
        this.domEventHandlers.contextmenu=callbackwrapper;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    onMouseOver(callback){
        this.domEventHandlers.mouseover=callback;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    onMouseOut(callback){
        this.domEventHandlers.mouseout=callback;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    onBlur(callback){
        this.domEventHandlers.blur=callback;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }
    
    onFocus(callback){
        this.domEventHandlers.focus=callback;
        if(this.htmlEl!==null){
            this.renderAttributes();
        }
        return this;
    }

    onScroll(callback){
        this.domEventHandlers.scroll=callback;
        if(this.htmlEl!=null){
            this.renderAttributes();
        }
        return this;
    }
    
    focus(){
        this.htmlEl.focus();
    }

    hide(){
        this.addClass("hidden");
        this.visible=false;
        return this;
    }
    
    show(){
        this.removeClass("hidden");
        this.visible=true;
        return this;
    }
    
    isVisible(){
        return this.visible;        
    }
}