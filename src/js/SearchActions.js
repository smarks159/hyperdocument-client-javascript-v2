"use strict";

GuiActions.SearchActions={};

let SearchResult=class SearchResult{
    constructor(positions,node){
        this.positions=positions;
        this.node=node;
    }

    getStructuralAddress(){
        let doc=new TreeNodeInterface(this.node).findParentByType(DataModels.StructuredDocumentDataModel);
        return [doc.getFile().fileName,doc.getNodeStructuralAddress(this.node)];
    }
}

GuiActions.SearchActions.searchText=function(textToSearch){
    let proseMirrorNodes=wikiDesktop.wikiData.propertyGraph.getNodeByType(DataModels.ProseMirrorDataModel);
    let searchResults=[];
    for (const node of proseMirrorNodes) {
        let textPositions=node.searchText(textToSearch);
        if(textPositions.length>0){
            searchResults.push(new SearchResult(textPositions,node))
        }
    }
    return searchResults;
}

GuiActions.SearchActions.openAddressFromSearchResult=function(searchResult,displayPresenter){
    let nodeToJumpTo=searchResult.node;
    let inputForm= new Presenters.KeyboardChoiceDialog([
        {"key":"l","text":"Left","value":"left"},
        {"key":"r","text":"Right","value":"right"}
    ]).textToDisplay("Which window do you want to open the link in?")
    wikiDesktop.popupPanel.setContent(inputForm);
    wikiDesktop.popupPanel.openAsPopup(displayPresenter);
    inputForm.onResponse((value)=>{
        let windowManager=wikiDesktop.topWindowPanel.windowManager;
        let window=windowManager.getWindowById(value);
        if(!window.isVisible()){
            window.show();
            windowManager.refresh();
        }
        GuiActions.WikiActions.selectWindow(value);
        GuiActions.StructuredDocumentActions.jumpToElement(nodeToJumpTo);
        wikiDesktop.popupPanel.closePopup();
    })
}