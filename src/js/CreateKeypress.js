
/**
 * Constructor function the build a keypress listener object. This is necessary because the keypress library attaches its listener to the toplevel document if no html element is passed into the constructor or if the html element is null or undefined. This causes really hard to find bugs when it is done unintentionally because it attaches multiple listeners to the top level document. In order to prevent these bugs use this function to create the keypress library rather than constructor provided by the library.
 * 
 * @param {HTMLElement} htmlEl
 */
function createKeypress(htmlEl){
    if(!htmlEl){
        throw new Error("The html element is not defined");
    }
    return new keypress.Listener(htmlEl)
}