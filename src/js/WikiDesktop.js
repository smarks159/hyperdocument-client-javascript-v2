"use strict";
var Desktop={};
Desktop.DocumentRange=class DocumentRange {    
    constructor(){
        this.start=null;
        this.end=null;
    }
    
    getRangeByOrderSelected(){
        return [this.start,this.end]
    }
    
    getRangeByDocumentOrder(){
        let startTreeNode=new TreeNodeInterface(this.start);
        let endTreeNode=new TreeNodeInterface(this.end);
        
        if(startTreeNode.parent!==endTreeNode.parent){
            throw Error("This function only works if the start and end nodes have the same parents.");
        }
        
        if(startTreeNode.getNodeIndex()<=endTreeNode.getNodeIndex()){
            return [this.start,this.end];
        }
        else{
            return [this.end,this.start];
        }
    }
    
    setRange(startNode,endNode){
        this.start=startNode;
        this.end=endNode;
    }
    
    clearRange(){
        this.start=null;
        this.end=null;
    }
    
    isEmpty(){
        let propGraph=wikiDesktop.wikiData.propertyGraph;
        if(this.start===null || this.end===null){
            return true;
        }
        // Undo/redo can remove the node from the property graph without updating the selectRange state. So, we must check if the nodes in the range still exist in the property graph. This is a problem because the range stores a copy of the node and not a reference.
        else if(propGraph.getNodeById(this.start.objectId)===null || propGraph.getNodeById(this.end.objectId)===null){
            return true;
        }
        else{
            return false;
        }
    }
}

class DocumentViewModel{
    constructor(){
        this.showStructuralPath=false;
        this.showNodeId=false;
        this.showRelativePath=false;
        this.nLevelsDown=1;
    }
}

Desktop.WikiDesktop=class WikiDesktop{
    constructor(){
        this.wikiData=null;
        this.linksDBInterface=null;
        this.tagsDBInterface=null;
        this.viewModels=null;
        this.topWindowPanel=null; 
        this.inputForm=null;
        this.contextMenu=null;
        this.confirmationDialog=null;
        this.messageDisplay=null;
        this.currentWikiFile=null;
        this.popupPanel=null;
        this.recentlyOpenedWikiFiles=[];
        
        this.selectedNode=null;
        this.selectedRange=new Desktop.DocumentRange();
        
        this.clipboardObjects=[];
        // If copy clip board object is false then we move the object instead of copying it.
        this.copyClipboardObject=false;
        this.selectedWindow=null;

        this.jumpHistory={
            "left":new NavigationHistory(),
            "right":new NavigationHistory()
        }

        this.documentViewModels={
            "left":new DocumentViewModel(),
            "right":new DocumentViewModel()
        }

        this.commandInterpreter=null;
        /**
         * When the command interpreter is active the following things happen:
         *    - All of the TextPresenters become read only.
         *    - The WikiActions button is hidden
         *    - A Command Interpreter Presenter is shown on the top of the window
         *    - The message bar is moved below the command interpreter.
         *    - The right click context menus are disabled.
         */
        this.commandInterpreterActive=false;
        
    }

    /**
     * createInterfaces must be called everytime a new wiki file is created or a wiki file is loaded. It creates the interfaces through which the database file is manipulated. Any new database interface should be added here to ensure the data and the interfaces stay in sync. This is only for interfaces that update the database, not for interfaces that manipulate the state of the application, like the view model.
     * @param {PropertyGraphModel.PropertyGraph || Object} wikiData - The wiki property graph as a property graph object or in serialized JSON format.
     */
    createDBInterfaces(wikiData){
        let wikiPropGraph;
        if(wikiData instanceof PropertyGraphModel.PropertyGraph){
            wikiPropGraph=wikiData;
        }
        else{
            wikiPropGraph=PropertyGraphModel.PropertyGraph.loadFromData(wikiData);
        }
        this.wikiData=new WikiDataInterface(wikiPropGraph);
        this.linksDBInterface=new LinkInterface(wikiPropGraph);
        this.tagsDBInterface=new TagsInterface(wikiPropGraph);
    }

    /**
     * Check to see if the default search node exists in the database and create it if it does not. The default search node is used for the global search. This method should be called after createDBInterface is called.
     */
    initializeDefaultSearchNode(){
        let results=this.tagsDBInterface.queryTagList("searchNode","default")
        if(results.length!=0){
            return;
        }
        let searchNodeTag=this.tagsDBInterface.getTagByName("searchNode");
        if(searchNodeTag===null){
            this.tagsDBInterface.createTag("searchNode");
            searchNodeTag=this.tagsDBInterface.getTagByName("searchNode");
        }
        let defaultTag=this.tagsDBInterface.getTagByName("default");
        if(defaultTag===null){
            this.tagsDBInterface.createTag("default");
            defaultTag=this.tagsDBInterface.getTagByName("default");
        }
        let searchNode=new DataModels.SearchDataModel();
        this.wikiData.propertyGraph.addNode(searchNode);
        this.tagsDBInterface.addTagToNode("searchNode",searchNode);
        this.tagsDBInterface.addTagToNode("default",searchNode);
    }
    
    getPresentersInRange(){
        if(this.selectedRange.isEmpty()){
            return [];
        }
        let [startDataNode,endDataNode]=this.selectedRange.getRangeByDocumentOrder();
        // Handle the cases where the start or end node in the range has been deleted.
        if(this.wikiData.propertyGraph.getNodeById(startDataNode.objectId)===null){
            return [];
        }
        if(this.wikiData.propertyGraph.getNodeById(endDataNode.objectId)===null){
            return [];
        }
        let startTreeNode=new TreeNodeInterface(startDataNode);
        let endTreeNode=new TreeNodeInterface(endDataNode);  
        let presenters=[];
        let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
        let docPresenter=selectedWindow.nodePresenter;
        let children=new TreeNodeInterface(startTreeNode.parent).children;
        for (const [index,node] of children.entries()) {
            if(index>=startTreeNode.getNodeIndex() && index<=endTreeNode.getNodeIndex()){
                let presenter=docPresenter.findFirstChildByObjectId(node.objectId);
                if(presenter!==null){
                    presenters.push(presenter);
                }
            }
        }
        return presenters;
    }
    
    setCurrentWikiFile(fileName){
        this.currentWikiFile=fileName;
        
        // do not add empty files to the history
        if(fileName===""){
            return;
        }
        let index=this.recentlyOpenedWikiFiles.indexOf(fileName);
        // move fileName to top of list if it is already in the list
        if(index!==-1){
            this.recentlyOpenedWikiFiles.splice(index,1)
            this.recentlyOpenedWikiFiles.unshift(fileName);
        }
        else{
            this.clearDeletedFilesFromHistory();
            // only keep the 10 most recently used files
            if(this.recentlyOpenedWikiFiles.length==10){
                this.recentlyOpenedWikiFiles.pop()
            }
            this.recentlyOpenedWikiFiles.unshift(fileName);
        }
        ipcRenderer.sendSync("setRecentlyOpenedWikiFiles",this.recentlyOpenedWikiFiles)
    }
    
    // clear out files that have been deleted or moved from history
    clearDeletedFilesFromHistory(){
        this.recentlyOpenedWikiFiles=this.recentlyOpenedWikiFiles.filter(filePath=>ipcRenderer.sendSync("fileExists",filePath));
        ipcRenderer.sendSync("setRecentlyOpenedWikiFiles",this.recentlyOpenedWikiFiles)
    }

    clearClipboard(){
        this.clipboardObjects=[];
    }
    
    resetGUIState(){
        this.currentWikiFile=null;
        this.selectedNode=null;
        this.selectedRange.clearRange();
        this.clipboardObjects=[];
        this.copyClipboardObject=false;
    }    

    getDocumentPresenter(toFind){
        if(toFind instanceof Presenters.Presenter){
            return toFind.findFirstParentByType(Presenters.StructuredDocumentPresenter);
        }
        else if(toFind instanceof DataModels.FileObject){
            let documentNode=toFind.fileContent;
            let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
            return selectedWindow.findFirstChildByObjectId(documentNode.objectId).nodePresenter;
        }
        else if(toFind instanceof PropertyGraphModel.Node){
            let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
            let presenter=selectedWindow.findFirstChildByObjectId(toFind.objectId);
            if(presenter instanceof Presenters.StructuredDocumentPresenter){
                return presenter;
            }
            else{
                return presenter.findFirstParentByType(Presenters.StructuredDocumentPresenter);
            }
        }
    }

    openFormInPopupPanel(inputForm,processInputCallback,offset,width=null){
        inputForm.onSubmitClick((event)=>{
            let inputData=inputForm.getData();
            // If there are previous errors and the user corrects them, then the error messages must be cleared from the form first before rechecking the errors.
            inputForm.clearErrorMsgs();
            processInputCallback(inputData);
        })
        inputForm.onCancelClick(()=>this.popupPanel.closePopup());
        this.popupPanel.setContent(inputForm);
        this.popupPanel.openAsPopup(offset,width);
        inputForm.focus();
    }
}