let TypeToClassMaps={};

TypeToClassMaps.getNodeClassFromType=function(typeName){
    if(TypeToClassMaps.nodeTypeToClassMap[typeName]===undefined){
        throw new Error("The node type "+typeName+" is not supported");
    }
    return TypeToClassMaps.nodeTypeToClassMap[typeName];
};

TypeToClassMaps.getEdgeClassFromType=function(typeName){
    if(TypeToClassMaps.edgeTypeToClassMap[typeName]===undefined){
        throw new Error("The edge type "+typeName+" is not supported");
    }
    return TypeToClassMaps.edgeTypeToClassMap[typeName];
};

TypeToClassMaps.nodeTypeToClassMap={
    "Node":PropertyGraphModel.Node,
    "TextDataModel":DataModels.TextDataModel,
    "RootFileNode":DataModels.RootFileNode,
    "FileObject":DataModels.FileObject,
    "StructuredDocumentDataModel":DataModels.StructuredDocumentDataModel,
    "ProseMirrorDataModel":DataModels.ProseMirrorDataModel,
    "sequence":ConfigAST.SequenceNode,
    "KeyboardChoice":ConfigAST.KeyboardChoice,
    "Choice":ConfigAST.Choice,
    "if":ConfigAST.If,
    "Equals":ConfigAST.Equals,
    "AND":ConfigAST.AND,
    "OR":ConfigAST.OR,
    "NOT":ConfigAST.NOT,
    "SetVar":ConfigAST.SetVar,
    "ExecuteAction":ConfigAST.ExecuteAction,
    "SelectObject":ConfigAST.selectObject,
    "ResetInterpreter":ConfigAST.ResetInterpreter,
    "Tag": PropertyGraphModel.TagNode,
    "ViewModel": DataModels.ViewModel,
    "CommandConfig":ConfigAST.CommandConfig,
    "SearchDataModel":DataModels.SearchDataModel
};

TypeToClassMaps.edgeTypeToClassMap={
    "Edge":PropertyGraphModel.Edge,
    "TreeChildEdge":PropertyGraphModel.TreeChildEdge,
    "FileContentEdge":DataModels.FileContentEdge,
    "LinkEdge": DataModels.LinkEdge
};


