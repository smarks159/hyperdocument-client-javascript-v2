const {Schema,DOMSerializer,DOMParser}= nodeRequire("prosemirror-model");

DataModels.ProseMirrorDataModel=class ProseMirrorDataModel extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.doc=null;
        this.type="ProseMirrorDataModel"
        this.schema=DataModels.ProseMirrorDataModel.Schema
    }
    
    getTextContent(){
        if(this.doc===null){
            return "";
        }
        else{
            return this.doc.textContent;
        }
    }

    /**
     * Given a text string, find the location of all the places in the paragraph where the text string is located.
     * 
     * The positions correspond to prosemirror position, which are needed to highlight the position of the search results in the paragraph. Normally, prosemirror positions do not map one-to-one with text indexes. Currently, the prosemirror document only contains a single paragraph, so it is possible to map the text string to prosemirror manually. If content becomes more complicated in the future and this is no longer possible, then prosemirror has a descendants method which can be used to perform the position calculations.
     * 
     * @param {string} textToSearch 
     * @returns - list of [start,end] prosemirror positions where the match starts and ends. 
     */
    searchText(textToSearch){
        let results=[]
        let regex=new RegExp(textToSearch,'g');
        let matches=this.getTextContent().matchAll(regex);
        // Add 1 to the index because the 1st position in prosemirror is occupied by the opening paragraph tag.
        for (const match of matches) {
            results.push([match.index+1,match.index+1+match[0].length])
        }
        return results;
    }
    
    savePropertiesToData(){
        let data={};
        if(this.doc){
            data.doc=this.doc.toJSON();
        }
        else{
            data.doc=null;
        }
        return data;        
    }
    
    loadPropertiesFromData(data){
        if(data.doc){
            this.doc=ProseMirrorNode.fromJSON(this.schema,data.doc);
        }
        else{
            this.doc=null;
        } 
    }
    
}

// Define the schema once to improve LoadFromData performance.
DataModels.ProseMirrorDataModel.Schema=new Schema({
    nodes:{                
        doc:{content:"paragraph+"},
        paragraph:{
            content:"text*",
            toDOM: (node)=>{return ["p",0]}
        },
        text:{}
    },
    marks:{
        strong:{
            toDOM(){return ["strong"]},
            parseDOM:[{tag:"strong"}]
        },
        link: {
            attrs: {href:{}},
            // Add the url to the title attribute so that the url is displayed when you hover over the link, like a normal url. This behavior is not the default on contenteditable links.
            toDOM(node) { 
                return ["a",
                    {href: node.attrs.href, 
                    title:node.attrs.href},
                    0
                ]},
            parseDOM:[{tag:"a", getAttrs(dom) { return {href: dom.href}}}],
            inclusive: false
        },
        internalLink:{
            attrs: {objectId:{},edgeId:{}},
            toDOM(node){
                return ["span",{class:"internalLink",objectId:node.attrs.objectId,edgeId:node.attrs.edgeId},0]
            },
            parseDOM:[{tag:"span.internalLink", getAttrs(dom) {return {objectId:parseInt(dom.objectId),edgeId:parseInt(dom.edgeId)}}}],
            inclusive:false
        },
        highlightText: {
            toDOM(node) {return ["span",{class:"highlighted"}]}
        }
    }
});

DataModels.ProseMirrorDataModel.convertContentToHTMLString=function(contentFragment){
    let serializer=DOMSerializer.fromSchema(DataModels.ProseMirrorDataModel.Schema);
    let newDOM=document.createElement("div");
    newDOM.appendChild(serializer.serializeFragment(contentFragment));
    return newDOM.innerHTML;
}

/**
 * Given a prosemirror document convert the contents to an HTML node
 */
 DataModels.ProseMirrorDataModel.convertProseMirrorDocToHTMLNode=function(doc){
    let serializer=DOMSerializer.fromSchema(DataModels.ProseMirrorDataModel.Schema);
    // serializeFragment returns a dom fragment, so to return an HTML node get the firstChild
    return serializer.serializeFragment(doc.content).firstChild;
}

/**
 * When pasting an htmlString within an existing paragraph, as opposed to creating a new paragraph, then it must be converted to a prosemirror slice object inorder to paste it properly.
 */
DataModels.ProseMirrorDataModel.convertHTMLStringToProseMirrorSlice=function(htmlString){
    let parser=DOMParser.fromSchema(DataModels.ProseMirrorDataModel.Schema);
    let div=document.createElement("div");
    div.innerHTML=htmlString.trim();
    return parser.parseSlice(div);
}

/**
 * Parse an HTMLString into a prosemirror node. This is used when creating a new prosemirror data model and setting the whole content of data node equal to the contents of the htmlString
 */
DataModels.ProseMirrorDataModel.convertHTMLStringToProseMirrorNode=function(htmlString){
    let parser=DOMParser.fromSchema(DataModels.ProseMirrorDataModel.Schema);
    let div=document.createElement("div");
    div.innerHTML=htmlString.trim();
    return parser.parse(div);
}

