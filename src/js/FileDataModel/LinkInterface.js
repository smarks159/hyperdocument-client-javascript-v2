"use strict";

DataModels.LinkEdge= class LinkEdge extends PropertyGraphModel.Edge{
    constructor(sourceNode,targetNode){
        super(sourceNode,targetNode);
        this.type="LinkEdge"
    }
}

class LinkInterface{
    constructor(propertyGraph){
        this.propertyGraph=propertyGraph;
    }

    addLink(sourceNode,targetNode){
        if(!(sourceNode instanceof PropertyGraphModel.Node)){
            throw new Error("sourceNode must be a property graph node");
        }
        if(!(targetNode instanceof PropertyGraphModel.Node)){
            throw new Error("targetNode must be a property graph node");
        }
        let link=new DataModels.LinkEdge(sourceNode,targetNode);
        this.propertyGraph.addEdge(link);
        return link;
    }

    deleteLink(linkEdge){
        if(!(linkEdge instanceof DataModels.LinkEdge)){
            throw new Error("linkEdge must be an instance of class LinkEdge")
        }
        this.propertyGraph.deleteEdge(linkEdge);
    }
}