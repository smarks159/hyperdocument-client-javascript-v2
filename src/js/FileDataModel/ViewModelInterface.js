/**
 * This is a very simple view model. A view model is a graph node that can have any number of simple attribute values. This is implemented as a simple key-value pair.
 */
DataModels.ViewModel=class ViewModel extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.attributes={};
        this.type="ViewModel";
    }

    getAttribute(name){
        if(this.hasAttribute(name)){
            return this.attributes[name];
        }
        else{
            throw new Error(`The attribute '${name}' does not exist in the view model`);
        }
        
    }

    hasAttribute(name){
        if(this.attributes.hasOwnProperty(name)){
            return true;
        }
        else{
            return false;
        }
    }

    setAttribute(name,value){
        this.attributes[name]=value;
        return this;
    }

    savePropertiesToData(){
        return {"attributes":this.attributes};
    }

    loadPropertiesFromData(data){
        this.attributes=data.attributes;
    }
}


/**
 * A ViewModelInferace is a simple interface for creating and retrieving view models. It uses the tags interface to implement this. ViewModels all have the ViewModel tag, which acts as a namespace to avoid name conflicts with other types of nodes, such as config files or wiki files.
 */
class ViewModelInterface{
    /**
     * The ViewModelInterface constructor will create a ViewModel tag if it is not already defined in the database.
     * @param {PropertyGraphModel.PropertyGraph} propGraph 
     */
    constructor(propGraph){
        this.propGraph=propGraph;
        this.tagsInterface=new TagsInterface(propGraph);
        if(this.tagsInterface.getTagByName("ViewModel")===null){
            this.tagsInterface.createTag("ViewModel");
        }
    }

    /**
     * This method will create a new view model if one does not exist or will get the existing view model from the database. It will also create a new tag that is the same name of the ViewModel if it does not exist already.
     * @param {string} name - The name of the view model to get.
     */
    getViewModel(name){
        let results=this.tagsInterface.queryTagList(name,"ViewModel");
        // The view model does not exist, so create a new one.
        if(results.length==0){
            let viewModel=new DataModels.ViewModel();
            this.propGraph.addNode(viewModel);
            if(this.tagsInterface.getTagByName(name)===null){
                this.tagsInterface.createTag(name);
            }
            this.tagsInterface.addTagToNode(name,viewModel);
            this.tagsInterface.addTagToNode("ViewModel",viewModel);
            return viewModel;
        }
        else{
            return results[0];
        }
    }
}