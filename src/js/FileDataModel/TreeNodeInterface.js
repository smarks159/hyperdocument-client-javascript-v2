"use strict";

/*
 * This class is a wrapper around a PropertyGraph node that allows the programmer to manipulate the node like
 * a tree, instead of manipulating the raw property graph.
 */
 
class TreeNodeInterface{
    constructor(node){
        this.node=node;
    }
    
    get children(){
        var childEdges=[];            
        this.node.outgoingEdges.forEach(function(edge){
            if(edge instanceof PropertyGraphModel.TreeChildEdge){
                childEdges.push(edge);
            }
        });
        childEdges.sort(function(a,b){
            if(a.index<b.index){
                return -1;
            }
            else if(a.index>b.index){
                return 1;
            }
            else{
                return 0;
            }
        });
        var childNodes=[];
        childEdges.forEach(function(edge){childNodes.push(edge.targetNode);});
        return childNodes;
    }
    
    get parent(){
        var parent=null;
        this.node.incomingEdges.forEach(function(edge){
            if(edge instanceof PropertyGraphModel.TreeChildEdge){
                parent=edge.sourceNode;
                return;
            }
        });
        return parent;
    }
    
    getNext(){
        if(this.parent===null){
            return null;
        }
        let index=this.getNodeIndex();
        if(index===new TreeNodeInterface(this.parent).children.length-1){
            return null;
        }
        return new TreeNodeInterface(this.parent).children[index+1];
    }
    
    getPrevious(){
        if(this.parent===null){
            return null;
        }    
        let index=this.getNodeIndex();
        if(index===0){
            return null;
        }
        return new TreeNodeInterface(this.parent).children[index-1];
    }
    
    getLastChildIndex(){
        return this.children.length-1;  
    }
    
    appendChild(newNode){
        var graph=this.node.propertyGraph;   
        // when calling moveBranch within the same property graph the node will already exist in the graph
        if(graph.nodes.indexOf(newNode)===-1){        
            graph.addNode(newNode);
        }
        var childEdge=new PropertyGraphModel.TreeChildEdge(this.node,newNode);
        childEdge.index=this.getLastChildIndex()+1;
        graph.addEdge(childEdge);
    }

    appendChildFirst(newNode){
        if(!this.hasChildren()){
            this.appendChild(newNode);
        }
        else{
            let firstChild=new TreeNodeInterface(this.children[0]);
            firstChild.appendBefore(newNode);
        }
    }
    
    appendNext(nextNode){
        var treeNode=new TreeNodeInterface(this.node);
        if(treeNode.parent===null){
            throw Error("Cannot call appendNext on a node with no parent");
        }
        var graph=this.node.propertyGraph;
        if(graph.nodes.indexOf(nextNode)===-1){        
            graph.addNode(nextNode);
        }
        var currentIndex=treeNode.getNodeIndex();
        treeNode.parent.outgoingEdges.forEach(function(edge){
            if(edge instanceof PropertyGraphModel.TreeChildEdge){
                if(edge.index>currentIndex){
                    edge.index+=1;
                }
            };
        });
        var newEdge= new PropertyGraphModel.TreeChildEdge(treeNode.parent,nextNode);
        newEdge.index=currentIndex+1;
        graph.addEdge(newEdge);
    }
    
    appendBefore(nextNode){
        var treeNode=new TreeNodeInterface(this.node);
        if(treeNode.parent===null){
            throw Error("Cannot call appendNext on a node with no parent");
        }
        var graph=this.node.propertyGraph;
        if(graph.nodes.indexOf(nextNode)===-1){        
            graph.addNode(nextNode);
        }
        var currentIndex=treeNode.getNodeIndex();
        treeNode.parent.outgoingEdges.forEach(function(edge){
            if(edge instanceof PropertyGraphModel.TreeChildEdge){
                if(edge.index>=currentIndex){
                    edge.index+=1;
                }
            };
        });
        var newEdge= new PropertyGraphModel.TreeChildEdge(treeNode.parent,nextNode);
        newEdge.index=currentIndex;
        graph.addEdge(newEdge);
    }
    
    delete(){
        var treeNode=new TreeNodeInterface(this.node);
        if(treeNode.parent===null){
            throw Error("Cannot call delete on a node with no parent");
        }
        treeNode.children.forEach(function(child){
            new TreeNodeInterface(child).delete(); 
        });
        var currentIndex=treeNode.getNodeIndex();
        var graph=this.node.propertyGraph;
        treeNode.parent.outgoingEdges.forEach(function(edge){
            if(edge instanceof PropertyGraphModel.TreeChildEdge){
                if(edge.index>currentIndex){
                    edge.index-=1;
                }
            }
        });
        graph.deleteNode(this.node);
    }
    
    deleteChild(childNode){
        if(this.children.indexOf(childNode)===-1){
            throw new Error("The Child Node cannot be found");
        }
        new TreeNodeInterface(childNode).delete();
    }
    
    replaceChild(newChild,oldChild){
       var treeNode=new TreeNodeInterface(this.node);
       var children=treeNode.children;
       var index=children.indexOf(oldChild);
       if(index===-1){
           throw Error("Old Child not found");
       }
       var graph=this.node.propertyGraph;   
       graph.deleteNode(oldChild);
       if(graph.nodes.indexOf(newChild)===-1){        
            graph.addNode(newChild);
       }
       var edge=new PropertyGraphModel.TreeChildEdge(this.node,newChild);
       edge.index=index;   
       graph.addEdge(edge);  
    }
    
    getNodeIndex(){
        var index=null;
        this.node.incomingEdges.forEach(function(edge){
            if(edge instanceof PropertyGraphModel.TreeChildEdge){
                index=edge.index;
                return;
            }
        });
        if(index===null){
            throw Error("cannot get index of a node with no parents");
        } else{
            return index;
        }
    }
    
    hasChildren(){
        return this.children.length>0;
    }

    findParentByType(type){       
        let currentNode=this.parent;
        while(currentNode!==null){
            if(currentNode instanceof type){
                return currentNode;
            }
            currentNode=new TreeNodeInterface(currentNode).parent;
        }
        return null;
    }
    
    static insertTreeNode(targetNode,nodeToInsert,position){
        let targetTreeNode=new TreeNodeInterface(targetNode);
        if(position==="child" || position==="childLast"){
            targetTreeNode.appendChild(nodeToInsert);
        }
        else if(position==="childFirst"){
            targetTreeNode.appendChildFirst(nodeToInsert);
        }    
        else if(position==="next"){
            targetTreeNode.appendNext(nodeToInsert);
        }
        else if(position==="before"){
            targetTreeNode.appendBefore(nodeToInsert);
        }
        else if(position==="here"){
            let parentNode=new TreeNodeInterface(targetTreeNode.parent)
            parentNode.replaceChild(nodeToInsert,targetNode);
        }
        else {
            throw new Error("The position "+position+" is not supported");
        } 
    }
    
    static copyBranch(sourceNode,destinationNode,position){
        // Copy the branch to a temp graph to avoid infinite recursion when copying a node to a position that is underneath the source in the tree.
        let tempNode=TreeNodeInterface.copyNode(sourceNode);
        let tempGraph=new PropertyGraphModel.PropertyGraph();
        tempGraph.addNode(tempNode);
        TreeNodeInterface.copyBranchInner(new TreeNodeInterface(sourceNode).children,tempNode);

        let newNode=TreeNodeInterface.copyNode(sourceNode);
        TreeNodeInterface.insertTreeNode(destinationNode,newNode,position);
        TreeNodeInterface.copyBranchInner(new TreeNodeInterface(tempNode).children,newNode);
        return newNode;
    }
    
    static copyBranchInner(children,parentNode){
        children.forEach(function(child){
            var childNodeCopy=TreeNodeInterface.copyNode(child);
            new TreeNodeInterface(parentNode).appendChild(childNodeCopy);        
            TreeNodeInterface.copyBranchInner(new TreeNodeInterface(child).children,childNodeCopy);
        });
    }
    
    static copyNode(node){
        let newNodeClass=TypeToClassMaps.getNodeClassFromType(node.type);
        let newNode=new newNodeClass();
        let nodeData=node.savePropertiesToData();
        newNode.loadPropertiesFromData(nodeData);
        if(newNode.loadReferencesToOtherNodes!==undefined){
            newNode.loadReferencesToOtherNodes(nodeData.properties);
        }
        return newNode;
    }
    
    static moveBranch(sourceNode,destinationNode,position){
        if(sourceNode.propertyGraph!==destinationNode.propertyGraph){
            let newNode=TreeNodeInterface.copyBranch(sourceNode,destinationNode,position);
            new TreeNodeInterface(sourceNode).delete();
            return newNode;
        }
        else{        
            // move the node without removing it from the graph.        
            let treeSourceNode=new TreeNodeInterface(sourceNode);
            let parent=treeSourceNode.parent;
            let sourceIndex=treeSourceNode.getNodeIndex();
            // delete the edge between the source node and its parent
            for(let edge of sourceNode.incomingEdges){
                if(edge instanceof PropertyGraphModel.TreeChildEdge){
                    sourceNode.propertyGraph.deleteEdge(edge);                
                }
            }
            
            // update the edge indexes of nodes that used to be siblings of the source
            for(let edge of parent.outgoingEdges){
                if(edge instanceof PropertyGraphModel.TreeChildEdge && edge.index>sourceIndex){                
                    edge.index-=1;
                }
            }        
            
            TreeNodeInterface.insertTreeNode(destinationNode,sourceNode,position);
            return sourceNode;
        }
    }
}