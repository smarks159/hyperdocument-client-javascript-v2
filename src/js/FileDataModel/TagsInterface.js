/**
 * Tags are nodes in the database which contain a set of objectIds associated with this tag. Conceptually a tag can be thought of as a set of objects with a name. ObjectIds are stored as sets instead of edges because it is easier and more efficient to work with sets directly than a list of edges.
 * 
 * The tag node defines set methods because the javascript Set api is incomplete. The class will use the built-in methods when it is defined but it is necessary to define many of the set operations yourself. For consistency purposes, this class will define methods for all set operations whether it is part of the built-in api or not.
 */
PropertyGraphModel.TagNode=class TagNode extends PropertyGraphModel.Node {
    constructor(){
        super();
        this.name=null;
        this.nodesWithTag=new Set()
        this.type="Tag"
    }

    savePropertiesToData(){
        return {"name":this.name,"nodesWithTag":Array.from(this.nodesWithTag)};
    }

    loadPropertiesFromData(data){
        this.name=data["name"];
        this.nodesWithTag=new Set(data["nodesWithTag"]);
    }

    /**
     * Add an objectId to the tag set
     * @param {integer} objectId 
     */
    add(objectId){
        this.nodesWithTag.add(objectId);
    }

    /**
     * Check to see if an objectId exists in the tag set
     * @param {integer} objectId 
     * @returns {boolean}
     */
    has(objectId){
        return this.nodesWithTag.has(objectId);
    }

    /**
     * Get the size of the tag set.
     * @returns {integer}
     */
    numObjects(){
        return this.nodesWithTag.size;
    }

    /**
     * Remove an objectId from the tag set
     * @param {integer} objectId 
     */
    delete(objectId){
        this.nodesWithTag.delete(objectId);
    }

    getObjectSet(){
        return this.nodesWithTag;
    }

    /**
     * Return the intersection between the tag's objectIds and the set passed in.
     * @param {Set} setA - set to intersect with
     */
    intersection(setA){
        let results = new Set()
        for (let elem of setA) {
            if (this.nodesWithTag.has(elem)) {
                results.add(elem)
            }
        }
        return results
    }
}

/**
 * This class defines an interface for manipulating and querying tags in the property graph database. The tag operations are defined as a separate interface, so the underlying datastructure or database can be changed in the future.
 */
class TagsInterface {
    /**
     * @param {PropertyGraphModel.PropertyGraph} propGraph 
     */
    constructor(propGraph){
        this.propGraph=propGraph;
    }

    /**
     * Create a new tag node in the database and add it to the tag index.
     * This method raises an error if you attempt to create that already exists in order to prevent overwritting the existing tag associations.
     * @param {string} tagName - The name of the tag to create.
     */
    createTag(tagName){
        if(this.propGraph.tagIndex.hasOwnProperty(tagName)){
            throw new Error(`The tag "${tagName}" already exists in the database. Cannot add the same tag twice`)
        }
        let newTag=new PropertyGraphModel.TagNode();
        newTag.name=tagName;
        this.propGraph.addNode(newTag);
        this.propGraph.tagIndex[tagName]=newTag;
    }

    /**
     * Get a tag object by tagName. If a tag does not exist then return null. Returning null when a node does not exist is consistent with the getNodeById and getEdgeById apis.
     * @param {string} tagName - The name of the tag to retrieve
     * @returns {PropertyGraphModel.TagNode|null}
     */
    getTagByName(tagName){
        if(this.propGraph.tagIndex.hasOwnProperty(tagName)){
            return this.propGraph.tagIndex[tagName];
        } else{
            return null;
        }
    }

    /**
     * Add a tag to a node. In order to add a tag to a node, the tag must have been created already and the node must already be in the database. Adding a tag twice to the same node twice causes an error.
     * @param {string} tagName - The name of the tag to add
     * @param {PropertyGraphModel.Node | integer} node - Node to add the tag to. Can be either a node object or an objectId
     */
    addTagToNode(tagName,node){
        let tag=this.getTagByName(tagName);
        if(tag===null){
            throw new Error(`The tag '${tagName}' does not exist in the database. A tag created before adding it to the database.`)
        }
        let objectId;
        if(node instanceof PropertyGraphModel.Node){
            if(node.objectId===null){
                throw new Error("A node must be added to the database before adding a tag");
            }
            objectId=node.objectId;
        }
        else{
            objectId=node;
        }
        node=this.propGraph.getNodeById(objectId);
        if(node===null){
            throw new Error(`The objectId ${objectId} does not exist in the database`);
        }
        if(tag.has(objectId)){
            throw new Error(`The tag '${tagName}' already contains the objectId ${objectId}`);
        }
        tag.add(objectId);
    }

    /**
     * Remove a tag from a node. In order to remove a tag from a node, the tag must have been created already and the node must already be in the database. Attempting to remove a tag from a node that does not have that tag will cause an error.
     * @param {string} tagName - The name of the tag to remove
     * @param {PropertyGraphModel.Node | integer} node - Node to remove the tag from. Can be either a node object or an objectId
     */
    removeTagFromNode(tagName,node){
        let tag=this.getTagByName(tagName);
        if(tag===null){
            throw new Error(`The tag '${tagName}' does not exist in the database. A tag created before adding it to the database.`)
        }
        let objectId;
        if(node instanceof PropertyGraphModel.Node){
            if(node.objectId===null){
                throw new Error("A node must be added to the database before adding a tag");
            }
            objectId=node.objectId;
        }
        else{
            objectId=node;
        }
        node=this.propGraph.getNodeById(objectId);
        if(node===null){
            throw new Error(`The objectId ${objectId} does not exist in the database`);
        }
        if(!tag.has(objectId)){
            throw new Error(`The object ${objectId} does not have the tag '${tagName}'`);
        }
        tag.delete(objectId);
    }

    /**
     * Delete a tag. Remove the tag node from the database and the tag index. Deleting a tag works whether or not a tag still is still associated with other nodes.
     * @param {string} tagName - The name of the tag to delete
     */
    deleteTag(tagName){
        let tag=this.getTagByName(tagName);
        if(tag===null){
            throw new Error(`The tag '${tagName}' does not exist in the database.`);
        }
        this.propGraph.deleteNode(tag);
        delete this.propGraph.tagIndex[tagName];
    }

    /**
     * Rename a tag. If you attempt to rename a tag to a tag that already exists in the database an error will be raised.
     * @param {string} oldTagName - The current name of the tag
     * @param {string} newTagName - The new name of the tag
     */
    renameTag(oldTagName,newTagName){
        let oldTag=this.getTagByName(oldTagName);
        if(oldTag===null){
            throw new Error(`The tag '${oldTagName}' does not exist in the database.`);
        }

        let newTag=this.getTagByName(newTagName);
        if(newTag!==null){
            throw new Error(`The tag '${newTagName}' already exists in the database.`);
        }

        oldTag.name=newTagName;
        delete this.propGraph.tagIndex[oldTagName];
        this.propGraph.tagIndex[newTagName]=oldTag;
    }

    /**
     * Query a list of tags and return the nodes that match the intersection of the tag list. If one of the tags in the query does not exist in the database then an empty result list is returned.
     * 
     * @param {...string} - The tags to query passed in as a parameter list.
     */
    queryTagList(...tags){
        let results=null;
        for (const tag of tags) {
            let tagNode=this.getTagByName(tag);
            if(tagNode===null){;
                return [];
            }
            if(results===null){
                results=tagNode.getObjectSet();
            }
            else{
                results=tagNode.intersection(results);
            }
        }
        let resultNodes=[];
        for(const objectId of results){
            resultNodes.push(this.propGraph.getNodeById(objectId))
        }
        return resultNodes;
    }
}