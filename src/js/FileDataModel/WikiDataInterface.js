"use strict";

/*
    The wikiData file format is as follows:
        - There is a single node at the top of the Graph called RootFileNode, which is where all the files and folders are defined.        
*/

DataModels.FileContentEdge= class FileContentEdge extends PropertyGraphModel.Edge{
    constructor(){
        super();
        this.type="FileContentEdge"
    }
}

DataModels.RootFileNode= class RootFileNode extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.type="RootFileNode";
    }
    
}

DataModels.FileObject= class FileObject extends PropertyGraphModel.Node{
    constructor(fileName){
        super();
        this.type="FileObject";
        this.fileName=fileName
        this._fileContentsEdge=null;
    }
    set fileContent(contentNode){        
        if(this._fileContentsEdge===null){
            this._fileContentsEdge=new DataModels.FileContentEdge();
            this._fileContentsEdge.sourceNode=this;            
            this._fileContentsEdge.targetNode=contentNode;
            this.propertyGraph.addNode(contentNode);
            this.propertyGraph.addEdge(this._fileContentsEdge);
        }
        else{
            let oldContentNode=this._fileContentsEdge.targetNode;
            this.propertyGraph.removeEdge(this._fileContentsEdge);
            this.propertyGraph.deleteNode(oldContentNode);
            
            this._fileContentsEdge=new DataModels.FileContentEdge();
            this._fileContentsEdge.sourceNode=this;            
            this._fileContentsEdge.targetNode=contentNode;
            this.propertyGraph.addEdge(this._fileContentsEdge);
        }
    }
    get fileContent(){
        return this._fileContentsEdge.targetNode;
    }
    
    savePropertiesToData(){
        let data={"type":"FileObject","fileName":this.fileName}
        if(this._fileContentsEdge!==null){
            data["fileContentsEdge"]=this._fileContentsEdge.edgeId;
        }
        else{
            data["fileContentsEdge"]=null;
        }
        return data;
    }
    
    loadPropertiesFromData(data){
        this.fileName=data.fileName;    
    }
    
    loadReferencesToOtherNodes(data){
        if(data.fileContentsEdge!==null){
            this._fileContentsEdge=this.propertyGraph.getEdgeById(data.fileContentsEdge);
        }
    }
}

var WikiDataInterface=class WikiDataInterface {
    constructor(propertyGraphData){
        if(propertyGraphData instanceof PropertyGraphModel.PropertyGraph){
            this.propertyGraph=propertyGraphData;
        }
        else{
            this.propertyGraph=PropertyGraphModel.PropertyGraph.loadFromData(propertyGraphData);
        }
        let RootFileNode=this.propertyGraph.getFirstNodeByType(DataModels.RootFileNode);
        if (RootFileNode===null){
            RootFileNode=new DataModels.RootFileNode();
            this.propertyGraph.addNode(RootFileNode);
        }
        this.RootFileNode=new TreeNodeInterface(RootFileNode);
    }
    
    addFile(fileName){
        let newFile=new DataModels.FileObject(fileName)
        this.propertyGraph.addNode(newFile);
        newFile.fileContent=new DataModels.StructuredDocumentDataModel();
        this.RootFileNode.appendChild(newFile);        
        return newFile;
    }
    
    removeFile(fileName){
        let fileObject=this.getFileObjectByName(fileName);
        for(let child of new TreeNodeInterface(fileObject.fileContent).children){
            new TreeNodeInterface(child).delete();
        }        
        this.propertyGraph.deleteNode(fileObject.fileContent);        
        this.RootFileNode.deleteChild(fileObject);                
    }
    
    renameFile(fileObject,newName){
        fileObject.fileName=newName;
    }
    
    getFileObjectByName(fileName){
        for(let fileObj of this.RootFileNode.children){
            if(fileObj.fileName===fileName){
                return fileObj;
            }
        }
        return null;
    }
}