function isPrimitive(test) {
    if (test===null){
        return true;
    }
    let type=typeof test;
    if(type==="object" || type==="function"){
        return false;
    } else{
        return true;
    }
}

function deepEquals(a,b){
    if(isPrimitive(a) && isPrimitive(b)){
        if(a!==b){
            return false;
        }
        else{
            return true;
        }
    }
    if(a===b){
        return true;
    }
    if(typeof a !==typeof b){
        return false;
    }

    // typeof returns "object" for null. So, we must handle the cases when one variable is null and the other is an object separately.
    if((a!==null && b===null) || (a===null && b!==null)){
        return false;
    }

    if(Array.isArray(a) && Array.isArray(b)){
        if (a.length!==b.length){
            return false
        }
        for (let i=0; i<a.length;i++) {
            if(!deepEquals(a[i],b[i])){
                return false
            }
        }
        return true
    }
    else if(typeof a ==="object" && typeof b==="object"){
        for(let key in a){
            if(!a.hasOwnProperty(key)){
                continue;
            }
            if(!b.hasOwnProperty(key)){
                return false;
            }
            if(!deepEquals(a[key],b[key])){
                return false;
            }
        }
        for(let key in b){
            if(!b.hasOwnProperty(key)){
                continue;
            }
            if(!a.hasOwnProperty(key)){
                return false;
            }
        }

        return true;
    }
}

var PropertyGraphModel={};

/*
Capabilities to Support:   
    Capabilities:
        - save and load the data model to and from a file on disk           
            - supporting methods:
                - saveToData
                - loadFromData
            - sub-capablity:
                Need to be able to serialize node and edge objects to disk. 
                - This requires creating a uniqueId for each edge and node. The nextObjectId, nextEdgeId, 
                getNextObjectId and getNextEdgeId properties and methods support this.
                - Each node and edge has a savePropertiesToData and loadPropertiesFromData to support
                serialization and deserializaiton.
        - add and delete nodes and edges: 
            - addNode
            - addEdge
            - deleteNode
            - deleteEdge
        - find nodes and edges in the graph:
            - findNodeById
            - findNodeByType
            - findEdgeById
 */
 
PropertyGraphModel.PropertyGraph= class PropertyGraph
{
    constructor(){
        this.nodes=[];
        this.edges=[];
        this.nextObjectId=1;
        this.nextEdgeId=1;
        this.type="PropertyGraphModel";

        // id indexes exist only at runtime currently. It makes loading much faster.
        this.objectIdtoNodeIndex={};
        this.edgeIdToEdgeIndex={};

        this.tagIndex={};
    }
    
    getNextObjectId(){
        var currentObjectId=this.nextObjectId;
        this.nextObjectId+=1;
        return currentObjectId;
    }
    
    getNextEdgeId(){
        var currentEdgeId=this.nextEdgeId;
        this.nextEdgeId+=1;
        return currentEdgeId;
    }
    
    getNodeById(objectId){
        let node=this.objectIdtoNodeIndex[objectId];
        if(node){
            return node;
        } 
        else{
            return null;
        }
    }

    getNodeIndexById(objectId){
        if(!Number.isInteger(objectId)){
            throw new Error("Must pass in an objectId")
        }
        let index=-1
        for (const node of this.nodes) {
            if(node.objectId==objectId){
                return index+=1
            }
            index+=1
        }
        return null;
    }

    getEdgeIndexById(edgeId){
        if(!Number.isInteger(edgeId)){
            throw new Error("Must pass in an objectId")
        }
        let index=-1
        for (const edge of this.edges) {
            if(edge.edgeId==edgeId){
                return index+=1
            }
            index+=1
        }
        return null;
    }
    
    getEdgeById(edgeId){
        let edge=this.edgeIdToEdgeIndex[edgeId];
        if(edge){
            return edge;
        } 
        else{
            return null;
        }
    }

    getNodeByType(objectType){
        let matches=[];
        for (const node of this.nodes) {
            if(node instanceof objectType){
                matches.push(node);
            }
        }
        return matches;
    }
    
    getFirstNodeByType(objectType){
        let nodeMatch=null;
        this.nodes.forEach(function(node){
            if(node instanceof objectType){
                nodeMatch=node;
                return;
            }
        });
        return nodeMatch;
    }
    
    addNode(node){
        if(node.objectId===null){
            node.objectId=this.getNextObjectId();
        }
        else{
            // When adding a node with an existing id, make sure that the nextObjectId is greater than the node that was added. One place this situation can happen is applying diffs that add nodes to the graph.
            if(node.objectId>=this.nextObjectId){
                this.nextObjectId=node.objectId+1;
            }
        }

        if(node.outgoingEdges.length>0){
            throw new Error("Cannot add node with existing outgoing edges");
        }

        if(node.incomingEdges.length>0){
            throw new Error("Cannot add node with existing incoming edges");
        }
        
        if(this.getNodeById(node.objectId)!==null){
            throw new Error("A node with this objectId already exists");
        }
        node.propertyGraph=this;
        this.objectIdtoNodeIndex[node.objectId]=node;
        this.nodes.push(node);
    }
    
    deleteNode(node){
        let nodeIndex=this.getNodeIndexById(node.objectId)             
        if(nodeIndex===null){
            throw new Error("Node does not exist in graph");        
        }
        this.nodes.splice(nodeIndex,1);
        delete this.objectIdtoNodeIndex[node.objectId];
        var that=this;
        node.outgoingEdges.forEach(function(edge){
            that.deleteEdge(edge);
        });
        node.incomingEdges.forEach(function(edge){
            that.deleteEdge(edge);
        });
    }
    
    deleteEdge(edge){
        let edgeIndex=this.getEdgeIndexById(edge.edgeId);
        if(edge===null){
            throw new Error("Edge does not exist in graph");        
        }
        var edge=this.edges[edgeIndex];
        this.edges.splice(edgeIndex,1);
        delete this.edgeIdToEdgeIndex[edge.edgeId];
        var outIndex=edge.sourceNode.outgoingEdges.indexOf(edge);
        edge.sourceNode.outgoingEdges.splice(outIndex,1);
        var inIndex=edge.targetNode.incomingEdges.indexOf(edge);
        edge.targetNode.incomingEdges.splice(inIndex,1);
    }
    
    addEdge(edge){
        if(edge.edgeId===null){
            edge.edgeId=this.getNextEdgeId();
        }
        else{
            // When adding a edge with an existing id, make sure that the nextEdgeId is greater than the edge that was added. One place this situation can happen is applying diffs that add edges to the graph.
            if(edge.edgeId>=this.nextEdgeId){
                this.nextEdgeId=edge.edgeId+1;
            }
        }
        if(this.getEdgeById(edge.edgeId)!==null){
            throw new Error("An edge with the same edgeId already exists")
        }
        let sourceNode=edge.sourceNode;
        let targetNode=edge.targetNode;

        if(this.getNodeById(sourceNode.objectId)===null)
        {
            throw new Error("The source node of the edge does not exist in the property graph")
        }
        if(this.getNodeById(targetNode.objectId)===null){
            throw new Error("The target node of the edge does not exist in the property graph")
        }

        if(sourceNode.outgoingEdges.includes(edge)){
            throw new Error("The edge has already been added to the source node")
        }

        if(targetNode.incomingEdges.includes(edge)){
            throw new Error("The edge has already been added to the target node")
        }
        edge.propertyGraph=this;
        sourceNode.outgoingEdges.push(edge);   
        edge.targetNode.incomingEdges.push(edge);
        this.edgeIdToEdgeIndex[edge.edgeId]=edge;
        this.edges.push(edge);  
    }
    
    saveToData(){
        let graphData={"nextObjectId":this.nextObjectId,"nextEdgeId":this.nextEdgeId,nodes:[],edges:[],tagIndex:{}};
        for (const node of this.nodes) {
            graphData.nodes.push(node.saveToData());
        }
        for(const edge of this.edges){
            graphData.edges.push(edge.saveToData());
        }
        for(const [tagName,tagObject] of Object.entries(this.tagIndex)){
            graphData["tagIndex"][tagName]=tagObject.objectId;
        }
        return graphData;
    }
    
    static loadFromData(data){
        var graph=new PropertyGraphModel.PropertyGraph();
        graph.nextObjectId=data.nextObjectId;
        graph.nextEdgeId=data.nextEdgeId;
        data.nodes.forEach(function(nodeData){
            let nodeObject = PropertyGraphModel.Node.loadFromData(nodeData);
            graph.addNode(nodeObject);
        });
        
        data.edges.forEach(function(edgeData){
            let edgeObject=PropertyGraphModel.Edge.loadFromData(edgeData,graph);
            graph.addEdge(edgeObject);
        });
        
        /**
         * Some nodes contain references to other nodes, these references must be loaded after loading all of the nodes and edges into the graph.
         */
        data.nodes.forEach(function(nodeData){
            var nodeObject=graph.getNodeById(nodeData.objectId);
            if(nodeObject.loadReferencesToOtherNodes!==undefined){
                nodeObject.loadReferencesToOtherNodes(nodeData.properties);
            }
        });

        // files created before tags were implemented will not have a tagIndex.
        if(data.tagIndex){
            for(const [tagName,objectId] of Object.entries(data.tagIndex)){
                graph.tagIndex[tagName]=graph.getNodeById(objectId);
            }
        }
        return graph;
    }
    
    copy(){
        return PropertyGraphModel.PropertyGraph.loadFromData(this.saveToData());
    }
}

/**
 * This is the base edge class of the property graph.
 * 
 * To define a new edge type, see the instructions in the Node class. The differences are that a new edge type should inherit from the base Edge class and that in TypeToClassMaps.js you should add an entry to TypeToClassMaps.edgeTypeToClassMap.
 */
PropertyGraphModel.Edge=class Edge{
    constructor(sourceNode,targetNode){
        this.sourceNode=sourceNode;    
        this.targetNode=targetNode;
        this.edgeId=null;
        this.propertyGraph=null;
        this.type="Edge";
    }
    
    savePropertiesToData(){
        return {};
    }
    
    loadPropertiesFromData(data){
        
    }

    saveToData(){
        let data={"edgeId":this.edgeId,"type":this.type,"properties":this.savePropertiesToData(),
        sourceNode:this.sourceNode.objectId,targetNode:this.targetNode.objectId}
        return data;
    }

    static loadFromData(edgeData,graph){
        let edgeClass=TypeToClassMaps.getEdgeClassFromType(edgeData.type);
        let edgeObject=new edgeClass();
        edgeObject.edgeId=edgeData.edgeId;
        edgeObject.loadPropertiesFromData(edgeData.properties);
        edgeObject.sourceNode=graph.getNodeById(edgeData.sourceNode);
        edgeObject.targetNode=graph.getNodeById(edgeData.targetNode);
        return edgeObject;
    }
}

/**
 * This is the base node class of the property graph.
 * 
 * In order to define a new node type do the following:
 *  - Create a new node class that extends PropertyGraphModel.Node
 *  - Define serialization attributes and methods:
 *    - Add a type attribute. This is used in deserialization to determine which class a piece of data belongs to.
 *    - In TypeToClassMaps.js add an entry to TypeToClassMaps.nodeTypeToClassMap mapping the name in the type attribute to the appropriate class.
 *    - Define a constructor. By default, constructors take no parameters. The reason for this is that the loadFromData class for the base Node does not construct a node object with parameters. If you wish to change this, this method should be overriden in the child class.
 *    - Define savePropertiesToData, which returns a JSON object contain the attributes. This should only contain the attributes defined in this class, except for the type attribute which is always included.
 *    - Define a loadPropertiesFromData method, which takes a JSON object and converts it into the attributes of the class. This is the opposite of savePropertiesToData.
 */
PropertyGraphModel.Node=class Node{
    constructor(){
        this.outgoingEdges=[];
        this.incomingEdges=[];
        this.objectId=null;
        this.propertyGraph=null;
        this.type="Node";
    }
    
    savePropertiesToData(){
        return {};
    }
    
    loadPropertiesFromData(data){
        
    }

    saveToData(){
        let data={"objectId":this.objectId,"type":this.type,"properties":this.savePropertiesToData(),"outgoingEdges":[],"incomingEdges":[]};
        for(const edge of this.outgoingEdges){
            data.outgoingEdges.push(edge.edgeId)
        }
        for(const edge of this.incomingEdges){
            data.incomingEdges.push(edge.edgeId)
        }
        return data;
    }

    static loadFromData(nodeData){
        let nodeClass=TypeToClassMaps.getNodeClassFromType(nodeData.type);
        let nodeObject=new nodeClass();
        nodeObject.objectId=nodeData.objectId;
        nodeObject.loadPropertiesFromData(nodeData.properties);
        return nodeObject;
    }
}

/*
 *  Capability:
 *      - The nodes in a tree at the same level should have an order to them. The position of the node
 *      in the tree at that level is specified by the index property of the TreeChild.
 */
PropertyGraphModel.TreeChildEdge=class TreeChildEdge extends PropertyGraphModel.Edge{
     constructor(sourceNode,targetNode){
        super(sourceNode,targetNode);
        this.index=null; 
        this.type="TreeChildEdge";
     }
     
     savePropertiesToData(){
         return {"index":this.index};
     }
     
     loadPropertiesFromData(data){
         this.index=data.index;
     }
}


/*
    Diffs work by comparing to different versions of the same graph against each other. Comparing two different graphs is not supported because the diff algorithm relies on the unique object ids of each object in the graph to make the comparison.
    
    The diff contains node data serialized to JSON and not node objects to avoid sharing object references by mistake.

    For nodesChangedProperties and edgesChangedProperties data is stored in a {"from":beforeData,"to":afterData}. The before and after data are used to create a reverse diff without having to do a graph comparison.
*/
PropertyGraphModel.Diff=class Diff{
    constructor(){
        this.nodesAdded=[];
        this.edgesAdded=[];
        this.nodesDeleted=[];
        this.edgesDeleted=[];
        this.nodesChangedProperties=[];
        this.edgesChangedProperties=[];
    }
}

PropertyGraphModel.reverseDiff=function(diff){
    let diffCopy=JSON.parse(JSON.stringify(diff));
    let reverseDiff=new PropertyGraphModel.Diff();
    reverseDiff.nodesAdded=diffCopy.nodesDeleted;
    reverseDiff.nodesDeleted=diffCopy.nodesAdded;
    reverseDiff.nodesChangedProperties=[]
    for(const {to,from} of diffCopy.nodesChangedProperties){
        reverseDiff.nodesChangedProperties.push({"to":from,"from":to})
    }
    
    reverseDiff.edgesAdded=diffCopy.edgesDeleted;
    reverseDiff.edgesDeleted=diffCopy.edgesAdded;
    reverseDiff.edgesChangedProperties=[]
    for(const {to,from} of diffCopy.edgesChangedProperties){
        reverseDiff.edgesChangedProperties.push({"to":from,"from":to})
    }

    return reverseDiff;
}

/*
Gen diff such that if you apply the diff to graph1 you will get graph2.
This function works on serialized graph data not PropertyGraph objects.
*/
PropertyGraphModel.genDiff=function(graph1,graph2){
    if(graph1 instanceof PropertyGraphModel.PropertyGraph){
        throw new Error("graph1 must not be a property graph.")
    }

    if(graph2 instanceof PropertyGraphModel.PropertyGraph){
        throw new Error("graph2 must not be a property graph.")
    }

    let diff=new PropertyGraphModel.Diff();
    
    let nodesGraph1={};
    let nodesGraph2={};
    for(let node of graph1.nodes){        
        nodesGraph1[node.objectId]=node;
    }
    for(let node of graph2.nodes){
        nodesGraph2[node.objectId]=node;
    }
    
    diff.nodesAdded=[]
    for(let node of graph2.nodes){
        if(!(node.objectId in nodesGraph1)){
            /* When adding a node to the graph, the edges attached to that node will be added as well. 
            The edges are stored in the edgesAdded property. In order to prevent the same edge from being stored twice in the same node we have to clear the incoming and outgoing edges in the diff.
            */
            node.incomingEdges=[]
            node.outgoingEdges=[]
            diff.nodesAdded.push(node)
        }
    }
    
    diff.nodesDeleted=[]
    diff.nodesChangedProperties=[];
    for(let node of graph1.nodes){
        if(!(node.objectId in nodesGraph2)){
            /* When adding a node to the graph, the edges attached to that node will be added as well. 
            The edges are stored in the edgesAdded property. In order to prevent trying to delete the same edge twice we have to clear the incoming and outgoing edges in the diff.
            */
            node.incomingEdges=[]
            node.outgoingEdges=[]
            diff.nodesDeleted.push(node)
        }
        else{
            let node2=nodesGraph2[node.objectId];
            if(!deepEquals(node.properties,node2.properties)){
                diff.nodesChangedProperties.push({"from":node,"to":node2});
            }
        }
    }
    
    let edgesGraph1={};
    let edgesGraph2={};
    for(let edge of graph1.edges){
        edgesGraph1[edge.edgeId]=edge;
    }
    for(let edge of graph2.edges){
        edgesGraph2[edge.edgeId]=edge;
    }
    
    diff.edgesAdded=[]
    for(let edge of graph2.edges){
        if(!(edge.edgeId in edgesGraph1)){
            diff.edgesAdded.push(edge)
        }
    }
    
    diff.edgesDeleted=[]
    diff.edgesChangedProperties=[];
    for(let edge of graph1.edges){
        if(!(edge.edgeId in edgesGraph2)){
            diff.edgesDeleted.push(edge)
        }
        else{
            let edge2=edgesGraph2[edge.edgeId];
            if(!deepEquals(edge.properties,edge2.properties)){
                diff.edgesChangedProperties.push({"from":edge,"to":edge2});
            }
        }
    }

    return diff;
}

/*
Apply the diff to the property graph modifies the graph in place.
Here a diff is made of JSON data objects and not property graph nodes.
*/
PropertyGraphModel.applyDiff=function(graph,diff){
    for(let nodeData of diff.nodesAdded){
        let node=PropertyGraphModel.Node.loadFromData(nodeData);
        graph.addNode(node);
    }
    
    for(let nodeData of diff.nodesDeleted){
        let node=PropertyGraphModel.Node.loadFromData(nodeData);
        graph.deleteNode(node);
    }
    
    for(let changeData of diff.nodesChangedProperties){
        let currentNode=graph.getNodeById(changeData.to.objectId)
        currentNode.loadPropertiesFromData(changeData.to.properties)
    }
    
    for(let edgeData of diff.edgesAdded){
        let edge=PropertyGraphModel.Edge.loadFromData(edgeData,graph);
        graph.addEdge(edge);
        
    }
    
    for(let edgeData of diff.edgesDeleted){
        let edge=PropertyGraphModel.Edge.loadFromData(edgeData,graph);
        // The edge could have already been remove if there was a node deleted by applying the diff
        let hasEdge=graph.getEdgeById(edge.edgeId);
        if(hasEdge!==null){
            graph.deleteEdge(edge);
        }
    }
    
    for(let changeData of diff.edgesChangedProperties){
        let edge=graph.getEdgeById(changeData.to.edgeId);
        edge.loadPropertiesFromData(changeData.to.properties)
    }

    /**
     * Some nodes contain references to other nodes, these references must be loaded after loading all of the nodes and edges into the graph.
     */
    for(let nodeData of diff.nodesAdded){
        let nodeObject=graph.getNodeById(nodeData.objectId);
        if(nodeObject.loadReferencesToOtherNodes!==undefined){
            nodeObject.loadReferencesToOtherNodes(nodeData.properties);
        }
    }
    
    return graph;
}