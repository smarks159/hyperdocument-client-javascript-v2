/**
 * NavigationHistory records which document and the rootObject of the document view where the user has been. This is used to implement jump history. In order to keep track of user history setCurrent must be called after each jump operation you wish to record.
 */
class NavigationHistory {
    constructor(maxHistory=null){
        this.backward=[];
        this.forward=[];
        this.current=null;
        if(maxHistory){
            this.maxHistory=maxHistory;
        }
        else{
            this.maxHistory=50;
        }
    }

    setCurrent(historyObject){
        if(this.current!==null && !this.current.equals(historyObject)){
            if(this.backward.length===this.maxHistory){
                this.backward.shift();
            }  
            this.backward.push(this.current);
        }
        this.current=historyObject;
        this.forward=[];
    }

    goBack(){
        if(this.backward.length===0){
            return null;
        }
        this.forward.push(this.current);
        this.current=this.backward.pop();
        return this.current;
    }

    goForward(){
        if(this.forward.length===0){
            return null;
        }
        this.backward.push(this.current);
        this.current=this.forward.pop();
        return this.current;
    }
}

class jumpHistory {
    constructor(selectedRootObj,docDataModel){
        this.selectedRootObj=selectedRootObj;
        this.doc=docDataModel;
    }

    getFileName(){
        return this.doc.getFile().fileName;
    }

    equals(obj){
        if(this.selectedRootObj.objectId==obj.selectedRootObj.objectId && this.doc.objectId==obj.doc.objectId){
            return true;
        }
        else{
            return false;
        }
    }
}