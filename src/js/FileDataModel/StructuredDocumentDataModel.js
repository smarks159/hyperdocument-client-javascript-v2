"use strict";

DataModels.StructuredDocumentDataModel=class StructuredDocumentDataModel extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.type="StructuredDocumentDataModel";
    }

    isEmpty(){
        return this.outgoingEdges.length===0;
    }

    getFile(){
        let fileContentEdge=this.incomingEdges.filter(edge=>edge instanceof DataModels.FileContentEdge)[0];
        return fileContentEdge.sourceNode;
    }

    /**
     * Get the object structural path. The format of the path is one of alternating numbers and letters, starting with a number relative to the top of the document node. An address of 0 is the origin node, which represents the document node.
     * @param {PropertyGraphModel.Node} dataNode 
     */
    getNodeStructuralAddress(dataNode){
        if(dataNode==this){
            return "0";
        }
        // we build the path starting from the current object and work our way up to the origin. We first must determine whether the last element in the address should be display as a letter or a number
        let level=this.getNodeLevelFromOrigin(dataNode);
        let letter=false;
        if(level%2==0){
            letter=true;
        }

        let path=""
        let currentNode=dataNode;
        while(currentNode!=this){
            // indexes in addresses are 1 based while getNodeIndex is zero based so we add one here.
            let index=new TreeNodeInterface(currentNode).getNodeIndex()+1;
            if(letter===false){
                path=index+path;
                letter=true;
            }
            else{
                path=this.convertIndexToLetterPath(index)+path;
                letter=false;
            }
            currentNode=new TreeNodeInterface(currentNode).parent;
        }
        return path
    }

    getNodeRelativeAddress(dataNode,relativeTo){
        let level=this.getNodeLevelFromAncestor(dataNode,relativeTo);
        let letter=false;
        if(level%2==0){
            letter=true;
        }
        let path=""
        let currentNode=dataNode;
        while(currentNode!=relativeTo){
            // indexes in addresses are 1 based while getNodeIndex is zero based so we add one here.
            let index=new TreeNodeInterface(currentNode).getNodeIndex()+1;
            if(letter===false){
                path=index+path;
                letter=true;
            }
            else{
                path=this.convertIndexToLetterPath(index)+path;
                letter=false;
            }
            currentNode=new TreeNodeInterface(currentNode).parent;
        }
        return "./"+path
    }

    getNodeLevelFromOrigin(dataNode){
        return this.getNodeLevelFromAncestor(dataNode,this);
    }

    getNodeLevelFromAncestor(dataNode,ancestorNode){
        let level=0
        let currentObj=dataNode;
        while(currentObj!==ancestorNode){
            level+=1;
            currentObj=new TreeNodeInterface(currentObj).parent;
        }
        return level;
    }

    /**
     * Converts an integer index into a letter, similar to column names in excel. A-Z would be indexes 1-26, 27 is AA, 28 is AB, etc.
     * @param {integer} index 
     */
    convertIndexToLetterPath(index){
        let baseChar="A".charCodeAt(0);
        let letterPath=""
        while(index>0){
            letterPath=String.fromCharCode(baseChar+((index-1)%26))+letterPath;
            index=Math.floor((index-1)/26);
        }
        return letterPath;
    }

    /**
     * Does the opposite of convertIndexToLetterPath
     * @param {string} letters 
     */
    convertLetterPathToIndex=function(letters){
        let letterArray=letters.split("");
        let index=0;
        for (let i=letterArray.length-1; i>=0;i--){
            let letter=letterArray[i].toUpperCase();
            let num=letter.charCodeAt(0)-64;
            index=index+(num*Math.pow(26,letterArray.length-(i+1)));
        }
        return index;
    }

    getNodeFromAddress(address,relativeTo=null){
        if(address==""){
            return null;
        }
        if(address=="0"){
            return this;
        }

        // the address can be an object id string starting with a 0
        else if(address.match(/^0[0-9]+$/)){
            let id=parseInt(address.slice(1));
            return wikiDesktop.wikiData.propertyGraph.getNodeById(id);
        }
        // the address can be relative and start with a "./"
        else if(address.match(/^\.\//)){
            if(relativeTo===null){
                throw new Error("Must pass in the relativeTo parameter for a relative address");
            }
            let addressPath=address.slice(2);
            addressPath=addressPath.split(/([0-9]+)/).filter((item)=>item!="")
            let currentNode=relativeTo;
            for(const pathEl of addressPath){
                let index;
                if(isNaN(pathEl)){
                    index=this.convertLetterPathToIndex(pathEl);
                }
                else{
                    index=parseInt(pathEl);
                }
                currentNode=new TreeNodeInterface(currentNode).children[index-1];
                if(currentNode===undefined){
                    return null;
                }
            }
            return currentNode
        }
        // the address can be a structural address of alternating numbers and letters relative to the top of the document.
        else{
            let addressPath=address.split(/([0-9]+)/).filter((item)=>item!="");
            let currentNode=this;
            for(const pathEl of addressPath){
                let index;
                if(isNaN(pathEl)){
                    index=this.convertLetterPathToIndex(pathEl);
                }
                else{
                    index=parseInt(pathEl);
                }
                currentNode=new TreeNodeInterface(currentNode).children[index-1];
                if(currentNode===undefined){
                    return null;
                }
            }
            return currentNode
        }
    }
}