var DataModels={};

DataModels.TextDataModel=class TextDataModel extends PropertyGraphModel.Node{
    constructor(){
        super();    
        this.text="";
        this.type="TextDataModel";
    }
    
    savePropertiesToData(){
        return {"text":this.text};
    }
    
    loadPropertiesFromData(data){
        this.text=data["text"];
    }
};