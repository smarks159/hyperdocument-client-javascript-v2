class KeyboardChoiceHelpPresenter extends Presenters.Presenter{
    constructor(){
        super();
        this._choices=[]
    }
    choices(choices){
        this._choices=choices;
        return this;
    }
    definition(){
        let col=[];
        col.push(this.textLabel("Available Choices:").css({"font-weight":"bold"}));
        for(const choice of this._choices){
            col.push(this.textLabel(`Choice:${choice.key}, Value:${choice.text}`));
        }
        return this.column(col);
    }
}

/**
 * This class displays the main command interpreter on the top of the page, in command interpreter mode.
 * 
 * After initialization, the config file, defining the commands and the action dispatcher, defining how to map action names to functions in the system, must be passed in seperately. See the loadBaseCommandInterpreterSubsystem function for an example about how to do this.
 */
class CommandInterpreterPresenter extends Presenters.Presenter{
    constructor(){
        super();
        this.textToDisplay=[];
        this.commandInterpreter=new CommandInterpreter();
        this.keyboardChoiceHelp=new KeyboardChoiceHelpPresenter().class("panel").hide();
        this._keyboardListener=createKeypress(document.body);
        this.initEvents();
        this.textDisplayPresenter=null;
        this.config=null;
        this.actionDispatcher=null;
    }

    definition(){
        this.textDisplayPresenter=this.textLabel(this.textToDisplay.join(" ")).css({
            "min-height":"2em",
            "border":"1px solid black",
            "font-weight":"bold",
            "padding":"0.3em"
        }).class("panel");
        return this.column([
            this.textDisplayPresenter,
            this.keyboardChoiceHelp,
            this.defAddressInput().hide()
        ])
    }

    defAddressInput(){
        let inputForm=new Presenters.SelectAddressPresenter()
            .onSelectNode((selectResponse)=>{
                if(selectResponse.mode=="TypedAddress"){
                    this.displayText(`addressSelected:${selectResponse.addressInput}`);
                }
                else{
                    this.removePreviousText()
                    this.displayText(`[${selectResponse.presenterType} selected in ${selectResponse.selectedColor}]`);
                }
                this.deactivateObjectSelection();
                this.commandInterpreter.sendResponse("SelectObject",{"nodeSelected":selectResponse.node})
            })
            .onCancel(()=>{
                // The user can press 'space' to display the address input form and then the cancel button to hide it again. The space listener must be reregistered in order for hitting space again to work and display the input.
                this.registerSpaceKeyListenerForAddressInput();
            })
        this.addressInput=inputForm.class("panel");
        return this.addressInput;
    }

    initEvents(){
        this.commandInterpreter.listenForRequest("KeyboardChoice",(args)=>{
            this.displayText(`(${args.noisewords})`);
            this.activateKeyboardChoices(args.choices);
        })

        this.commandInterpreter.listenForRequest("SelectObject",({presenterType,selectedColor})=>{
            this.displayText(`[select ${presenterType}]`);
            this.activateObjectSelection(presenterType,selectedColor);
        })

        this.commandInterpreter.listenForRequest("ExecuteAction",({actionName,params,resultsVariable})=>{
            let result=this.actionDispatcher.execute(actionName,...params);
            if(resultsVariable!==null){
                this.commandInterpreter.setVariable(resultsVariable,result)
            }
        })

        this.commandInterpreter.listenForRequest("ResetInterpreter",()=>{
            this.run();
        })
    }

    displayText(text){
        this.textToDisplay.push(text);
        this.updateText();
    }

    clearText(){
        this.textToDisplay=[];
        this.updateText();
    }

    removePreviousText(){
        this.textToDisplay.pop();
        this.updateText();
    }

    updateText(){
        this.textDisplayPresenter.text(this.textToDisplay.join(" "));
    }

    loadConfig(config){
        this.config=config;
        this.commandInterpreter.loadConfig(config);
    }

    loadActionDispatcher(actionDispatcher){
        this.actionDispatcher=actionDispatcher
    }

    activateKeyboardChoices(choices){
        this.keyboardChoiceHelp.choices(choices);
        this.keyboardChoiceHelp.refresh();
        for(const choice of choices){
            this._keyboardListener.register_combo({
                "keys":choice.key,
                "is_exclusive":true,
                "prevent_default":true,
                "on_keydown":(e)=>{                    
                    e.stopPropagation();
                    // remove noiseword after selection is made.
                    this.removePreviousText();
                    this.displayText(choice.text);
                    this.deactivateKeyboardChoices(choices);
                    this.keyboardChoiceHelp.hide();
                    this.commandInterpreter.sendResponse("KeyboardChoice",{"choice":choice.value});
                }
            })
        }
        this._keyboardListener.register_combo({
            keys:"?",
            "is_exclusive":true,
                "prevent_default":true,
                "on_keydown":(e)=>{
                    e.stopPropagation();
                    this.keyboardChoiceHelp.show();
                }
        })
    }

    activateObjectSelection(presenterType,selectedColor){
        this.registerSpaceKeyListenerForAddressInput();
        this.addressInput.activateObjectSelection(presenterType,selectedColor);
    }

    registerSpaceKeyListenerForAddressInput(){
        this._keyboardListener.register_combo({
            keys:"space",
            "is_exclusive":true,
            "on_keydown":(e)=>{
                this.addressInput.show();
                this.addressInput.focusOnFirstField();
                // Unregister the space key listener so the user can type a space in the address.
                this._keyboardListener.unregister_combo("space");
                
            }
        })
    }

    deactivateObjectSelection(){
         // When calling run for the first time, it calls deactivateObjectSelection before addressInput is defined. So, there must be a check here to see if addressInput exists
         if(this.addressInput){
            this.addressInput.deactivateObjectSelection();
        }
        this._keyboardListener.unregister_combo("space");
    }

    deactivateKeyboardChoices(choices){
        for(const choice of choices){
            this._keyboardListener.unregister_combo(choice.key);
        }
    }

    clearKeyboardListener(){        
        this._keyboardListener.destroy();
        this._keyboardListener=createKeypress(document.body);
    }

    initKeyboardListener(){
        if(this._keyboardListener){
            this.clearKeyboardListener();
        }
       
        this._keyboardListener.register_combo({
            "keys":"esc",
            "is_exclusive":true,
            "prevent_default":true,
            "on_keydown":(e)=>{
                if(this.keyboardChoiceHelp.visible){
                    this.keyboardChoiceHelp.hide();
                }
                this.run();
            }
        })
    }

    run(){
        this.reset();
        this.initEvents();
        this.initKeyboardListener();
        this.commandInterpreter.executeCurrentCommand();
    }

    reset(){
        this.commandInterpreter.reset();
        if(this.htmlEl){
            this.deactivateObjectSelection();
            
            if(this.addressInput){
                this.addressInput.unselectPresenters();
            }
            this.clearKeyboardListener();
            this.clearText();
        }
    }
}