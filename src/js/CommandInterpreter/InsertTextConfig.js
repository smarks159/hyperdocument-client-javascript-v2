/**
 * The select position dialog for the InsertText command is used in both the context menu config and in the command interpreter specs, so it is defined in a seperate, resuable Config definition.
 * 
 * Requirements: The variable hasChildren must be set in order to use this config.
 */
class InsertTextSelectPosition extends CommandConfig{
definition(){
return this.Sequence([
    this.if(this.Equals("isTargetDisplayedAsRoot",false),
        this.KeyboardChoice({
            choices: ["before","next","child"],
            variableName:"position",
            noisewords:"Insert Text Where?",
        })
    ),
    // If the target is displayed as root then only the child nodes are visible on the screen, so the only option displayed should be a child position.
    this.if(this.Equals("isTargetDisplayedAsRoot",true),
        this.setVariable("position","child")
    ),
    this.if(this.AND(this.Equals("position","child"),this.Equals("hasChildren",true)),
        this.KeyboardChoice({
            choices: [
                this.Choice({"text":"First","key":"f","value":"childFirst"}),
                this.Choice({"text":"Last","key":"l","value":"childLast"})
            ],
            variableName:"position",
            noisewords:"Insert where in Child?"
        })
    ),
    this.if(this.AND(this.Equals("position","child"),this.Equals("hasChildren",false)),
        this.setVariable("position","childFirst")
    )
])
}
}

/**
 * When Inserting a Text Node, using a context menu option, the user does not select the presenter in the interpreter. They just type the position the want to insert the node at relative to the node the context menu was opened on.
 */
class InsertTextContextMenuConfig extends CommandConfig{
definition(){
return this.Sequence([
    this.subConfig(new InsertTextSelectPosition()),
    this.executeAction({actionName:"insertText",params:["position"]})
])
}
}