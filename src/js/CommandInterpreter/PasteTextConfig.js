/**
 * The select position dialog for the PasteText config is defined separately because it is reused in serveral places. It is reused between the menu config and the command interpreter. It is also reused in both the paste branch and paste from clipboard commands.
 * 
 * The PasteText Config is similar to the InsertText Config expect it has a "Here" option and the noisewords are different.
 * 
 * This config requires the variables hasChildren and hasContent to be set.
 */
class PasteTextSelectPosition extends CommandConfig {
definition(){
return this.Sequence([
    this.if(this.Equals("isTargetDisplayedAsRoot",false),
        this.KeyboardChoice({
            choices: ["before","next","child","here"],
            variableName:"position",
            noisewords:"Paste Text Where?",
        })
    ),
    // If the target is displayed as root then only the child nodes are visible on the screen, so the only option displayed should be a child position.
    this.if(this.Equals("isTargetDisplayedAsRoot",true),
        this.setVariable("position","child")
    ),
    this.setVariable("actionCanceled",false),
    this.if(this.AND(this.Equals("position","child"),this.Equals("hasChildren",true)),
        this.KeyboardChoice({
            choices: [
                {"text":"first","key":"f","value":"childFirst"},
                {"text":"last","key":"l","value":"childLast"}
            ],
            variableName:"position",
            noisewords:"Paste text where in Child?"
        })
    ),
    this.if(this.AND(this.Equals("position","child"),this.Equals("hasChildren",false)),
        this.setVariable("position","childFirst")
    ),
    this.if(this.AND(this.Equals("position","here"),this.AND(this.Equals("hasChildren",false),this.Equals("hasContent",false))),
        this.setVariable("position","here")
    ),
    this.if(this.AND(this.Equals("position","here"),this.OR(this.Equals("hasChildren",true),this.Equals("hasContent",true))),this.Sequence([
        this.KeyboardChoice({
            choices:["yes","no"],
            variableName:"confirmHere",
            noisewords:"The selected node contains text or has children, which will be overwritten do you want to proceed?"
        }),
        this.if(this.Equals("confirmHere","yes"),
            this.setVariable("position","here"),
        ),
        this.if(this.Equals("confirmHere","no"),
            this.setVariable("actionCanceled",true)
        )
    ]))
])
}
}

/**
 * When the user calls the paste menu option, the user has already copied the src to the clipboard and the user does not select select the presenter target in the interpreter. They just type the position they want to insert the node at relative to the node the context menu was opened on.
 */
class PasteTextContextMenuConfig extends CommandConfig{
definition(){
return this.Sequence([
    this.subConfig(new PasteTextSelectPosition()),
    this.if(this.NOT(this.Equals("actionCanceled",true)),
        this.executeAction({actionName:"pasteBranch","params":["position"]})
    ),
    this.resetInterpreter()
])
}
}

/**
 * This is the same as PasteTextContextMenuConfig except it pastes text from the os clipboard instead of from the hyperdocument system.
 */
class PasteTextFromClipboardContextMenuConfig extends CommandConfig{
definition(){
return this.Sequence([
    this.subConfig(new PasteTextSelectPosition()),
    this.if(this.NOT(this.Equals("actionCanceled",true)),
        this.executeAction({actionName:"pasteTextFromClipboard","params":["position"]})
    ),
    this.resetInterpreter()
])
}
}