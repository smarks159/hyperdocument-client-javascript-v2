ConfigAST={};

ConfigAST.SequenceNode=class SequenceNode extends PropertyGraphModel.Node {
    constructor(){
        super();
        this.nodes=[];
        this.type="sequence"
    }
    push(node){        
        this.nodes.push(node);
    }
    savePropertiesToData(){
        return {"nodes":this.nodes.map(node=>node.objectId)};
    }
    loadReferencesToOtherNodes(data){
        let node;
        for(const objectId of data.nodes){
            node=this.propertyGraph.getNodeById(objectId)
            this.nodes.push(node);
        }
    }
}

ConfigAST.KeyboardChoice=class KeyboardChoice extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.choices=[];
        this.variableName=null;
        this.noisewords=null;
        this.type="KeyboardChoice";
    }
    savePropertiesToData(){
        return {"choices":this.choices.map(node=>node.objectId),"variableName":this.variableName,"noisewords":this.noisewords};
    }
    loadPropertiesFromData(data){
        this.variableName=data.variableName;
        this.noisewords=data.noisewords;
    }
    loadReferencesToOtherNodes(data){
        for(const objectId of data.choices){
            this.choices.push(this.propertyGraph.getNodeById(objectId));
        }
    }
}

ConfigAST.Choice=class Choice extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.text=null;
        this.key=null;
        this.value=null;
        this.type="Choice";
    }
    savePropertiesToData(){
        return {"text":this.text,"key":this.key,"value":this.value};
    }
    loadPropertiesFromData(data){
        this.text=data.text;
        this.key=data.key;
        this.value=data.value;
    }
}

ConfigAST.If=class If extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.condition=null;
        this.ifTrue=null;
        this.type="if";
    }
    savePropertiesToData(){
        return {
            "condition":this.condition ? this.condition.objectId : null,
            "ifTrue":this.ifTrue ? this.elseStatement.objectId : null
        }
        
    }
    loadReferencesToOtherNodes(data){
        if(data.condition){
            this.condition=this.propertyGraph.getNodeById(data.condition.objectId);
        }
        if(data.ifTrue){
            this.ifTrue=this.propertyGraph.getNodeById(data.ifTrue.objectId);
        }
    }
}

ConfigAST.Equals=class Equals extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.variable=null;
        this.value=null;
        this.type="Equals"
    }
    savePropertiesToData(){
        return{"variable":this.variable,"value":this.value}
    }
    loadPropertiesFromData(data){
        this.variable=data.variable;
        this.value=data.value;
    }
}

ConfigAST.AND=class AND extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.lhs=null;
        this.rhs=null;
        this.type="AND";
    }

    savePropertiesToData(){
        return {"lhs":this.lhs.objectId,"rhs":this.rhs.objectId};
    }

    loadReferencesToOtherNodes(data){
        this.lhs=this.propertyGraph.getNodeById(data.lhs.objectId);
        this.rhs=this.propertyGraph.getNodeById(data.rhs.objectId);
    }
}

ConfigAST.OR=class OR extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.lhs=null;
        this.rhs=null;
        this.type="OR";
    }

    savePropertiesToData(){
        return {"lhs":this.lhs.objectId,"rhs":this.rhs.objectId};
    }

    loadReferencesToOtherNodes(data){
        this.lhs=this.propertyGraph.getNodeById(data.lhs.objectId);
        this.rhs=this.propertyGraph.getNodeById(data.rhs.objectId);
    }
}

ConfigAST.NOT=class NOT extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.statement=null;
        this.type="NOT";
    }

    savePropertiesToData(){
        return {"statement":this.statement.objectId};
    }

    loadReferencesToOtherNodes(data){
        this.statement=this.propertyGraph.getNodeById(data.statement.objectId);
    }
}

ConfigAST.SetVar=class SetVar extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.varName=null;
        this.value=null;
        this.type="SetVar";
    }
    savePropertiesToData(){
        return {"varName":this.varName,"value":this.value}
    }
    loadPropertiesFromData(data){
        this.varName=data.varName;
        this.value=data.value;
    }
}

ConfigAST.ExecuteAction=class ExecuteAction extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.params=[];
        this.actionName=null;
        this.resultsVariable=null;
        this.type="ExecuteAction";
    }
    savePropertiesToData(){
        return{"params":this.params,"actionName":this.actionName}
    }
    loadPropertiesFromData(data){
        this.params=data.params;
        this.actionName=data.actionName;
    }
}

ConfigAST.selectObject=class SelectObject extends PropertyGraphModel.Node{
    constructor(){
        super();
        this.presenterType=null;
        this.variableName=null;
        this.selectedColor=null;
        this.type="SelectObject";
    }
    savePropertiesToData(){
        return{"presenterType":this.presenterType,"variableName":this.variableName,"selectedColor":this.selectedColor}
    }
    loadPropertiesFromData(data){
        this.presenterType=data.presenterType;
        this.variableName=data.variableName;
        this.selectedColor=data.selectedColor;
    }
}

ConfigAST.ResetInterpreter=class ResetInterpreter extends PropertyGraphModel.Node{
    constructor(){
        super()
        this.type="ResetInterpreter";
    }
}


/**
 * This is the base class for building a config. The functions in this class are used to build nodes in a property graph representing the program to be executed. Writing a config file definition is sort of like writing a programming language were the methods are program statements or expressions. This is useful for defining and prototyping a language without having to build a seperate parser.
 */
class CommandConfig {
    constructor(){
        this.propertyGraph=new PropertyGraphModel.PropertyGraph();
    }

    definition(){
        throw new Error("This method must be implemented in the child");
    }

    Sequence(statements){
        if (!(statements instanceof Array)) {
            throw Error("arguement must be an array")
        }
        let sequence=new ConfigAST.SequenceNode();
        this.propertyGraph.addNode(sequence);
        for(const statement of statements){
            sequence.push(statement);
            new TreeNodeInterface(sequence).appendChild(statement);
        }
        return sequence;
    }

    KeyboardChoice({choices,variableName="_lastCommand",noisewords}){
        let keyboardChoice=new ConfigAST.KeyboardChoice();
        let choicesAST=[];
        for (const choice of choices) {
            if(typeof choice=="string"){
                choicesAST.push(this.Choice({text:choice,key:choice[0],value:choice}));
            }
            else if(choice instanceof ConfigAST.Choice){
                choicesAST.push(choice);
            }
            else if(choice.constructor==Object){
                choicesAST.push(this.Choice(choice));
            }
        }
        keyboardChoice.choices=choicesAST;
        keyboardChoice.variableName=variableName;
        keyboardChoice.noisewords=noisewords;
        this.propertyGraph.addNode(keyboardChoice);
        return keyboardChoice;
    }

    lastCommandEq(valueEquals,sequence){
        if(sequence instanceof Array){
            sequence=this.Sequence(sequence);
        }
        return this.if(this.Equals("_lastCommand",valueEquals),sequence);
    }

    Choice({text,key,value}){
        let choice=new ConfigAST.Choice();
        choice.text=text;
        choice.key=key;
        choice.value=value;
        this.propertyGraph.addNode(choice);
        return choice;
    }

    if(condition,ifTrue){
        let ifStatement=new ConfigAST.If();
        ifStatement.condition=condition;
        ifStatement.ifTrue=ifTrue;
        this.propertyGraph.addNode(ifStatement);
        new TreeNodeInterface(ifStatement).appendChild(ifTrue);
        return ifStatement;
    }

    Equals(variable,value){
        let equals=new ConfigAST.Equals();
        equals.variable=variable;
        equals.value=value;
        this.propertyGraph.addNode(equals);
        return equals;
    }

    AND(lhs,rhs){
        let andNode=new ConfigAST.AND();
        andNode.lhs=lhs;
        andNode.rhs=rhs;
        this.propertyGraph.addNode(andNode);
        return andNode;
    }

    OR(lhs,rhs){
        let orNode=new ConfigAST.OR();
        orNode.lhs=lhs;
        orNode.rhs=rhs;
        this.propertyGraph.addNode(orNode);
        return orNode;
    }

    NOT(statement){
        let notNode=new ConfigAST.NOT();
        notNode.statement=statement;
        this.propertyGraph.addNode(notNode);
        return notNode;
    }

    setVariable(varName,value){
        let node=new ConfigAST.SetVar();
        node.varName=varName;
        node.value=value;
        this.propertyGraph.addNode(node);
        return node;
    }

    executeAction({params,actionName,resultsVariable=null}){
        let executeStatement=new ConfigAST.ExecuteAction();
        executeStatement.params=params;
        executeStatement.actionName=actionName;
        executeStatement.resultsVariable=resultsVariable;
        this.propertyGraph.addNode(executeStatement);
        return executeStatement;
    }

    subConfig(config){
        config.propertyGraph=this.propertyGraph;
        return config.definition();
    }

    selectNode({variableName,presenterType,selectedColor="yellow"}){
        let node=new ConfigAST.selectObject();
        node.variableName=variableName;
        node.presenterType=presenterType;
        node.selectedColor=selectedColor;
        return node;
    }

    resetInterpreter(){
        return new ConfigAST.ResetInterpreter();
    }
}