/**
 * The EventEmitter class is used by the CommandInterpreter to implement a request/response pattern when talking with Presenter elements to get user input.
 * In the future messages should have a defined schema and should be checked for the proper format.
 */
class EventEmitter{
    constructor(){
        this.events={};
    }

    on(event,callback){
        if(this.events[event]===undefined){
            this.events[event]=[];
        }
        // Check to prevent adding duplicate callbacks
        if(this.events[event].includes(callback)){
            throw new Error("This callback has already been added to this event.")
        }
        this.events[event].push(callback);
    }

    once(event,callback){
        let onceCallback=(args)=>{
            callback(args)
            this.off(event,onceCallback);
        }
        this.on(event,onceCallback);
    }

    off(event,callback){
        if(this.events[event]===undefined){
            throw new Error(`The event '${event}' is not defined`)
        }
        let index=this.events[event].indexOf(callback);
        if(index===-1){
            throw new Error(`The callback is not defined for the event ${event}`)
        }
        this.events[event].splice(index,1);
    }

    emit(event,args){
        if(this.events[event]===undefined){
            throw new Error(`The event '${event}' is not defined`)
        }
        for(const callback of this.events[event]){
            callback(args);
        }
    }
}

/**
 * Each control flow element is defined as a separate class with a getNext method that is used to determine the next instruction to execute. It is necessary to implement execution this way, so that the next instruction can be called during a callback after a response from the user.
 */
class SequenceControlFlow{
    constructor(statement,interpreter){
        this.interpreter=interpreter;
        this.statement=statement;
        this.statements=statement.nodes;
        this.currentStatement=0;
    }

    getNext(){
        if(this.statements.length>this.currentStatement){
            let index=this.currentStatement;
            this.currentStatement+=1;
            return this.statements[index];
        }
        else{
            return "done";
        }
    }
}

class IfControlFlow{
    constructor(statement,isTrue){        
        this.statement=statement;
        this.isTrue=isTrue;
        this.statementExecuted=false;
    }

    /**
     * An if statement is executed once and then returns the control flow to the parent control element.
     */
    getNext(){
        if(!this.statementExecuted){
            this.statementExecuted=true;
            if(this.isTrue){
                return this.statement.ifTrue;
            }
            else{
                return "done";
            }
        }
        return "done";
    }
}

/**
 * A command interpreter takes a CommandConfig and uses it to collect input from the user. The command interpreter collects the input from the user by talking to presenter elements through synchronous request/response pattern. This is implemented in the browser using an event emitter.
 */
class CommandInterpreter{
    constructor(config){
        this.config=config;
        this.startNode=config;
        this.currentNode=config;
        this.controlFlowStack=[];
        this.currentControlNode=null;
        this.eventEmitter=new EventEmitter();
        this.variables={};
    }

    setVariable(varName,value){
        this.variables[varName]=value;
    }

    executeCurrentCommand(){        
        let currentCommand=this.currentNode.type;
        if(this[currentCommand]!==undefined){
            this[currentCommand]();
        }
        else{
            throw new Error("The command " + currentCommand + " is not supported by the interpreter");
        }
    }

    executeNextCommand(){
        this.currentNode=this.currentControlNode.getNext();
        if(this.currentNode==="done"){
            if(this.controlFlowStack.length>0){
                this.popControlFlowStack();
                this.executeNextCommand();
            }
        }
        else {
            this.executeCurrentCommand();
        }
    }

    clearVars(){
        this.variables={};
    }

    clearControlFlowStack(){
        this.controlFlowStack=[];
        this.currentControlNode=null;
    }

    reset(){
        this.currentNode=this.startNode;
        this.clearVars();
        this.clearControlFlowStack();
        /**
         * When listening for a response, the interpreter attaches a callback that is called once a response is sent. If the interpreter is reset before a response is sent the callback will still remain, which means the next the a response is sent it will call the wrong callback and cause bugs. Clearing the event emitter when reset is called fixes this problem. This also means that the event listeners must be reinitialized in the presenter when reset is called.
         */
        this.eventEmitter=new EventEmitter();
    }

    loadConfig(config){
        this.config=config;
        this.startNode=config;
        this.reset();
    }

    evalCondition(condition){
        if(condition.type==="Equals"){
            if(this.variables[condition.variable]===undefined){
                throw new Error(`The variable '${condition.variable}' has not be set`)
            }
            return this.variables[condition.variable]===condition.value;
        }
        else if(condition.type==="AND"){
            return this.evalCondition(condition.lhs) && this.evalCondition(condition.rhs);
        }
        else if(condition.type==="OR"){
            return this.evalCondition(condition.lhs) || this.evalCondition(condition.rhs);
        }
        else if(condition.type==="NOT"){
            return !this.evalCondition(condition.statement);
        }
        else{
            throw new Error(`The condition type ${condition.type} is not supported`);
        }
    }

    if(){
        let isTrue=this.evalCondition(this.currentNode.condition);
        this.currentControlNode=new IfControlFlow(this.currentNode,isTrue);
        this.controlFlowStack.push(this.currentControlNode);
        this.executeNextCommand();
    }

    sequence(){
        this.currentControlNode=new SequenceControlFlow(this.currentNode);
        this.controlFlowStack.push(this.currentControlNode);
        this.executeNextCommand();
    }

    /**
     * SetVar is the command called by interpretering the AST Node. It is also possible for the program to pass in variables directly to the interpreter by calling the setVariable method directly.
     */
    SetVar(){
        let {varName,value}=this.currentNode;
        this.setVariable(varName,value);
        this.executeNextCommand();
    }

    popControlFlowStack(){
        this.controlFlowStack.pop();
        if(this.controlFlowStack.length!=0){
            this.currentControlNode=this.controlFlowStack[this.controlFlowStack.length-1];
        }
    }

    KeyboardChoice(){
        let keyboardChoice=this.currentNode;
        let choices=keyboardChoice.choices.map((choice)=>{
            return {"text":choice.text,"key":choice.key,"value":choice.value}
        })
        this.sendRequest("KeyboardChoice",{"choices":choices,"noisewords":keyboardChoice.noisewords});
        let callback=(args)=>{
            this.setVariable(keyboardChoice.variableName,args["choice"]);            
            this.executeNextCommand();
        }
        this.listenForResponse("KeyboardChoice",callback);
    }

    SelectObject(){
        let {presenterType,variableName,selectedColor}=this.currentNode;
        this.sendRequest("SelectObject",{"presenterType":presenterType,"selectedColor":selectedColor});
        let callback=(args)=>{
            this.setVariable(variableName,args["nodeSelected"])
            this.executeNextCommand();
        }
        this.listenForResponse("SelectObject",callback);
    }

    ExecuteAction(){
        let params=[];
        if(this.currentNode.params){
            for(const variable of this.currentNode.params){
                if(this.variables[variable]===undefined){
                    throw new Error(`The parameter "${variable} is not defined"`);
                }
                params.push(this.variables[variable]);            
            }
        }
        this.sendRequest("ExecuteAction",{"actionName":this.currentNode.actionName,"params":params,"resultsVariable":this.currentNode.resultsVariable});
        this.executeNextCommand();
    }

    /**
     * The method just sends a "ResetInterpreter". The presenter listening to this event should reset the interpreter and re-initialize the events. This must be done in the presenter because calling reset destroys all of the callback events. If we call it here then the callback will be destroyed and we cannot re-initialize the callbacks in the Presenter. On the otherhand, we cannot call reset afterwards because then the callbacks that are re-initialized in the presenter will be detroyed. So, the command interpreter must be explicitly reset in the presenter before re-initializing the events.
     */
    ResetInterpreter(){
        this.sendRequest("ResetInterpreter");
    }

    sendRequest(commandName,args){
        this.eventEmitter.emit(`${commandName}-request`,args);
    }

    sendResponse(commandName,args){
        this.eventEmitter.emit(`${commandName}-response`,args);
    }

    listenForResponse(commandName,callback){
        this.eventEmitter.once(`${commandName}-response`,callback);
    }

    listenForRequest(commandName,callback){
        this.eventEmitter.on(`${commandName}-request`,callback);
    }
}
