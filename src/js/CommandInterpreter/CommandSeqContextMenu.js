/**
 * This Command Sequence Presenter is meant to be called from a context menu, instead of being the central interface of the program. It takes to config for a single command, which is initiated by clicking on a context menu item. It then executes a single command at the end.
 * 
 * It is driven by a CommandInterpreter. Elements are rendered by the presenter based on events raised by the CommandInterpeter.
 * 
 * This class is similar to the CommandSequencePresenter except it is driven by a CommandInterpreter instead of a CommandSequence.
 */
class CommandSeqContextMenu extends Presenters.Presenter{
    constructor(config){
        super();
        this.commandInterpreter=new CommandInterpreter();
        this._inner=null;
        this._onAction=null;
        this.loadConfig(config);
        this.initEvents();
        this.keypressListener=null;
    }

    loadConfig(config){
        this.commandInterpreter.loadConfig(config);
    }

    initEvents(){
        this.commandInterpreter.listenForRequest("KeyboardChoice",(args)=>{
            let dialog=new Presenters.KeyboardChoiceDialog(args.choices)
                .textToDisplay(args.noisewords)
                .onResponse((response)=>{
                    this.commandInterpreter.sendResponse("KeyboardChoice",{"choice":response});
                })
            this.display(dialog)
        })
        this.commandInterpreter.listenForRequest("ExecuteAction",({params})=>{
            this._onAction(...params);
        })
        this.commandInterpreter.listenForRequest("ResetInterpreter",()=>{
            if(this.isVisible){
                this.hide();
            }
            this.commandInterpreter.reset();
        })
    }

    onAction(callback){
        this._onAction=callback;
        return this;
    }

    setVariable(varName,value){
        this.commandInterpreter.setVariable(varName,value);
    }

    run(vars={}){
        this.commandInterpreter.reset();
        this.initEvents();
        for (const [key, value] of Object.entries(vars)){
            this.setVariable(key,value);
        }
        this.commandInterpreter.executeCurrentCommand();
    }

    definition(){
        return this.column([this.defInner()]);
    }

    /*
        The inner element is necessary in order to rerender the display without changing the position of the popup.
    */
    defInner(){
        this._inner=this.contentHolder()
        return this._inner;
    }

    display(presenter){
        this._inner.content=presenter;
        this._inner.focus();
    }

    _initJavascript(){
        this.keypressListener=createKeypress(this.htmlEl);
        this.keypressListener.register_many([
            {
                "keys":"esc",
                "is_exclusive":true,
                "on_keyup":(e)=>{                    
                    this.commandInterpreter.reset();
                    this.closePopup();
                }
            }
        ])
    }

    _destroyJavascript(){
        this.keypressListener.destroy();
        this.keypressListener=null;
    }
}

