"use strict";
GuiActions.StructuredDocumentActions={};

GuiActions.StructuredDocumentActions.insertText=function(dataNode,position){
    let newDataNode=new DataModels.ProseMirrorDataModel();
    dataActionDispatcher.execute("insertNode",dataNode,newDataNode,position);
    
    // The target node must be expanded to make sure that the child node is visible in the GUI
    if(["child","childLast","childFirst"].includes(position)){
        let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
        let documentNode=selectedWindow.findFirstChildByObjectId(dataNode.objectId);        
        documentNode.expanded=true;
    }
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());
    GuiActions.WikiActions.setSelectedNode(newDataNode);
}

GuiActions.StructuredDocumentActions.insertTextCommand=function(presenter){
    let sequencePresenter=wikiDesktop.sequencePresenter;
    sequencePresenter.loadConfig(new InsertTextContextMenuConfig().definition());
    sequencePresenter.onAction(position=>{
        GuiActions.StructuredDocumentActions.insertText(presenter.dataNode,position);
        sequencePresenter.closePopup()
    })
    let treeNode=new TreeNodeInterface(presenter.dataNode)
    // must call openAsPopup before run, so that we the dialog is displayed the keyboard events bind properly.
    sequencePresenter.openAsPopup(presenter);
    sequencePresenter.run({"hasChildren":treeNode.hasChildren(),"isTargetDisplayedAsRoot":GuiActions.StructuredDocumentActions.isNodeDisplayedAsRoot(presenter.dataNode)});
    
}

GuiActions.StructuredDocumentActions.insertClipboardText=function(targetPresenter,position){
    let textToInsert=clipboard.readText()
    let firstNodeInserted=dataActionDispatcher.execute("insertTextAtNode",textToInsert,targetPresenter.dataNode,position);
    
    // The target node must be expanded to make sure that the child node is visible in the GUI
    if(["child","childLast","childFirst"].includes(position)){
        let documentNode=targetPresenter.findFirstParentByType(Presenters.StructuredDocumentNodePresenter);
        documentNode.expanded=true;
    }
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());
    GuiActions.WikiActions.setSelectedNode(firstNodeInserted);
}

GuiActions.StructuredDocumentActions.insertClipboardTextCommand=function(presenter){
    let sequencePresenter=wikiDesktop.sequencePresenter;
    sequencePresenter.loadConfig(new PasteTextFromClipboardContextMenuConfig().definition());
    sequencePresenter.onAction(position=>{
        GuiActions.StructuredDocumentActions.insertClipboardText(presenter,position);
        sequencePresenter.closePopup()
    })
    let treeNode=new TreeNodeInterface(presenter.dataNode)
    sequencePresenter.openAsPopup(presenter);
    let targetText=presenter.dataNode.getTextContent();
    sequencePresenter.run({"hasChildren":treeNode.hasChildren(),"hasContent":targetText!=="","isTargetDisplayedAsRoot":GuiActions.StructuredDocumentActions.isNodeDisplayedAsRoot(presenter.dataNode)});
}

GuiActions.StructuredDocumentActions.insertClipboardHTML=function(targetPresenter,position){
    let textToInsert=clipboard.readText()
    let firstNodeInserted=dataActionDispatcher.execute("insertHTMLAtNode",textToInsert,targetPresenter.dataNode,position);
    
    // The target node must be expanded to make sure that the child node is visible in the GUI
    if(["child","childLast","childFirst"].includes(position)){
        let documentNode=targetPresenter.findFirstParentByType(Presenters.StructuredDocumentNodePresenter);
        documentNode.expanded=true;
    }
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());
    GuiActions.WikiActions.setSelectedNode(firstNodeInserted);
}

GuiActions.StructuredDocumentActions.insertClipboardHTMLCommand=function(presenter){
    let sequencePresenter=wikiDesktop.sequencePresenter;
    sequencePresenter.loadConfig(new PasteTextFromClipboardContextMenuConfig().definition());
    sequencePresenter.onAction(position=>{
        GuiActions.StructuredDocumentActions.insertClipboardHTML(presenter,position);
        sequencePresenter.closePopup()
    })
    let treeNode=new TreeNodeInterface(presenter.dataNode)
    sequencePresenter.openAsPopup(presenter);
    let targetText=presenter.dataNode.getTextContent();
    sequencePresenter.run({"hasChildren":treeNode.hasChildren(),"hasContent":targetText!=="","isTargetDisplayedAsRoot":GuiActions.StructuredDocumentActions.isNodeDisplayedAsRoot(presenter.dataNode)});
}

GuiActions.StructuredDocumentActions.indent=function(presenter){
    let newParent,src;    
    if(wikiDesktop.selectedRange.isEmpty()){ 
        src=presenter.dataNode;
        let target=new TreeNodeInterface(presenter.dataNode).getPrevious();
        if(target===null){
            return;
        }
        dataActionDispatcher.execute("moveBranch",src,target,"child");
        newParent=target;
    }
    else{
        let [startDataNode,]=wikiDesktop.selectedRange.getRangeByDocumentOrder();
        let target=new TreeNodeInterface(startDataNode).getPrevious();
        newParent=target;
        if(target===null){
            return;
        }
        let srcList=wikiDesktop.getPresentersInRange().map(presenter=>presenter.dataNode);
        dataActionDispatcher.execute("moveRange",srcList,target,"child")
        src=srcList.slice(-1)[0];
    }
    
    let document=wikiDesktop.getDocumentPresenter(presenter);
    let newParentPresenter=document.findFirstChildByObjectId(newParent.objectId);
    newParentPresenter.expanded=true;
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());
    GuiActions.WikiActions.setSelectedNode(src,false);
}

GuiActions.StructuredDocumentActions.deIndent=function(presenter){
    let srcDataNode=presenter.dataNode
    let targetDataNode=new TreeNodeInterface(srcDataNode).parent
    // deIndent should do nothing if the parent node is displayed as root, because the nodes will be moved off screen, which is not desirable behavior.
    if(GuiActions.StructuredDocumentActions.isNodeDisplayedAsRoot(targetDataNode)){
        return;
    }

    let document=wikiDesktop.getDocumentPresenter(presenter);
    // you cannot deindent nodes at the top level of a document.
    if(targetDataNode===document.dataNode){
        return
    } else{
        if(wikiDesktop.selectedRange.isEmpty()){
            dataActionDispatcher.execute("moveBranch",srcDataNode,targetDataNode,"next")
        }
        else{            
            let target=targetDataNode
            let srcList=wikiDesktop.getPresentersInRange().map(presenter=>presenter.dataNode);
            dataActionDispatcher.execute("moveRange",srcList,target,"next");
            srcDataNode=srcList.slice(-1)[0];
        } 
        wikiDesktop.topWindowPanel.windowManager
            .getAllWindows()
            .forEach(window=>window.refresh());
        GuiActions.WikiActions.setSelectedNode(srcDataNode,false);
    }
}

GuiActions.StructuredDocumentActions.getPreviousPresenter=function(presenter){    
    let document=wikiDesktop.getDocumentPresenter(presenter); 
    let srcTreeNode=new TreeNodeInterface(presenter.dataNode);
    let previous=srcTreeNode.getPrevious();
    
    if(previous!==null){
        let previousPresenter=document.findFirstChildByObjectId(previous.objectId);
        let previousTreeNode=new TreeNodeInterface(previous);
        // If the previous node has children and they are visible, select the child. Repeat until the selected not has no visible children.
        while(previousTreeNode.hasChildren() && previousPresenter.expanded===true){
            previous=previousTreeNode.children[previousTreeNode.children.length-1];
            previousTreeNode=new TreeNodeInterface(previous)
            previousPresenter=document.findFirstChildByObjectId(previous.objectId);
        }
    }
    if(previous===null){
        previous=srcTreeNode.parent;
    }
    if(previous===document.dataNode){
        previous=null;
    }
    if(previous!==null){
        previous=document.findFirstChildByObjectId(previous.objectId);
    }
    return previous;
}

/*
    The getSuccessorPresenter function gets the next node that is on the same level as the current presenter. It does search the child nodes, like the getNextPresenter function.
*/
GuiActions.StructuredDocumentActions.getSuccessorPresenter=function(presenter){
    let srcTreeNode=new TreeNodeInterface(presenter.dataNode);  
    let next=null;
    next=srcTreeNode.getNext();
    if(next!==null){
        let document=wikiDesktop.getDocumentPresenter(presenter); 
        next=document.findFirstChildByObjectId(next.objectId);
    }
    return next
}

GuiActions.StructuredDocumentActions.getNextPresenter=function(presenter){
    let srcTreeNode=new TreeNodeInterface(presenter.dataNode);  
    let next=null;
    let presenterDocumentNode=presenter.findFirstParentByType(Presenters.StructuredDocumentNodePresenter)
    
    // handle the case where the presenter has a visible child node. The first child node is selected in this case
    if(srcTreeNode.hasChildren() && presenterDocumentNode.expanded===true){
        next=srcTreeNode.children[0];    
    }   
    else{
        next=srcTreeNode.getNext();
        
        // Handle the case where the presenter is the last child in a branch. In that case search all the ancestors for the nearest level with a next node.
        if(next===null){
            let document=wikiDesktop.getDocumentPresenter(presenter); 
            let ancestor=srcTreeNode.parent
            next=null;
            while(next===null && ancestor!==document.dataNode){
                next=new TreeNodeInterface(ancestor).getNext();
                ancestor=new TreeNodeInterface(ancestor).parent;
            }
        }       
    }
    if(next!==null){
        let document=wikiDesktop.getDocumentPresenter(presenter);
        next=document.findFirstChildByObjectId(next.objectId);
    }
    return next
}

GuiActions.StructuredDocumentActions.selectPrevious=function(presenter){
    let previous=GuiActions.StructuredDocumentActions.getPreviousPresenter(presenter);
    if(previous!==null){
        GuiActions.WikiActions.setSelectedNode(previous.dataNode,false);
    }
}

GuiActions.StructuredDocumentActions.selectNext=function(presenter){    
    let next=GuiActions.StructuredDocumentActions.getNextPresenter(presenter);
    if(next!==null){        
        GuiActions.WikiActions.setSelectedNode(next.dataNode,false)
    }
}

GuiActions.StructuredDocumentActions.moveToPrevious=function(presenter){
    let srcTreeNode,previous
    if(wikiDesktop.selectedRange.isEmpty()){
        srcTreeNode=new TreeNodeInterface(presenter.dataNode);
        previous=srcTreeNode.getPrevious()
        if(previous===null){
            return;
        }
        dataActionDispatcher.execute("moveBranch",presenter.dataNode,previous,"before");
    }
    else{
        let [startDataNode,]=wikiDesktop.selectedRange.getRangeByDocumentOrder();
        srcTreeNode=new TreeNodeInterface(startDataNode);
        previous=srcTreeNode.getPrevious();
        if(previous===null){
            return;
        }
        let srcList=wikiDesktop.getPresentersInRange().map(presenter=>presenter.dataNode);
        dataActionDispatcher.execute("moveRange",srcList,previous,"before");
    }
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());
    GuiActions.WikiActions.setSelectedNode(presenter.dataNode,false);
}

GuiActions.StructuredDocumentActions.moveToNext=function(presenter){
    let srcTreeNode,next
    if(wikiDesktop.selectedRange.isEmpty()){
        srcTreeNode=new TreeNodeInterface(presenter.dataNode);
        next=srcTreeNode.getNext();
        if(next===null){
            return;
        }
        dataActionDispatcher.execute("moveBranch",presenter.dataNode,next,"next");
    }
    else{
        let [,endDataNode]=wikiDesktop.selectedRange.getRangeByDocumentOrder();
        srcTreeNode=new TreeNodeInterface(endDataNode);
        next=srcTreeNode.getNext();
        if(next===null){
            return;
        }
        let srcList=wikiDesktop.getPresentersInRange().map(presenter=>presenter.dataNode);
        dataActionDispatcher.execute("moveRange",srcList,next,"next");
    }
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());
    GuiActions.WikiActions.setSelectedNode(presenter.dataNode,false);
}

GuiActions.StructuredDocumentActions.delete=function(presenter){
    let document=wikiDesktop.getDocumentPresenter(presenter);
    // When a node is selected through jump to element and you delete the node selected, an error is raised when calling refresh because the node to render no longer exists. To prevent this, jump up should be called before deleting the selected node.
    if(!document.isSelectedNodeTopLevel() && presenter.dataNode==document.rootDisplayNode){
        GuiActions.StructuredDocumentActions.jumpUp();
    }
    let nodeToSelect=null;
    // nodeToSelect is presenter to focus on after deleting the current presenter
    nodeToSelect=GuiActions.StructuredDocumentActions.getPreviousPresenter(presenter);  
    if(nodeToSelect===null){
        nodeToSelect=GuiActions.StructuredDocumentActions.getSuccessorPresenter(presenter);
    }

    
    dataActionDispatcher.execute("deleteNode",presenter.dataNode);    
    
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());
    // If nodeToSelect is null this means the user has deleted all the nodes in the document
    if(nodeToSelect!==null){
        GuiActions.WikiActions.setSelectedNode(nodeToSelect.dataNode);
    }
}

GuiActions.StructuredDocumentActions.deleteSelectedRange=function(){
    let[startDataNode,endDataNode]=wikiDesktop.selectedRange.getRangeByDocumentOrder();
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let startPresenter=selectedWindow.findFirstChildByObjectId(startDataNode.objectId)
    let endPresenter=selectedWindow.findFirstChildByObjectId(endDataNode.objectId);
    let nodeToSelect=null;
    nodeToSelect=GuiActions.StructuredDocumentActions.getPreviousPresenter(startPresenter);  
    if(nodeToSelect===null){
        nodeToSelect=GuiActions.StructuredDocumentActions.getSuccessorPresenter(endPresenter);
    }
    let nodeList=wikiDesktop.getPresentersInRange().map(presenter=>presenter.dataNode);
    dataActionDispatcher.execute("deleteRange",nodeList);
    
    wikiDesktop.selectedRange.clearRange();
    
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());

    // If nodeToSelect is null this means the user has deleted all the nodes in the document.
    if(nodeToSelect!==null){
        GuiActions.WikiActions.setSelectedNode(nodeToSelect.dataNode);
    }
}

GuiActions.StructuredDocumentActions.pasteBranch=function(presenter,position){
    let srcNodes=wikiDesktop.clipboardObjects.map(objectId=>{
        return wikiDesktop.wikiData.propertyGraph.getNodeById(objectId)
    })
    let destNode=presenter.dataNode
    let documentNode=presenter.findFirstParentByType(Presenters.StructuredDocumentNodePresenter);
    if(position.startsWith("child")){
        documentNode.expanded=true;
    }

    let newNodes;
    if(wikiDesktop.copyClipboardObject){
        newNodes=dataActionDispatcher.execute("copyRange",srcNodes,destNode,position);
    }
    else{
        dataActionDispatcher.execute("moveRange",srcNodes,destNode,position);
        newNodes=srcNodes;
       
    }
    
    wikiDesktop.clearClipboard()
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());            
    if(newNodes.length===1){
        GuiActions.WikiActions.setSelectedNode(newNodes[0]);
    } else{
        GuiActions.WikiActions.setSelectedRange(newNodes[0],newNodes[newNodes.length-1]);
    }
}

/**
 * When plain text is copied to the clipboard multiple nodes are separated by newline characters. This check is necessary because we do not want to paste text that contains multiple nodes into a single node.
 */
GuiActions.StructuredDocumentActions.doesTextContainMultipleNodes=function(text){
    return text.match(/\r?\n/);
}

GuiActions.StructuredDocumentActions.doesHTMLContainMultipleNodes=function(htmlString){
    let wrapper=document.createElement("div");
    wrapper.innerHTML=htmlString;
    if(wrapper.childNodes.length===1){
        return false;
    }
    else{
        return true;
    }
}

GuiActions.StructuredDocumentActions.isStringHTML=function(text){
    // HTML starts and ends with a tag, while plain text does not.
    return text.startsWith("<") && text.endsWith(">")
}

GuiActions.StructuredDocumentActions.openTextRangeContextMenu=function(textPresenter){
    let contextChoices=[]
    let clipboardContents=clipboard.readText()
    if (GuiActions.StructuredDocumentActions.isStringHTML(clipboardContents)){
        if(!GuiActions.StructuredDocumentActions.doesHTMLContainMultipleNodes(clipboardContents)){
            contextChoices.push(
                {"text":"paste html from clipboard",callback:()=>{
                    wikiDesktop.contextMenu.closePopup();
                    let slice=DataModels.ProseMirrorDataModel.convertHTMLStringToProseMirrorSlice(clipboardContents);
                    textPresenter.textEditorEl.insertSliceOverSelection(slice);
                    
                }}
            )
        }
    }
    else{
        if(clipboardContents!==""){
            if(!GuiActions.StructuredDocumentActions.doesTextContainMultipleNodes(clipboardContents)){
                contextChoices.push(
                    {"text":"paste text from clipboard",callback:()=>{
                        wikiDesktop.contextMenu.closePopup();
                        textPresenter.textEditorEl.insertTextOverSelection(clipboardContents);
                    }}
                )
            }
        }
    }
    

    contextChoices.push(
        { 
            text:"bold text",callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                textPresenter.textEditorEl.toggleBold();
            }
        },
        {
            text:"copy to clipboard", callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                let content=textPresenter.textEditorEl.getSelectionContent();
                let htmlString=DataModels.ProseMirrorDataModel.convertContentToHTMLString(content);
                clipboard.writeText(htmlString);
            }
        },
        {
            text:"copy to clipboard as plain text",callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                let text=textPresenter.textEditorEl.getSelectedTextAsPlainText();
                clipboard.writeText(text);
            }
        }

    )
    
    if(!textPresenter.textEditorEl.isLinkSelected() && !textPresenter.textEditorEl.isInternalLinkSelected()){
        contextChoices.push(
            {
                text:"add link",callback:()=>{
                    wikiDesktop.contextMenu.closePopup();
                    GuiActions.StructuredDocumentActions.addLink(textPresenter)
                }
            },
            {
                text:"add internalLink", callback:()=>{
                    wikiDesktop.contextMenu.closePopup();
                    GuiActions.StructuredDocumentActions.addLinkInternal(textPresenter)
                }
            }
        )
    }
    
    wikiDesktop.contextMenu.choices(contextChoices);
    
    let textRange=textPresenter.textEditorEl.getSelectionRange();
    let onOpenCallback=()=>textPresenter.textEditorEl.highlightRange(textRange);
    let onCloseCallback=()=>textPresenter.textEditorEl.unhighlightRange(textRange);
    wikiDesktop.contextMenu.openAsPopupWithCallbacks(textPresenter.textEditorEl.getSelectionOffset(),onOpenCallback,onCloseCallback);
};

GuiActions.StructuredDocumentActions.openLinkMenu=function(textPresenter){
    wikiDesktop.contextMenu.choices([
        {text:"",callback:()=>{}},
        {
            text:"open link",callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                GuiActions.StructuredDocumentActions.openLink(textPresenter);
            }
        },
        {
            text:"edit link",callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                GuiActions.StructuredDocumentActions.editLink(textPresenter);
            }
        },
        {
            text:"delete link",callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                GuiActions.StructuredDocumentActions.deleteLink(textPresenter);
            }
        }
    ]);
    let linkRange=textPresenter.textEditorEl.getSelectedLinkIndexRange();
    let onOpenCallback=()=>textPresenter.textEditorEl.highlightRange(linkRange);
    let onCloseCallback=()=>textPresenter.textEditorEl.unhighlightRange(linkRange);
    wikiDesktop.contextMenu.openAsPopupWithCallbacks(textPresenter.textEditorEl.getSelectionOffset(),onOpenCallback,onCloseCallback);
};

GuiActions.StructuredDocumentActions.addLink=function(textPresenter){
    let inputForm=new InputFormPresenters.inputForm()
    inputForm.addField(new InputFormPresenters.displayTextField("enter link url"))
    inputForm.addField(new InputFormPresenters.stringLineField("linkUrl"))
    wikiDesktop.openFormInPopupPanel(inputForm,
        (inputData)=>{
            textPresenter.textEditorEl.addLink(inputData["linkUrl"]);
            wikiDesktop.popupPanel.closePopup();
        }
    ,textPresenter.textEditorEl.getSelectionOffset())
}

GuiActions.StructuredDocumentActions.openLink=function(textPresenter){
    let [linkURL,]=textPresenter.textEditorEl.getSelectedLinkData();
    shell.openExternal(linkURL);
};

GuiActions.StructuredDocumentActions.editLink=function(textPresenter){
    let [linkURL,linkText]=textPresenter.textEditorEl.getSelectedLinkData();
    let [startIndex,endIndex]=textPresenter.textEditorEl.getSelectedLinkIndexRange();
    let inputForm=new InputFormPresenters.inputForm()
    inputForm.addField(new InputFormPresenters.displayTextField("enter link url"))
    inputForm.addField(new InputFormPresenters.stringLineField("linkUrl",linkURL))
    wikiDesktop.openFormInPopupPanel(inputForm,
        (inputData)=>{
            textPresenter.textEditorEl.editLink(startIndex,endIndex,inputData["linkUrl"]);
            wikiDesktop.popupPanel.closePopup();
        }
    ,textPresenter.textEditorEl.getSelectionOffset())
};

GuiActions.StructuredDocumentActions.deleteLink=function(textPresenter){
    let [startIndex,endIndex]=textPresenter.textEditorEl.getSelectedLinkIndexRange();
    textPresenter.textEditorEl.removeLink(startIndex,endIndex);
}

GuiActions.StructuredDocumentActions.openInternalLinkMenu=function(textPresenter){
    wikiDesktop.contextMenu.choices([
        {
            text:"jump to link",callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                GuiActions.StructuredDocumentActions.openInternalLink(textPresenter);
            }
        },
        {
            text:"delete link", callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                GuiActions.StructuredDocumentActions.deleteInternalLink(textPresenter);
            }
        }
    ]);
    let linkRange=textPresenter.textEditorEl.getSelectedLinkIndexRange();
    let onOpenCallback=()=>textPresenter.textEditorEl.highlightRange(linkRange);
    let onCloseCallback=()=>textPresenter.textEditorEl.unhighlightRange(linkRange);
    wikiDesktop.contextMenu.openAsPopupWithCallbacks(textPresenter.textEditorEl.getSelectionOffset(),onOpenCallback,onCloseCallback);
};

GuiActions.StructuredDocumentActions.addLinkInternal=function(textPresenter){
    let inputForm=new Presenters.SelectAddressPresenter();
    wikiDesktop.popupPanel.setContent(inputForm);
    wikiDesktop.popupPanel.openAsPopup(textPresenter.textEditorEl.getSelectionOffset());
    inputForm.onSelectNode((selectResponse)=>{
        let srcNode=textPresenter.dataNode;
        let targetNode=selectResponse.node;
        let linkEdge=wikiDesktop.linksDBInterface.addLink(srcNode,targetNode)
        textPresenter.textEditorEl.addLinkInternal(selectResponse.node.objectId,linkEdge.edgeId);
        wikiDesktop.popupPanel.closePopup();
    });
    inputForm.onCancel(()=>wikiDesktop.popupPanel.closePopup());
}

GuiActions.StructuredDocumentActions.openInternalLink=function(textPresenter){
    let [objectId,,]=textPresenter.textEditorEl.getSelectedInternalLinkData();
    let node=wikiDesktop.wikiData.propertyGraph.getNodeById(objectId);
    let inputForm= new Presenters.KeyboardChoiceDialog([
        {"key":"l","text":"Left","value":"left"},
        {"key":"r","text":"Right","value":"right"}
    ]).textToDisplay("Which window do you want to open the link in?")
    wikiDesktop.popupPanel.setContent(inputForm);
    wikiDesktop.popupPanel.openAsPopup(textPresenter.textEditorEl.getSelectionOffset());
    inputForm.onResponse((value)=>{
        let windowManager=wikiDesktop.topWindowPanel.windowManager;
        let window=windowManager.getWindowById(value);
        if(!window.isVisible()){
            window.show();
            windowManager.refresh();
        }
        GuiActions.WikiActions.selectWindow(value);
        GuiActions.StructuredDocumentActions.jumpToElement(node);
        wikiDesktop.popupPanel.closePopup();
    })
};

GuiActions.StructuredDocumentActions.deleteInternalLink=function(textPresenter){
    let [,edgeId,]=textPresenter.textEditorEl.getSelectedInternalLinkData();
    let edge=wikiDesktop.wikiData.propertyGraph.getEdgeById(edgeId);
    wikiDesktop.linksDBInterface.deleteLink(edge);
    let [startIndex,endIndex]=textPresenter.textEditorEl.getSelectedLinkIndexRange();
    textPresenter.textEditorEl.removeInternalLink(startIndex,endIndex);
}

/*
    SplitText splits the currently selected text node at the current user position and puts the text in a new text node following the current one on the same level. The children of the current node are not affected.
*/
GuiActions.StructuredDocumentActions.splitText=function(textPresenter){
    // splitting multiple text nodes at the same time does not make sense, so raise an error when the user attempts to do so.
    if(!wikiDesktop.selectedRange.isEmpty()){
        wikiDesktop.messageDisplay.displayErrorMessage("cannot split text when a range is selected");
        return
    }
    let newDataNode=dataActionDispatcher.execute("splitText",textPresenter)
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());
    GuiActions.WikiActions.setSelectedNode(newDataNode);
}

GuiActions.StructuredDocumentActions.ConfirmAndDelete=function(textPresenter){
    wikiDesktop.confirmationDialog
        .textToDisplay("Delete the selected node and its children?")
        .onResponse(response=>{
            if(response==="yes"){
                GuiActions.StructuredDocumentActions.delete(textPresenter);
            }
            
            if(response==="no"){
                textPresenter.unhighlight();
            }
            wikiDesktop.confirmationDialog.closePopup()
        });
    wikiDesktop.confirmationDialog.openAsPopup(textPresenter);
    // must call highlight after opening the confirmation dialog in order to be able to calculate the offset of the text presenter properly.
    textPresenter.highlight();
}

GuiActions.StructuredDocumentActions.ConfirmAndDeleteSelectedRange=function(){
    let[startDataNode,]=wikiDesktop.selectedRange.getRangeByOrderSelected();
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let startPresenter=selectedWindow.findFirstChildByObjectId(startDataNode.objectId)
    
    wikiDesktop.confirmationDialog
        .textToDisplay("Delete the selected range of nodes and their children?")
        .onResponse(response=>{
            if(response==="yes"){
                GuiActions.StructuredDocumentActions.deleteSelectedRange();
            }
            
            if(response==="no"){
                GuiActions.WikiActions.unhighlightSelectedRange();
            }
            wikiDesktop.confirmationDialog.closePopup()
        });
    wikiDesktop.confirmationDialog.openAsPopup(startPresenter);
    // must call highlight after opening the confirmation dialog in order to be able to calculate the offset of the text presenter properly.
    GuiActions.WikiActions.highlightSelectedRange();
}

GuiActions.StructuredDocumentActions.collapseTextPresenter=function(presenter){
    let documentNode=presenter.findFirstParentByType(Presenters.StructuredDocumentNodePresenter);
    documentNode.expanded=false;
    documentNode.refresh(); 
    GuiActions.WikiActions.setSelectedNode(presenter.dataNode);
}

GuiActions.StructuredDocumentActions.expandPresenter=function(presenter){
    let documentNode
    if(!(presenter instanceof Presenters.StructuredDocumentNodePresenter)){
        documentNode=presenter.findFirstParentByType(Presenters.StructuredDocumentNodePresenter);
    }
    else{
        documentNode=presenter;
    }
    documentNode.expanded=true;
    documentNode.refresh();
    GuiActions.WikiActions.setSelectedNode(presenter.dataNode);
}

GuiActions.StructuredDocumentActions.openTextNodeContextMenu=function(presenter){
    let contextChoices=[]
    contextChoices.push(...GuiActions.WikiActions.getWindowMenuCommands());
    let clipboardContents=clipboard.readText();

    if (GuiActions.StructuredDocumentActions.isStringHTML(clipboardContents) && wikiDesktop.selectedRange.isEmpty()){
        if(GuiActions.StructuredDocumentActions.doesHTMLContainMultipleNodes(clipboardContents)){
            contextChoices.push(
                {"text":"paste html from clipboard (multiple nodes)",callback:()=>{
                    wikiDesktop.contextMenu.closePopup();
                    GuiActions.StructuredDocumentActions.insertClipboardHTMLCommand(presenter);
                }}
            )
        }
        else{
            contextChoices.push(
                {"text":"paste html from clipboard",callback:()=>{
                    wikiDesktop.contextMenu.closePopup();
                    let slice=DataModels.ProseMirrorDataModel.convertHTMLStringToProseMirrorSlice(clipboardContents);
                    presenter.textEditorEl.insertSliceOverSelection(slice);
                }}
            )
        }
    }
    else{
        
        if(clipboardContents!=="" && wikiDesktop.selectedRange.isEmpty()){
            if(GuiActions.StructuredDocumentActions.doesTextContainMultipleNodes(clipboardContents)){
                contextChoices.push(
                    {"text":"paste text from clipboard (multiple lines)",callback:()=>{
                        wikiDesktop.contextMenu.closePopup();
                        GuiActions.StructuredDocumentActions.insertClipboardTextCommand(presenter);
                    }}
                )
            }
            else{
                contextChoices.push(
                    {"text":"paste text from clipboard",callback:()=>{
                        wikiDesktop.contextMenu.closePopup();
                        presenter.textEditorEl.insertTextOverSelection(clipboardContents);
                    }}
                )
            }
        }
    }

    contextChoices.push(
        {"text":"insertTextNode",callback:()=>{
            wikiDesktop.contextMenu.closePopup();
            GuiActions.StructuredDocumentActions.insertTextCommand(presenter);
        }},
        {"text":"copyBranch", callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                if(!wikiDesktop.selectedRange.isEmpty()){
                    wikiDesktop.clearClipboard()
                    for(let selectedPresenter of wikiDesktop.getPresentersInRange()){
                        wikiDesktop.clipboardObjects.push(selectedPresenter.dataNode.objectId);
                    }
                }
                else{
                    wikiDesktop.clipboardObjects=[presenter.dataNode.objectId];
                }

                wikiDesktop.copyClipboardObject=true;
                wikiDesktop.messageDisplay.displaySuccessMessage("object copied to the clipboard");
                setTimeout(function(){
                    wikiDesktop.messageDisplay.clearMessage();
                },1000);
                wikiDesktop.topWindowPanel.windowManager
                    .getAllWindows()
                    .forEach(window=>window.refresh());
            }
           
        },
        {"text":"copyBranch To Clipboard as Plain Text", callback:()=>{                
                wikiDesktop.contextMenu.closePopup();
                let nodesToCopy=[]
                if(!wikiDesktop.selectedRange.isEmpty()){
                    for(let selectedPresenter of wikiDesktop.getPresentersInRange()){
                        nodesToCopy.push(selectedPresenter.dataNode);
                    }
                }
                else{
                    nodesToCopy.push(presenter.dataNode);
                }

                clipboard.writeText(io.nodesToText(...nodesToCopy))
                wikiDesktop.messageDisplay.displaySuccessMessage("branch copied to the clipboard as plain text");
                setTimeout(function(){
                    wikiDesktop.messageDisplay.clearMessage();
                },1000);
            }
        },
        {"text":"copyBranch To Clipboard", callback:()=>{                
                wikiDesktop.contextMenu.closePopup();
                let nodesToCopy=[]
                if(!wikiDesktop.selectedRange.isEmpty()){
                    for(let selectedPresenter of wikiDesktop.getPresentersInRange()){
                        nodesToCopy.push(selectedPresenter.dataNode);
                    }
                }
                else{
                    nodesToCopy.push(presenter.dataNode);
                }

                let htmlString=io.nodesToHTML(...nodesToCopy);
                clipboard.writeText(htmlString);
                wikiDesktop.messageDisplay.displaySuccessMessage("branch copied to the clipboard");
                setTimeout(function(){
                    wikiDesktop.messageDisplay.clearMessage();
                },1000);
            }
        },
        {"text":"cutBranch", callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                if(!wikiDesktop.selectedRange.isEmpty()){
                    wikiDesktop.clearClipboard()
                    for(let selectedPresenter of wikiDesktop.getPresentersInRange()){
                        wikiDesktop.clipboardObjects.push(selectedPresenter.dataNode.objectId);
                    }
                }
                else{
                    wikiDesktop.clipboardObjects=[presenter.dataNode.objectId];
                }
                wikiDesktop.copyClipboardObject=false;
                wikiDesktop.messageDisplay.displaySuccessMessage("object cut to the clipboard");
                setTimeout(function(){
                    wikiDesktop.messageDisplay.clearMessage();
                },1000);
                wikiDesktop.topWindowPanel.windowManager
                    .getAllWindows()
                    .forEach(window=>window.refresh());
            }
        },
        {"text":"deleteBranch",callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                if(!wikiDesktop.selectedRange.isEmpty()){
                    GuiActions.StructuredDocumentActions.ConfirmAndDeleteSelectedRange();
                }
                else{
                    GuiActions.StructuredDocumentActions.ConfirmAndDelete(presenter);
                }
            }
        }
    )

    if(wikiDesktop.selectedRange.isEmpty()){
        contextChoices.push(
            {"text":"Jump to Node", callback:()=>{
                    wikiDesktop.contextMenu.closePopup();
                    GuiActions.StructuredDocumentActions.jumpToElement(presenter.dataNode);
                }
            },
            {"text":"split text", callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                GuiActions.StructuredDocumentActions.splitText(presenter);
            }
        }
        )
    }
    
    if(wikiDesktop.clipboardObjects.length>0){
        contextChoices.push(
            {"text":"clear clipboard", callback:()=>{
                    wikiDesktop.contextMenu.closePopup();
                    GuiActions.StructuredDocumentActions.clearClipboard()
                }
            }
        )
        
        let addPasteOptions=true;
        
        /*
            The user should not be able to cut or copy and paste objects to the same position, nor should they be able to cut and paste an object within the same selected range. This also includes any of the children of the selected nodes.
        */
        for (let clipboardObjectId of wikiDesktop.clipboardObjects){
            if(clipboardObjectId===presenter.dataNode.objectId){
                addPasteOptions=false;
            }
            let parentNode=new TreeNodeInterface(presenter.dataNode).parent;
            while(parentNode!==null){
                if(clipboardObjectId===parentNode.objectId){
                    addPasteOptions=false;
                }
                parentNode=new TreeNodeInterface(parentNode).parent
            }
        }
        
        if(addPasteOptions===true){
            contextChoices.push(
                {"text":"pasteBranch", callback:()=>{                    
                    wikiDesktop.contextMenu.closePopup();
                    let sequencePresenter=wikiDesktop.sequencePresenter;
                    sequencePresenter.loadConfig(new PasteTextFromClipboardContextMenuConfig().definition());
                    sequencePresenter.onAction(position=>{
                        GuiActions.StructuredDocumentActions.pasteBranch(presenter,position);
                        sequencePresenter.closePopup()
                    })
                    let treeNode=new TreeNodeInterface(presenter.dataNode)
                    sequencePresenter.openAsPopup(presenter);
                    let targetText=presenter.dataNode.getTextContent();
                    sequencePresenter.run({"hasChildren":treeNode.hasChildren(),"hasContent":targetText!=="","isTargetDisplayedAsRoot":GuiActions.StructuredDocumentActions.isNodeDisplayedAsRoot(presenter.dataNode)});
                }}
            )
        }
    }
    
    wikiDesktop.contextMenu.choices(contextChoices)
    if(wikiDesktop.selectedRange.isEmpty()){
        let onOpenCallback=()=>presenter.highlight();
        let onCloseCallback=()=>presenter.unhighlight();
        wikiDesktop.contextMenu.openAsPopupWithCallbacks(presenter,onOpenCallback,onCloseCallback);
    }
    else{
        wikiDesktop.contextMenu.openAsPopup(presenter);
    }
}

GuiActions.StructuredDocumentActions.clearClipboard=function(){
    let clipboardObjects=wikiDesktop.clipboardObjects;
    wikiDesktop.clearClipboard()
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh());
}

/**
 * This method is called from the ProseMirrorEditor every time there is a content change in the document. It is responsible for the follow:
 *  - Update the database with the appropriate changes
 *  - Update the other TextPresenters where the data is being displayed
 * 
 * 
 * @param dataNode - The dataNode associate with the editor. This is the node in the database to update.
 * @param newState - The new content of the dataNode, which is a prosemirror document. 
 * @param srcEditor - The editor element where the presenter originated from.
 */
GuiActions.StructuredDocumentActions.updateTextState=function(dataNode,newState,srcEditor){
    dataActionDispatcher.execute("updateProseMirrorData",dataNode,newState);
    let presenters=wikiDesktop.topWindowPanel.findAllChildrenByObjectId(dataNode.objectId);
    for(const presenter of presenters){
        // DataNodes are contained both in the Presenter and its enclosing StructuredDocumentNodePresenter. This will be fixed eventually.
        if(presenter instanceof Presenters.StructuredDocumentNodePresenter){
            continue;
        }
        // Do not refresh the source presenter, this has already been updated by a ProseMirrorTransaction in a way that preserves the current selection while typing.
        else if(presenter.textEditorEl===srcEditor){
            continue;
        }
        else{
            presenter.refresh();
        }
    }
}

/**
 * This function is called by the CommandInterpreter through the action dispatcher. Given a node it determines whether the node has a child data node.
 */
GuiActions.StructuredDocumentActions.nodeHasChildren=function(node){
    return new TreeNodeInterface(node).hasChildren();
}

GuiActions.StructuredDocumentActions.isNodeDisplayedAsRoot=function(node){
    let docPresenter=wikiDesktop.getDocumentPresenter(node);
    // The docPresenter can be null when the node you are attempting to get the document presenter for is not displayed on the screen in either window.
    if (docPresenter===null){
        return false;
    }
    return docPresenter.rootDisplayNode===node;
}

/**
 * Jump to an element in the selected window and add it to the jump history. 
 * @param {PropertyGraph.Node} node - The node to jump to
 */
GuiActions.StructuredDocumentActions.jumpToElement=function(node){
    let docNode;
    if(node instanceof DataModels.StructuredDocumentDataModel){
        docNode=node;
    }
    else{
        docNode=new TreeNodeInterface(node).findParentByType(DataModels.StructuredDocumentDataModel)
    }
    let nodeFileName=docNode.getFile().fileName;
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    if(selectedWindow.windowName!=nodeFileName){
        GuiActions.WikiActions.selectFile(nodeFileName);
    }
    let doc=selectedWindow.findFirstChildByType(Presenters.StructuredDocumentPresenter);
    GuiActions.StructuredDocumentActions.jumpToNode(doc,node);
    GuiActions.StructuredDocumentActions.addToJumpHistory(node,doc.dataNode);
}

/**
 * Jump to a node given a document presenter and a node. This is different from jumpToElement because it does not record the history. It is called by jumpToElement.
 * @param {Presenters.StructuredDocumentPresenter} doc 
 * @param {PropertyGraphModel.Node} node 
 */
GuiActions.StructuredDocumentActions.jumpToNode=function(doc,node){
    doc.rootDisplayNode=node;
    doc.refresh();
}

GuiActions.StructuredDocumentActions.jumpBack=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let nav=wikiDesktop.jumpHistory[selectedWindow.getWindowId()];
    let histObj=nav.goBack();
    if(histObj!==null){
        GuiActions.WikiActions.selectFile(histObj.getFileName());
        let doc=selectedWindow.findFirstChildByType(Presenters.StructuredDocumentPresenter);
        GuiActions.StructuredDocumentActions.jumpToNode(doc,histObj.selectedRootObj);
    }
}

GuiActions.StructuredDocumentActions.jumpForward=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let nav=wikiDesktop.jumpHistory[selectedWindow.getWindowId()];
    let histObj=nav.goForward();
    if(histObj!==null){
        GuiActions.WikiActions.selectFile(histObj.getFileName());
        let doc=selectedWindow.findFirstChildByType(Presenters.StructuredDocumentPresenter);
        GuiActions.StructuredDocumentActions.jumpToNode(doc,histObj.selectedRootObj);
    }
}

/**
 * Adds a jumpHistory object to the jumpHistory of the currently selected window.
 * @param {PropertyGraphModel.Node} selectedNode - The node in the document that is the current display root node.
 * @param {DataModels.StructuredDocumentDataModel} selectDocModel - The dataNode of the document that is selected.
 */
GuiActions.StructuredDocumentActions.addToJumpHistory=function(selectedNode,selectDocModel){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let navHistory=wikiDesktop.jumpHistory[selectedWindow.getWindowId()];
    navHistory.setCurrent(new jumpHistory(selectedNode,selectDocModel));
}

GuiActions.StructuredDocumentActions.jumpUp=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let doc=selectedWindow.findFirstChildByType(Presenters.StructuredDocumentPresenter)
    let currentNode=doc.rootDisplayNode;
    if(currentNode===doc.dataNode){
        return;
    }
    else{
        GuiActions.StructuredDocumentActions.jumpToElement(new TreeNodeInterface(currentNode).parent);
    }
}

/** Given a presenter, select the window the presenter is in and jumpUp.
 *  It is possible click on a GUI element to jump up in one window while another window is selected
 *  to ensure we jump up in the correct window, the window that the presenter is in must be selected first.
 **/ 
GuiActions.StructuredDocumentActions.jumpUpFromPresenter=function(presenter){
    let window=presenter.findFirstParentByType(Presenters.SingleNodeWindow);
    GuiActions.WikiActions.selectWindow(window.getWindowId());
    GuiActions.StructuredDocumentActions.jumpUp();
}

GuiActions.StructuredDocumentActions.jumpNext=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let doc=selectedWindow.findFirstChildByType(Presenters.StructuredDocumentPresenter)    
    let currentNode=doc.rootDisplayNode;
    if(currentNode===doc.dataNode){
        return;
    }
    else{
        let next=new TreeNodeInterface(currentNode).getNext();
        if(next!==null){
            GuiActions.StructuredDocumentActions.jumpToElement(next);
        }
    }
}

GuiActions.StructuredDocumentActions.jumpPrevious=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let doc=selectedWindow.findFirstChildByType(Presenters.StructuredDocumentPresenter)    
    let currentNode=doc.rootDisplayNode;
    if(currentNode===doc.dataNode){
        return;
    }
    else{
        let previous=new TreeNodeInterface(currentNode).getPrevious();
        if(previous!==null){
            GuiActions.StructuredDocumentActions.jumpToElement(previous);
        }
    }
}

GuiActions.StructuredDocumentActions.showStructuralPath=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let docViewModel=wikiDesktop.documentViewModels[selectedWindow.getWindowId()];
    docViewModel.showStructuralPath=true;
    selectedWindow.refresh();
}

GuiActions.StructuredDocumentActions.hideStructuralPath=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let docViewModel=wikiDesktop.documentViewModels[selectedWindow.getWindowId()];
    docViewModel.showStructuralPath=false;
    selectedWindow.refresh();
}

GuiActions.StructuredDocumentActions.showId=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let docViewModel=wikiDesktop.documentViewModels[selectedWindow.getWindowId()];
    docViewModel.showId=true;
    selectedWindow.refresh();
}

GuiActions.StructuredDocumentActions.hideId=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let docViewModel=wikiDesktop.documentViewModels[selectedWindow.getWindowId()];
    docViewModel.showId=false;
    selectedWindow.refresh();
}

GuiActions.StructuredDocumentActions.showRelativePath=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let docViewModel=wikiDesktop.documentViewModels[selectedWindow.getWindowId()];
    docViewModel.showRelativePath=true;
    selectedWindow.refresh();
}

GuiActions.StructuredDocumentActions.hideRelativePath=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let docViewModel=wikiDesktop.documentViewModels[selectedWindow.getWindowId()];
    docViewModel.showRelativePath=false;
    selectedWindow.refresh();
}

GuiActions.StructuredDocumentActions.getNodeFromAddress=function(address,fileName=null){
    if(fileName==null){
        let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
        let doc=selectedWindow.findFirstChildByType(Presenters.StructuredDocumentPresenter);
        return doc.dataNode.getNodeFromAddress(address,doc.rootDisplayNode);
    }
    else{
        let fileObj=wikiDesktop.wikiData.getFileObjectByName(fileName);
        if(fileObj===null){
            return null;
        }
        let dataNode=fileObj.fileContent;
        return dataNode.getNodeFromAddress(address);
    }
}

GuiActions.StructuredDocumentActions.viewLevelsDownIncrease=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let docViewModel=wikiDesktop.documentViewModels[selectedWindow.getWindowId()];
    docViewModel.nLevelsDown+=1;
    wikiDesktop.messageDisplay.displaySuccessMessage(`The viewLevelsDown has been changed to ${docViewModel.nLevelsDown}`)
    setTimeout(function(){
        wikiDesktop.messageDisplay.clearMessage();
    },1000);
    selectedWindow.refresh()
}

GuiActions.StructuredDocumentActions.viewLevelsDownDecrease=function(){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let docViewModel=wikiDesktop.documentViewModels[selectedWindow.getWindowId()];
    // The view levels down attribute is already at one and cannot be decreased any further.
    if(docViewModel.nLevelsDown==1){
        wikiDesktop.messageDisplay.displayErrorMessage(`ViewLevelsDown is already one, it cannot be decreased any further.`)
        return;
    }
    docViewModel.nLevelsDown-=1;
    selectedWindow.nodePresenter.clearExpandedState()
    selectedWindow.refresh()
    wikiDesktop.messageDisplay.displaySuccessMessage(`ViewLevelsDown has been changed to ${docViewModel.nLevelsDown}`)
    setTimeout(function(){
        wikiDesktop.messageDisplay.clearMessage();
    },1000);

}

GuiActions.StructuredDocumentActions.copyAndPasteBranch=function(src,target,position){
    let newNode=dataActionDispatcher.execute("copyRange",[src],target,position)[0]
    if(position.startsWith("child")){
        let document=wikiDesktop.getDocumentPresenter(target);
        let targetPresenterNode=document.findFirstChildByObjectId(target.objectId);
        targetPresenterNode.expanded=true;
    }
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh()); 
    GuiActions.WikiActions.setSelectedNode(newNode);
}

GuiActions.StructuredDocumentActions.cutAndPasteBranch=function(src,target,position){
    if(src.objectId==target.objectId){
        wikiDesktop.messageDisplay.displayErrorMessage("You cannot move a node to itself.")
        return;
    }
    let parentNode=new TreeNodeInterface(target).parent
    let targetIsChildOfSrc=false
    while(parentNode!==null){
        if(src.objectId==parentNode.objectId){
            targetIsChildOfSrc=true
            break
        }
        parentNode=new TreeNodeInterface(parentNode).parent
    }
    if(targetIsChildOfSrc){
        wikiDesktop.messageDisplay.displayErrorMessage("You cannot move a node below itself.")
        return;
    }

    dataActionDispatcher.execute("moveRange",[src],target,position)
    if(position.startsWith("child")){
        let document=wikiDesktop.getDocumentPresenter(target);
        let targetPresenterNode=document.findFirstChildByObjectId(target.objectId);
        targetPresenterNode.expanded=true;
    }
    wikiDesktop.topWindowPanel.windowManager
        .getAllWindows()
        .forEach(window=>window.refresh()); 
    GuiActions.WikiActions.setSelectedNode(src);
}

GuiActions.StructuredDocumentActions.loadBaseCommandInterpreterSubsystem=()=>{
    let config=new BaseOperationsConfig().definition();
    let commandDispatcher=new ActionDispatcher({
        "insertText":GuiActions.StructuredDocumentActions.insertText,
        "nodeHasChildren":GuiActions.StructuredDocumentActions.nodeHasChildren,
        "jumpBack":GuiActions.StructuredDocumentActions.jumpBack,
        "jumpForward":GuiActions.StructuredDocumentActions.jumpForward,
        "jumpToElement":GuiActions.StructuredDocumentActions.jumpToElement,
        "jumpNext":GuiActions.StructuredDocumentActions.jumpNext,
        "jumpPrevious":GuiActions.StructuredDocumentActions.jumpPrevious,
        "jumpUp":GuiActions.StructuredDocumentActions.jumpUp,
        "showStructuralPath":GuiActions.StructuredDocumentActions.showStructuralPath,
        "hideStructuralPath":GuiActions.StructuredDocumentActions.hideStructuralPath,
        "showId":GuiActions.StructuredDocumentActions.showId,
        "hideId":GuiActions.StructuredDocumentActions.hideId,
        "showRelativePath":GuiActions.StructuredDocumentActions.showRelativePath,
        "hideRelativePath":GuiActions.StructuredDocumentActions.hideRelativePath,
        "viewLevelsDownIncrease":GuiActions.StructuredDocumentActions.viewLevelsDownIncrease,
        "viewLevelsDownDecrease":GuiActions.StructuredDocumentActions.viewLevelsDownDecrease,
        "copyAndPasteBranch":GuiActions.StructuredDocumentActions.copyAndPasteBranch,
        "cutAndPasteBranch":GuiActions.StructuredDocumentActions.cutAndPasteBranch,
        "isNodeDisplayedAsRoot":GuiActions.StructuredDocumentActions.isNodeDisplayedAsRoot
    });

    let commandInterpreter=wikiDesktop.commandInterpreter;
    commandInterpreter.loadConfig(config);
    commandInterpreter.loadActionDispatcher(commandDispatcher);
}