"use strict";

/**
 * The io module contains functions for import and export of external data formats.
 */
let io={};

/**
 * This method converts a text input string into a node tree structure. Currently, it only supports text strings
 * where the indentation level is defined by 4 spaces. It does not handle other number of spaces or tabs.
 */
io.insertTextAtNode=function(inputString,targetNode,position){    
    // handle both types of line endings. \n or \r\n
    let contents=inputString.split(/\r?\n/);
        
    // Todo: better error handling of where the leading number of spaces is not a multiple of 4.
    contents=contents.map(line=>{
        // find all the leading spaces.
        let match = line.match(/^ +/)
        let numLeadingSpaces=match? match[0].length :0;
        let indentLevel=numLeadingSpaces/4
        let text=line.trim();
        // if the string starts with a "-" remove it and replace all the spaces afterwards
        if(text.startsWith("-")){
            text=text.slice(1);
            text=text.trim();
        }
        return {"text":text,"indentLevel":indentLevel}
    });

    let currentLevel=-1;
    let currentDataNode=targetNode;  
    let topLevelNodes=[];
    for(let record of contents){
        let newDataNode=new DataModels.ProseMirrorDataModel();
        let state=EditorState.create({schema:newDataNode.schema})
        let tr=state.tr;
        tr.insertText(record.text);
        state=state.apply(tr);
        newDataNode.doc=state.doc;
        // skip blank lines
        if(record.text==""){
            continue
        }

        if(currentLevel==-1){
            TreeNodeInterface.insertTreeNode(currentDataNode,newDataNode,position)
            topLevelNodes.push(newDataNode);
            currentDataNode=newDataNode;
            currentLevel+=1
        }
        else if(record.indentLevel>currentLevel){
            currentLevel+=1;
            TreeNodeInterface.insertTreeNode(currentDataNode,newDataNode,"child");
        }
        else if(record.indentLevel===currentLevel){
            if(currentLevel===0){
                topLevelNodes.push(newDataNode);
            }
            TreeNodeInterface.insertTreeNode(currentDataNode,newDataNode,"next");
        }
        else{
            // The next node could be more than one level up
            let levelDiff=currentLevel-record.indentLevel
            for(let i=0;i<levelDiff;i++){
                currentDataNode=new TreeNodeInterface(currentDataNode).parent;
                
            }
            currentLevel=record.indentLevel
            TreeNodeInterface.insertTreeNode(currentDataNode,newDataNode,"next");
        }
        currentDataNode=newDataNode;
    }
    return topLevelNodes;
}

io.insertHTMLAtNode=function(inputString,targetNode,position){
    let wrapperNode=document.createElement("div");
    wrapperNode.innerHTML=inputString;

    // The algorithm here is almost the same as in insertTextAtNode except the level and depth are determined based on HTML attributes instead of parsing the text.
    let currentLevel=-1;
    let currentDataNode=targetNode;  
    let topLevelNodes=[];
    for (const htmlNode of wrapperNode.childNodes) {
        let nodeDepth=parseInt(htmlNode.getAttribute("depth"));
        let nodeType=htmlNode.getAttribute("nodeType");
        let newDataNode;
        if(nodeType==="ProseMirrorDataModel"){
            newDataNode=new DataModels.ProseMirrorDataModel();
            newDataNode.doc=DataModels.ProseMirrorDataModel.convertHTMLStringToProseMirrorNode(htmlNode.outerHTML)
        }
        else{
            throw new Error(`Node type ${nodeType} not supported`)
        }
        if(currentLevel===-1){
            TreeNodeInterface.insertTreeNode(currentDataNode,newDataNode,position);
            topLevelNodes.push(newDataNode);
            currentDataNode=newDataNode;
            currentLevel+=1;
        }
        else if(nodeDepth>currentLevel){
            currentLevel+=1;
            TreeNodeInterface.insertTreeNode(currentDataNode,newDataNode,"child");
        }
        else if(nodeDepth===currentLevel){
            if(currentLevel===0){
                topLevelNodes.push(newDataNode);
            }
            TreeNodeInterface.insertTreeNode(currentDataNode,newDataNode,"next");
        }
        else{
            // The next node could be more than one level up
            let levelDiff=currentLevel-nodeDepth
            for(let i=0;i<levelDiff;i++){
                currentDataNode=new TreeNodeInterface(currentDataNode).parent;
                
            }
            currentLevel=nodeDepth
            TreeNodeInterface.insertTreeNode(currentDataNode,newDataNode,"next");
        }
        currentDataNode=newDataNode;
    }
    return topLevelNodes;
}

/*
Given a node, convert all of its children to text and adds it to the textOutput array
*/
io.nodeToText=function(currentNode,indentLevel,textOutput){
    if(currentNode instanceof PropertyGraphModel.Node){
        currentNode=new TreeNodeInterface(currentNode);
    }
    if(currentNode.node.type==="ProseMirrorDataModel"){
        let line;
        // Indent is defined by four spaces
        if(indentLevel>0){
            line=" ".repeat(indentLevel*4);
            line+=currentNode.node.getTextContent();
        } else{
            line=currentNode.node.getTextContent();
        }
        
        textOutput.push(line);
    }
    let children=currentNode.children;
    if(children.length>0){
        for (let child of children) {
            child=new TreeNodeInterface(child);
            io.nodeToText(child,indentLevel+1,textOutput);
        }
    }
}

io.nodesToText=function(...treeNodes){
    let textOutput=[];
    for(const node of treeNodes){
        io.nodeToText(node,0,textOutput)
    }
    return textOutput.join("\n")
}

io.nodesToHTML=function(...treeNodes){
    let depth=0;
    let HTMLStrings=[]
    for(const node of treeNodes){
        io.nodeToHTML(node,depth,HTMLStrings)
    }
    return HTMLStrings.join("")
}

/**
 * Converts document data nodes to an HTML string. The HTML format is defined as a non-nested list of HTML strings that represent a node. Each node has a depth attribute which determines the depth of the node relative to the toplevel nodes in the HTML string return. The nodes appear in order so a child node will come after a parent with a depth of one greater then the parent.
 * @param {PropertGraphModel.Node || TreeNodeInterface} node 
 */
io.nodeToHTML=function(node,depth,HTMLStrings){
    if(node instanceof PropertyGraphModel.Node){
        node=new TreeNodeInterface(node);
    }
    if(node.node.type==="ProseMirrorDataModel"){
        let HTMLNode=DataModels.ProseMirrorDataModel.convertProseMirrorDocToHTMLNode(node.node.doc);
        HTMLNode.setAttribute("depth",depth)
        HTMLNode.setAttribute("nodeType","ProseMirrorDataModel")
        HTMLStrings.push(HTMLNode.outerHTML)
    }
    else{
        throw new Error(`Node type ${node.node.type} not handled`);
    }
    let children=node.children;
    if(children.length>0){
        for (let child of children) {
            child=new TreeNodeInterface(child);
            io.nodeToHTML(child,depth+1,HTMLStrings)
        }
    }
}