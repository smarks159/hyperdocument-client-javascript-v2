"use strict";


GuiActions.WikiActions={};

GuiActions.WikiActions.setSelectedNode=function(dataNode,clearRange=true){
    wikiDesktop.selectedNode=dataNode;
    if(clearRange && !wikiDesktop.selectedRange.isEmpty()){
        GuiActions.WikiActions.unselectRange();
    }
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let newPresenter=selectedWindow.findFirstChildByObjectId(dataNode.objectId)
    newPresenter.focus(); 
}

GuiActions.WikiActions.getNodeDocument=function(dataNode){
    let treeNode=new TreeNodeInterface(dataNode);
    let doc=treeNode.findParentByType(DataModels.StructuredDocumentDataModel);
    return doc;
}

GuiActions.WikiActions.areNodesInSameDocument=function(startDataNode,endDataNode){
    let doc1=GuiActions.WikiActions.getNodeDocument(startDataNode);
    let doc2=GuiActions.WikiActions.getNodeDocument(endDataNode);
    if(doc1.objectId!=doc2.objectId){
        return false;
    }
    else{
        return true;
    }
}

GuiActions.WikiActions.documentNodeOnMouseDownCallback=function(event,selectedPresenter){
    let window=selectedPresenter.findFirstParentByType(Presenters.SingleNodeWindow);
    GuiActions.WikiActions.selectWindow(window.getWindowId());
    if(event.button===0){
        if(event.shiftKey && wikiDesktop.selectedNode && wikiDesktop.selectedNode!=selectedPresenter.dataNode
            &&  GuiActions.WikiActions.areNodesInSameDocument(wikiDesktop.selectedNode,selectedPresenter.dataNode)){   
            event.preventDefault();   
            GuiActions.WikiActions.setSelectedRange(wikiDesktop.selectedNode,selectedPresenter.dataNode);
        }
        else{
            GuiActions.WikiActions.setSelectedNode(selectedPresenter.dataNode);
        } 
        
        // Stop the mousedown event from propagating to parent document nodes. Otherwise the parent nodes will have setSelectedRange called on them as well. We still want the mousedown event to fire at the document level so popups will close when clicking on a document node.
        event.stopPropagation();
        $(document).trigger("mousedown");
    }
}

/**
 * Given a start and end node within the same document set the range as the nodes between the start and end node. If the start node and end node are on different levels of the document tree then select the node who is the most furtherest up the document and use that level to select the range.
 * @param {PropertyGraphModel.Node} startDataNode 
 * @param {PropertyGraphModel.Node} endDataNode 
 */
GuiActions.WikiActions.setSelectedRange=function(startDataNode,endDataNode){
    if(!GuiActions.WikiActions.areNodesInSameDocument(startDataNode,endDataNode)){
        throw Error("The objects in a range must belong to the same document");
    }
    let doc=GuiActions.WikiActions.getNodeDocument(startDataNode);
    let startLevel=doc.getNodeLevelFromOrigin(startDataNode);
    let endLevel=doc.getNodeLevelFromOrigin(endDataNode);
    let topNode,bottomNode

    // This logic, finds the sibling of the start node for the range, if the user selects a range with a child or parent element. This typically happens when selecting an expanded child element, so the range is set to the node at the top most level of the document.
    if(startLevel<=endLevel){
        topNode=startDataNode;
        bottomNode=endDataNode;
    }else{
        topNode=endDataNode;
        bottomNode=startDataNode;
    }
    let bottomTreeNode=new TreeNodeInterface(bottomNode);
    let topTreeNode=new TreeNodeInterface(topNode);
    while(bottomTreeNode.parent.objectId!=topTreeNode.parent.objectId){
        bottomTreeNode=new TreeNodeInterface(bottomTreeNode.parent);
    }
    bottomNode=bottomTreeNode.node
    let startNode,endNode;
    if(startLevel<=endLevel){
        startNode=topTreeNode;
        endNode=bottomTreeNode;
    } else{
        startNode=bottomTreeNode;
        endNode=topTreeNode;
    }
    wikiDesktop.selectedRange.setRange(startNode.node,endNode.node);
    GuiActions.WikiActions.highlightSelectedRange();
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let newPresenter=selectedWindow.findFirstChildByObjectId(endNode.node.objectId)
    newPresenter.focus(); 
}

GuiActions.WikiActions.unselectRange=function(){
    GuiActions.WikiActions.unhighlightSelectedRange();
    wikiDesktop.selectedRange.clearRange();
}

GuiActions.WikiActions.highlightSelectedRange=function(){
    wikiDesktop.getPresentersInRange().forEach(presenter=>{
        presenter.highlight();
    });    
}

GuiActions.WikiActions.unhighlightSelectedRange=function(){
    wikiDesktop.getPresentersInRange().forEach(presenter=>{
        presenter.unhighlight();
    }); 
}

GuiActions.WikiActions.addFile=function(offset){
    let fileName="";
    let inputForm=new InputFormPresenters.inputForm()
    inputForm.addField(new InputFormPresenters.displayTextField("Enter File Name"));
    inputForm.addField(new InputFormPresenters.stringLineField("fileName"));    
    wikiDesktop.openFormInPopupPanel(inputForm,
        (inputData)=>{
            fileName=inputData.fileName;
            if(!fileName){
                inputForm.displayFieldErrorMsg("fileName","You must enter a filename");
                return;
            }
            if(wikiDesktop.wikiData.getFileObjectByName(fileName)!==null){
                inputForm.displayFieldErrorMsg("fileName",`The File: "${fileName}" already exists in the wiki`)
                return;
            };
            dataActionDispatcher.execute("createFile",fileName);
            wikiDesktop.popupPanel.closePopup();
            wikiDesktop.messageDisplay.displaySuccessMessage("The File:"+fileName+" has been added to the wiki.");
            setTimeout(function(){
                wikiDesktop.messageDisplay.clearMessage();
            },1000);
            wikiDesktop.topWindowPanel.filePanelPresenter.refresh();
            GuiActions.WikiActions.selectFile(fileName);
            let fileData=wikiDesktop.wikiData.getFileObjectByName(fileName)
            GuiActions.StructuredDocumentActions.addToJumpHistory(fileData.fileContent,fileData.fileContent);
        }
    ,offset)
}

GuiActions.WikiActions.selectFile=function(fileName){  
    // unselect the range of the currently selected window before selecting another file.
    GuiActions.WikiActions.unselectRange();
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow()
    selectedWindow.setWindowName(fileName);
    let fileData=wikiDesktop.wikiData.getFileObjectByName(fileName)
    selectedWindow.dataNode=fileData.fileContent;
    selectedWindow.refresh()
}

GuiActions.WikiActions.ConfirmAndDeleteFile=function(fileName,offset){
    wikiDesktop.confirmationDialog
        .textToDisplay("Delete the file "+fileName+"?")
        .onResponse(key=>{
            if(key==="yes"){
                GuiActions.WikiActions.deleteFile(fileName);
            }
            wikiDesktop.confirmationDialog.closePopup()
        });
    wikiDesktop.confirmationDialog.openAsPopup(offset);
    
}

/**
 * Given a fileName that has been deleted remove all traces of the file from the GUI. This includes the follow state:
 *    Any window that is currently displaying the file must be cleared of data and the name of the window must be cleared as well.
 * 
 * The reason that this is separated out because this is used to update the GUI in undo/redo in addition to calling the wikiAction deleteFile.
 * @param fileData - A FileObject. The reason that this function takes a FileObject and not a name is that the file has already been removed from the data, so it cannot be retrieved from the database by file name. Previously, getting the objectId was required to remove cached data. This data is no longer cached.
 */
GuiActions.WikiActions.clearFileGUIState=function(fileData){
    let fileName=fileData.fileName
    wikiDesktop.topWindowPanel.filePanelPresenter.refresh();
    let windows=wikiDesktop.topWindowPanel.windowManager.getAllWindows();
    for(const window of windows){
        if(window.windowName===fileName){
            window.dataNode=null;
            window.setWindowName(null);
            window.refresh();
        }
    }
}

GuiActions.WikiActions.deleteFile=function(fileName){
    let fileData=wikiDesktop.wikiData.getFileObjectByName(fileName);
    dataActionDispatcher.execute("removeFile",fileName);
    GuiActions.WikiActions.clearFileGUIState(fileData);
}

GuiActions.WikiActions.renameFile=function(originalName,offset){
    let inputForm=new InputFormPresenters.inputForm()
    inputForm.addField(new InputFormPresenters.displayTextField("Enter New File Name"));
    inputForm.addField(new InputFormPresenters.stringLineField("newFileName",originalName));
    wikiDesktop.openFormInPopupPanel(inputForm,
        (inputData)=>{
            let newName=inputData.newFileName;
            if(!newName){
                inputForm.displayFieldErrorMsg("newFileName","You must enter a filename");
                return;
            }
            if(wikiDesktop.wikiData.getFileObjectByName(newName)!==null && newName!==originalName){
                inputForm.displayFieldErrorMsg("newFileName",`The File: "${newName}" already exists in the wiki`)
                return;
            }
            let fileNode=wikiDesktop.wikiData.getFileObjectByName(originalName);
            dataActionDispatcher.execute("renameFile",fileNode,newName);
            wikiDesktop.topWindowPanel.filePanelPresenter.refresh();
            let windows=wikiDesktop.topWindowPanel.windowManager.getAllWindows();
            for(const window of windows){
                if(window.windowName===originalName){
                    window.setWindowName(newName);
                }
            }
            wikiDesktop.popupPanel.closePopup();
            wikiDesktop.messageDisplay.displaySuccessMessage("The File:"+originalName+" has been rename to "+newName);
            setTimeout(function(){
                wikiDesktop.messageDisplay.clearMessage();
            },1000);
        }
    ,offset)
}

GuiActions.WikiActions.saveWiki=function(){
    let inputForm=new InputFormPresenters.inputForm();
    inputForm.addField(new InputFormPresenters.displayTextField("Where do you want to save the wiki file too?"));
    let currentFile="";
    if(wikiDesktop.currentWikiFile!==null){
        currentFile=wikiDesktop.currentWikiFile;
    }    
    inputForm.addField(
        new InputFormPresenters.fileInput("fileName",currentFile)
            .selectFileAction((field)=>{
                let fileName=GuiActions.WikiActions.selectFileToSave();
                if(fileName!==""){
                    field.inputFieldAttr.text=fileName;
                    field.inputFieldAttr.refresh();
                    inputForm.submitButton.click();
                }
            })
    );
    wikiDesktop.openFormInPopupPanel(inputForm,
        (inputData)=>{
            let fileName=inputData.fileName;;
            if(fileName!==""){
                let fileOpened=ipcRenderer.sendSync("isFileOpened",fileName);
                if(fileOpened===false){
                    GuiActions.WikiActions.saveFile(fileName,wikiDesktop.wikiData.propertyGraph.saveToData());
                    wikiDesktop.setCurrentWikiFile(fileName);
                    wikiDesktop.popupPanel.closePopup();
                    wikiDesktop.messageDisplay.displaySuccessMessage(`The wiki has been saved to ${fileName}`);
                    GuiActions.WikiActions.setWindowTitle(fileName);
                    setTimeout(function(){
                        wikiDesktop.messageDisplay.clearMessage();
                    },1000);
                }else{
                    inputForm.displayFieldErrorMsg("fileName","This file is already open in another browser.")
                }
            }
            else{
                inputForm.displayFieldErrorMsg("fileName","You must enter a filename");
            }
        }
    ,wikiDesktop.topWindowPanel.wikiActionDropDown);
}

GuiActions.WikiActions.loadWiki=function(){
    let inputForm=new InputFormPresenters.inputForm();
    inputForm.addField(new InputFormPresenters.displayTextField("What Wiki File do you want to load?"));
    inputForm.addField(
        new InputFormPresenters.fileInput("fileName")
        .selectFileAction((field)=>{
            let fileName=GuiActions.WikiActions.selectFileToLoad();
            if(fileName!==""){
                field.inputFieldAttr.text=fileName;
                field.inputFieldAttr.refresh();
                inputForm.submitButton.click();
            }
        })
    );
    wikiDesktop.openFormInPopupPanel(inputForm,
        (inputData)=>{
            let fileName=inputData["fileName"];
            if(fileName!==""){
                let fileOpened=ipcRenderer.sendSync("isFileOpened",fileName);
                if(fileOpened===false){
                    GuiActions.WikiActions.loadWikiFile(fileName);
                    wikiDesktop.popupPanel.closePopup();
                    wikiDesktop.messageDisplay.displaySuccessMessage(`Loaded wiki file ${fileName}`);
                    setTimeout(function(){
                        wikiDesktop.messageDisplay.clearMessage();
                    },1000);
                }else{
                    inputForm.displayFieldErrorMsg("fileName","This file is already open in another browser.")
                }
            }
            else{
                inputForm.displayFieldErrorMsg("fileName","You must select a file to load");
            }
        }
    ,wikiDesktop.topWindowPanel.wikiActionDropDown)
}

GuiActions.WikiActions.selectFileToSave=function(){
    return ipcRenderer.sendSync("selectFileToSave");
}

GuiActions.WikiActions.selectFileToLoad=function(){
    return ipcRenderer.sendSync("selectFileToLoad");
}

GuiActions.WikiActions.saveFile=function(fileName,data){
    let fileOpened=ipcRenderer.sendSync("isFileOpened",fileName);
    if(fileOpened===true){
        throw new Error(`The file: ${fileName} is already opened in another window`);
    }
    let results=ipcRenderer.sendSync("saveWikiFile",fileName,JSON.stringify(data));
    GuiActions.WikiActions.setUnsavedChanges(false);
    return results;
}

GuiActions.WikiActions.loadWikiFile=function(fileName){
    let fileOpened=ipcRenderer.sendSync("isFileOpened",fileName);
    if(fileOpened===true){
        ipcRenderer.sendSync("focusOnWindowByFilename",fileName);
    }
    else{
        let confirmed=ipcRenderer.sendSync("confirmUnsavedChanges");
        if(!confirmed){
            return;
        }
        let fileData=JSON.parse(ipcRenderer.sendSync("loadWikiFile",fileName));
        if(!fileData){
            throw new Error("Trying to load a wiki file that doesn't exist");
        }
        wikiDesktop.createDBInterfaces(fileData);
        wikiDesktop.initializeDefaultSearchNode();
        let MainPresenter=wikiDesktop.topWindowPanel
        MainPresenter.resetWiki();
        wikiDesktop.resetGUIState();
        GuiActions.WikiActions.setWindowTitle(fileName);
        wikiDesktop.setCurrentWikiFile(fileName);
        GuiActions.WikiActions.setUnsavedChanges(false);
    }
}

GuiActions.WikiActions.createNewWiki=function(){
    let confirmed=ipcRenderer.sendSync("confirmUnsavedChanges");
    if(!confirmed){
        return;
    }
    wikiDesktop.createDBInterfaces(new PropertyGraphModel.PropertyGraph());
    wikiDesktop.initializeDefaultSearchNode();
    let MainPresenter=wikiDesktop.topWindowPanel
    MainPresenter.resetWiki();
    wikiDesktop.resetGUIState();
    GuiActions.WikiActions.setWindowTitle("new wiki");
    GuiActions.WikiActions.setUnsavedChanges(false);
}

GuiActions.WikiActions.setWindowTitle=function(fileName){
    if(fileName===null || fileName===undefined){
        throw("Filename is not defined");
    }
    ipcRenderer.sendSync("setWindowTitle",fileName);
}

GuiActions.WikiActions.openFileContextMenu=function(presenter,fileName){
    wikiDesktop.contextMenu.choices([
        {text:"Add File",callback:()=>{
            wikiDesktop.contextMenu.closePopup();
            GuiActions.WikiActions.addFile(presenter);
        }},
        {text:"Remove File",callback:()=>{
            wikiDesktop.contextMenu.closePopup();
            GuiActions.WikiActions.ConfirmAndDeleteFile(fileName,presenter);
        }},
        {text:"Rename File",callback:()=>{
            wikiDesktop.contextMenu.closePopup();
            GuiActions.WikiActions.renameFile(fileName,presenter)
        }},
        {text:"Export File To Text",callback:()=>{
            wikiDesktop.contextMenu.closePopup();
            GuiActions.WikiActions.exportFileToText(fileName,presenter);
        }}
    ])
    wikiDesktop.contextMenu.openAsPopup(presenter);
}

GuiActions.WikiActions.openFilePanelContextMenu=function(offset){
    wikiDesktop.contextMenu.choices([
        {text:"Add File",callback:()=>{
            wikiDesktop.contextMenu.closePopup();
            GuiActions.WikiActions.addFile(offset);
        }}
    ])
    wikiDesktop.contextMenu.openAsPopup(offset);
}

GuiActions.WikiActions.importTextFile=function(){
    let inputForm=new InputFormPresenters.inputForm()
    let wikiFileField=new InputFormPresenters.stringLineField("wikiFileName");
    inputForm.addField(new InputFormPresenters.displayTextField("Select the File To Import:"));
    inputForm.addField(
        new InputFormPresenters.fileInput("importFileName")
        .selectFileAction((field)=>{
            let fileName=GuiActions.WikiActions.selectTextToImport();
            if(fileName!==""){
                field.inputFieldAttr.text=fileName;
                field.inputFieldAttr.refresh();

                let wikiFile=path.basename(fileName,".txt");
                wikiFileField.setData(wikiFile);
            }
        })
    )
    inputForm.addField(new InputFormPresenters.displayTextField("Enter the name of the file to create in the wiki"));
    inputForm.addField(wikiFileField);
    
    wikiDesktop.openFormInPopupPanel(inputForm,
        (inputData)=>{
            let importFileName=inputData.importFileName;
            let wikiFileName=inputData.wikiFileName;
            
            if(!wikiFileName){
                inputForm.displayFieldErrorMsg("wikiFileName","You must enter a filename")
            }
            
            if(wikiDesktop.wikiData.getFileObjectByName(wikiFileName)!==null){
                inputForm.displayFieldErrorMsg("wikiFileName",`The File: "${wikiFileName}" already exists in the wiki`)
            }
            
            if(!importFileName){
                inputForm.displayFieldErrorMsg("importFileName","You must select a file to import")
            }
            
            if(inputForm.hasErrors()){
                return;
            }
            
            let fileString=ipcRenderer.sendSync("loadFile",importFileName)
            dataActionDispatcher.execute("createFileFromText",wikiFileName,fileString);
            
            // call selectFile aftwards because it also refreshes the document being displayed.
            GuiActions.WikiActions.selectFile(wikiFileName);  
            wikiDesktop.topWindowPanel.filePanelPresenter.refresh();
            let fileData=wikiDesktop.wikiData.getFileObjectByName(wikiFileName)
            GuiActions.StructuredDocumentActions.addToJumpHistory(fileData.fileContent,fileData.fileContent);
            
            wikiDesktop.messageDisplay.displaySuccessMessage(`Text File imported successfully to the wiki file "${wikiFileName}".`);
            setTimeout(function(){
                wikiDesktop.messageDisplay.clearMessage();
            },1000);
            
            wikiDesktop.popupPanel.closePopup();
            GuiActions.WikiActions.setUnsavedChanges(true);

        }
    ,wikiDesktop.topWindowPanel.wikiActionDropDown)
}

GuiActions.WikiActions.exportFileToText=function(fileName,presenter){
    let inputForm=new InputFormPresenters.inputForm();
    inputForm.addField(new InputFormPresenters.displayTextField(`You have choosen to export the file "${fileName}". Select a file to export the data to.`))
    inputForm.addField(
        new InputFormPresenters.fileInput("exportFileName")
        .selectFileAction((field)=>{
            let fileName=GuiActions.WikiActions.selectFileToExport();
            if(fileName!==""){
                field.inputFieldAttr.text=fileName;
                field.inputFieldAttr.refresh();
            }
        })
    )
    wikiDesktop.openFormInPopupPanel(inputForm,
        (inputData)=>{
            let exportFileName=inputData.exportFileName;
            if(!exportFileName){
                inputForm.displayFieldErrorMsg("exportFileName","You must enter a filename");
                return;
            }
    
            let fileData=wikiDesktop.wikiData.getFileObjectByName(fileName);
            let documentNode=fileData.fileContent;
            let docTree=new TreeNodeInterface(documentNode)
            let outText=io.nodesToText(...docTree.children.map(child=>{return new TreeNodeInterface(child)}));        
            let hasFileSaved=ipcRenderer.sendSync("saveFile",exportFileName,outText);
    
            if(hasFileSaved===true){
                wikiDesktop.messageDisplay.displaySuccessMessage(`File "${fileName}" exported to text successfully`);
                setTimeout(function(){
                    wikiDesktop.messageDisplay.clearMessage();
                },1000)
            }
            wikiDesktop.popupPanel.closePopup();
        }    
    ,presenter)
}

GuiActions.WikiActions.selectTextToImport=function(){
    return ipcRenderer.sendSync("selectTextToImport");
}

GuiActions.WikiActions.selectFileToExport=function(){
    return ipcRenderer.sendSync("selectFileToExport");
}

GuiActions.WikiActions.displayRecentlyOpenedWikiPanel=function(){
    wikiDesktop.clearDeletedFilesFromHistory();
    wikiDesktop.recentlyOpenedWikiPanel.openAsPopup(wikiDesktop.topWindowPanel.wikiActionDropDown);
}

GuiActions.WikiActions.setUnsavedChanges=function(value){
    ipcRenderer.sendSync("setUnsavedChanges",value);
}

GuiActions.WikiActions.newWindow=function(filePath=null){
    ipcRenderer.sendSync("newWindow",filePath);
}

GuiActions.WikiActions.selectWindow=function(windowId){
    let windowManager=wikiDesktop.topWindowPanel.windowManager;
    windowManager.selectWindow(windowId);
    wikiDesktop.selectedWindow=windowId;
}

GuiActions.WikiActions.getSelectedWindow=function(){
    let windowManager=wikiDesktop.topWindowPanel.windowManager;
    return windowManager.getWindowById(wikiDesktop.selectedWindow);
}

/**
 * Returns the window commands to be displayed in the context menu based on the state of the window. There are currently only two windows, so the rules are based on that. The window commands are displayed in both the top level window context menu as well as each individual node context so the functionality is split into a separate function.
 * 
 * Rules:
 *    - If one window is open and the other is closed display the splitWindow command.
 *    - If both windows are open display the hideWindow command, but not the splitWindow command.
 */
GuiActions.WikiActions.getWindowMenuCommands=function(){
    let commands=[];
    let windowManager=wikiDesktop.topWindowPanel.windowManager;
    if((windowManager.getWindowById("left").isVisible() && !windowManager.getWindowById("right").isVisible()) || 
    (windowManager.getWindowById("right").isVisible() && !windowManager.getWindowById("left").isVisible())){
        commands.push({
            text:"Split Window", callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                GuiActions.WikiActions.splitWindow();
            }
        })
    }
    else if(windowManager.getWindowById("left").isVisible() && windowManager.getWindowById("left").isVisible()){
        commands.push({
            text:"Hide Window",callback:()=>{
                wikiDesktop.contextMenu.closePopup();
                GuiActions.WikiActions.hideWindow()
            }
        })
    }
    return commands;
}

GuiActions.WikiActions.openWindowMenu=function(offset){
    wikiDesktop.contextMenu.choices(GuiActions.WikiActions.getWindowMenuCommands());
    wikiDesktop.contextMenu.openAsPopup(offset)
}

GuiActions.WikiActions.splitWindow=function(){
    let windowManager=wikiDesktop.topWindowPanel.windowManager;
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    let windowToShow=null;
    if(selectedWindow.getWindowId()==="left"){
        windowToShow=windowManager.getWindowById("right")
    } 
    else if(selectedWindow.getWindowId()==="right"){
        windowToShow=windowManager.getWindowById("left")
    }
    windowToShow.show();
    windowManager.refresh();
}

GuiActions.WikiActions.hideWindow=function(){
    let windowManager=wikiDesktop.topWindowPanel.windowManager;
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    selectedWindow.hide();
    // When hiding a window, change the selection to the other window that is still visible.
    let windowId=selectedWindow.getWindowId();
    if(windowId=="left"){
        GuiActions.WikiActions.selectWindow("right");
    }
    else if(windowId=="right"){
        GuiActions.WikiActions.selectWindow("left");
    }
    windowManager.refresh();
}

GuiActions.WikiActions.toggleCommandInterpreter=function(){    
    // activate command interpreter
    if(wikiDesktop.commandInterpreterActive==false){
        wikiDesktop.commandInterpreterActive=true;
        // Must render the presenter first before running the command interpreter because the command interpreter will print text to the GUI on initialization.
        wikiDesktop.topWindowPanel.refresh();
        wikiDesktop.topWindowPanel.commandInterpreter.run();
    }
    // deactivate command interpreter
    else{
        wikiDesktop.commandInterpreterActive=false;
        wikiDesktop.topWindowPanel.refresh();
        wikiDesktop.topWindowPanel.commandInterpreter.reset();
    }
    
}

GuiActions.WikiActions.displayGlobalSearch=function(){
    let searchNode=wikiDesktop.tagsDBInterface.queryTagList("searchNode","default")[0]
    GuiActions.WikiActions.displaySearchInSelectedWindow(searchNode);
}

GuiActions.WikiActions.displaySearchInSelectedWindow=function(searchNode){
    let selectedWindow=GuiActions.WikiActions.getSelectedWindow();
    selectedWindow.dataNode=searchNode;
    selectedWindow.setWindowName("wiki search")
    selectedWindow.refresh()
}