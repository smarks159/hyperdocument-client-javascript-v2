var dataActions={};
dataActions.addToUndo=function(item){
    undoStack.push(item);
    /* For commands with a source and a target, undo will update the source element, while redo will update the target element. 
    So, for updateVisual to work properly we must keep track of wheter the operation being called is an undo or a redo operation.
    */
    item.visualData.isUndo=true;
    redoStack=[];
}

dataActions.moveBranch=function(src,target,position){
    let srcTree=new TreeNodeInterface(src)
    let documentNode=srcTree.findParentByType(DataModels.StructuredDocumentDataModel);
    TreeNodeInterface.moveBranch(src,target,position);
    if(documentNode.isEmpty()){
        new TreeNodeInterface(documentNode).appendChild(new DataModels.ProseMirrorDataModel());
    }
}

dataActions.copyBranch=function(src,target,position){
    return TreeNodeInterface.copyBranch(src,target,position);
}
dataActions.moveRange=function(srcNodes,target,position){
    srcNodes.forEach((srcNode,index)=>{
        if(index===0){
            dataActions.moveBranch(srcNode,target,position);
        }
        else{
            dataActions.moveBranch(srcNode,target,"next");
        }
        target=srcNode;
    });
}

dataActions.copyRange=function(srcNodes,target,position){
    let newNodes=[]
    srcNodes.forEach((srcNode,index)=>{
        let newNode;
        if(index===0){
            newNode=TreeNodeInterface.copyBranch(srcNode,target,position)
        }
        else{
            newNode=TreeNodeInterface.copyBranch(srcNode,target,"next")
        }
        target=newNode;
        newNodes.push(newNode);
    });
    return newNodes;
}

dataActions.insertNode=function(targetNode,newNode,position){
    TreeNodeInterface.insertTreeNode(targetNode,newNode,position);
}

dataActions.insertTextAtNode=function(inputString,targetNode,position){
   return io.insertTextAtNode(inputString,targetNode,position)
}

dataActions.insertHTMLAtNode=function(inputString,targetNode,position){
    return io.insertHTMLAtNode(inputString,targetNode,position)
 }

dataActions.deleteNode=function(dataNode){
    dataNode=new TreeNodeInterface(dataNode)
    let documentNode=dataNode.findParentByType(DataModels.StructuredDocumentDataModel);
    dataNode.delete();
    if(documentNode.isEmpty()){
        new TreeNodeInterface(documentNode).appendChild(new DataModels.ProseMirrorDataModel());
    }
}

dataActions.deleteRange=function(nodeList){
    nodeList.forEach(node=>dataActions.deleteNode(node))
}

dataActions.updateProseMirrorData=function(dataNode,proseMirrorState){
    dataNode.doc=proseMirrorState.doc
}

dataActions.addFile=function(fileName,addBlankNode=true){
    let newFile=wikiDesktop.wikiData.addFile(fileName);
    if(addBlankNode){
        dataActions.insertNode(newFile.fileContent,new DataModels.ProseMirrorDataModel(),"child");
    }
    return newFile;
}

dataActions.removeFile=function(fileName){
    wikiDesktop.wikiData.removeFile(fileName);
}

dataActions.renameFile=function(fileObject,newName){
    wikiDesktop.wikiData.renameFile(fileObject,newName);
}

dataActions.executeTransaction=function(transactionFunc){
    let oldGraph=wikiDesktop.wikiData.propertyGraph.saveToData();
    transactionFunc();
    let diff=PropertyGraphModel.genDiff(wikiDesktop.wikiData.propertyGraph.saveToData(),oldGraph);
    return diff;
}